package com.praya.herbalism;

import org.bukkit.plugin.java.JavaPlugin;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.command.CommandMemory;
import com.praya.herbalism.language.LanguageMemory;
import com.praya.herbalism.manager.plugin.CommandManager;
import com.praya.herbalism.manager.plugin.LanguageManager;
import com.praya.herbalism.manager.plugin.MetricsManager;
import com.praya.herbalism.manager.plugin.PlaceholderManager;
import com.praya.herbalism.manager.plugin.PluginManager;
import com.praya.herbalism.manager.plugin.PluginPropertiesManager;
import com.praya.herbalism.metrics.MetricsMemory;
import com.praya.herbalism.placeholder.PlaceholderMemory;
import com.praya.herbalism.plugin.PluginPropertiesMemory;

public final class HerbalismPluginMemory extends PluginManager {

	private final CommandManager commandManager;
	private final LanguageManager languageManager;
	private final PlaceholderManager placeholderManager;
	private final PluginPropertiesManager pluginPropertiesManager;
	private final MetricsManager metricsManager;
	
	protected HerbalismPluginMemory(Herbalism plugin) {
		super(plugin);
		
		this.commandManager = CommandMemory.getInstance();
		this.placeholderManager = PlaceholderMemory.getInstance();
		this.languageManager = LanguageMemory.getInstance();
		this.pluginPropertiesManager = PluginPropertiesMemory.getInstance();
		this.metricsManager = MetricsMemory.getInstance();
	}
	
	private static class DreamFishPluginMemorySingleton {
		private static final HerbalismPluginMemory instance;
		
		static {
			final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
			
			instance = new HerbalismPluginMemory(plugin);
		}
	}
	
	protected static final HerbalismPluginMemory getInstance() {
		return DreamFishPluginMemorySingleton.instance;
	}
	
	public final LanguageManager getLanguageManager() {
		return this.languageManager;
	}
	
	public final PlaceholderManager getPlaceholderManager() {
		return this.placeholderManager;
	}
	
	public final PluginPropertiesManager getPluginPropertiesManager() {
		return this.pluginPropertiesManager;
	}
	
	public final MetricsManager getMetricsManager() {
		return this.metricsManager;
	}
	
	public final CommandManager getCommandManager() {
		return this.commandManager;
	}
}
