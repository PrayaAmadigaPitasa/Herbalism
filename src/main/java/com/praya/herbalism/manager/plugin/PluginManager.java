package com.praya.herbalism.manager.plugin;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.handler.HandlerManager;

public abstract class PluginManager extends HandlerManager {
	
	protected PluginManager(Herbalism plugin) {
		super(plugin);
	}
	
	public abstract LanguageManager getLanguageManager();
	public abstract PlaceholderManager getPlaceholderManager();
	public abstract PluginPropertiesManager getPluginPropertiesManager();
	public abstract MetricsManager getMetricsManager();
	public abstract CommandManager getCommandManager();
}
