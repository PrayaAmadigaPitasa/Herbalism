package com.praya.herbalism.manager.plugin;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.bukkit.entity.Player;

import com.praya.agarthalib.utility.ListUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.PluginUtil;
import com.praya.agarthalib.utility.TextUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.crops.CropsPlant;
import com.praya.herbalism.crops.CropsProperties;
import com.praya.herbalism.crops.CropsRequirement;
import com.praya.herbalism.fertilizer.FertilizerEffect;
import com.praya.herbalism.fertilizer.FertilizerProperties;
import com.praya.herbalism.handler.HandlerManager;
import com.praya.herbalism.item.ItemRequirement;
import com.praya.herbalism.manager.game.CropsPropertiesManager;
import com.praya.herbalism.manager.game.FertilizerManager;
import com.praya.herbalism.manager.game.GameManager;
import com.praya.herbalism.manager.player.PlayerHerbalismManager;
import com.praya.herbalism.manager.player.PlayerManager;
import com.praya.herbalism.placeholder.replacer.ReplacerMVDWPlaceholderAPI;
import com.praya.herbalism.placeholder.replacer.ReplacerPlaceholderAPI;
import com.praya.herbalism.player.PlayerHerbalism;
import com.praya.herbalism.util.TimeUtil;

public abstract class PlaceholderManager extends HandlerManager {
	
	protected PlaceholderManager(Herbalism plugin) {
		super(plugin);
	};
	
	protected abstract HashMap<String, String> getMapPlaceholder();
	public abstract Collection<String> getPlaceholderIds();
	public abstract Collection<String> getPlaceholders();
	public abstract String getPlaceholder(String id);
	
	public final boolean isPlaceholderExists(String id) {
		return getPlaceholder(id) != null;
	}
	
	public final void registerAll() {
		final String placeholder = plugin.getPluginPlaceholder();
		
		if (PluginUtil.isPluginInstalled("PlaceholderAPI")) {
			new ReplacerPlaceholderAPI(plugin, placeholder).hook();
		}
		
		if (PluginUtil.isPluginInstalled("MVdWPlaceholderAPI")) {
			new ReplacerMVDWPlaceholderAPI(plugin, placeholder).register();
		}
	}
	
	public final List<String> localPlaceholder(List<String> list) {
		final String divider = "\n";
		final String builder = TextUtil.convertListToString(list, divider);
		final String text = localPlaceholder(builder);
		
		return ListUtil.convertStringToList(text, divider);
	}
	
	public final String localPlaceholder(String text) {
		return TextUtil.placeholder(getMapPlaceholder(), text);
	}
	
	public final List<String> pluginPlaceholder(List<String> list, String... identifiers) {
		return pluginPlaceholder(list, null, identifiers);
	}
	
	public final List<String> pluginPlaceholder(List<String> list, Player player, String... identifiers) {
		final String divider = "\n";
		final String builder = TextUtil.convertListToString(list, divider);
		final String text = pluginPlaceholder(builder, player, identifiers);
		
		return ListUtil.convertStringToList(text, divider);
	}
	
	public final String pluginPlaceholder(String text, String... identifiers) {
		return pluginPlaceholder(text, null, identifiers);
	}
	
	public final String pluginPlaceholder(String text, Player player, String... identifiers) {
		final HashMap<String, String> map = getMapPluginPlaceholder(player, identifiers);
		
		return TextUtil.placeholder(map, text);
	}
	
	public final HashMap<String, String> getMapPluginPlaceholder(String... identifiers) {
		return getMapPluginPlaceholder(null, identifiers);
	}
	
	public final HashMap<String, String> getMapPluginPlaceholder(Player player, String... identifiers) {
		final String placeholder = plugin.getPluginPlaceholder();
		final HashMap<String, String> map = new HashMap<String, String>();
		
		for (String identifier : identifiers) {
			final String replacement = getReplacement(player, identifier);
			
			if (replacement != null) {
				final String key = placeholder + "_" + identifier;
				
				map.put(key, replacement);
			}
		}
		
		return map;
	}
	
	public final String getReplacement(Player player, String identifier) {
		final GameManager gameManager = plugin.getGameManager();
		final PlayerManager playerManager = plugin.getPlayerManager();
		final CropsPropertiesManager cropsPropertiesManager = gameManager.getCropsPropertiesManager();
		final FertilizerManager fertilizerManager = gameManager.getFertilizerManager();
		final PlayerHerbalismManager playerHerbalismManager = playerManager.getPlayerHerbalismManager();
		final String[] parts = identifier.split(":");
        final int length = parts.length;
        
        if (length > 0) {
        	final String key = parts[0];
        	
        	if (key.equalsIgnoreCase("player") || key.equalsIgnoreCase("player_progress") || key.equalsIgnoreCase("player_herbalism")) {
        		final PlayerHerbalism playerHerbalism = playerHerbalismManager.getPlayerHerbalism(player);
        		
        		if (playerHerbalism != null && length > 1) {
        			final String mainData = parts[1];
        			
        			if (mainData.equalsIgnoreCase("level")) {
        				final int level = playerHerbalism.getLevel();
        				
        				return String.valueOf(level);
        			} else if (mainData.equalsIgnoreCase("exp")) {
        				final double exp = MathUtil.roundNumber(playerHerbalism.getExp());
        				
        				return String.valueOf(exp);
        			} else if (mainData.equalsIgnoreCase("exp_to_up")) {
        				final double expToUp = MathUtil.roundNumber(playerHerbalism.getExpToUp());
        				
        				return String.valueOf(expToUp);
        			} else if (mainData.equalsIgnoreCase("max_crops")) {
        				final int maxCrops = playerHerbalism.getMaxCrops();
        				
        				return String.valueOf(maxCrops);
        			} else if (mainData.equalsIgnoreCase("total_crops")) {
        				final int totalCrops = playerHerbalism.getTotalCrops();
        				
        				return String.valueOf(totalCrops);
        			} else if (mainData.equalsIgnoreCase("available_crops")) {
        				final int availableCrops = playerHerbalism.getAvailableCrops();
        				
        				return String.valueOf(availableCrops);
        			} else if (mainData.equalsIgnoreCase("total_unlocked_ability")) {
        				final int totalUnlockedAbility = playerHerbalism.getUnlockedAbilities().size();
        				
        				return String.valueOf(totalUnlockedAbility);
        			} else if (mainData.equalsIgnoreCase("total_unlocked_crops")) {
        				final int totalUnlockedCrops = playerHerbalism.getUnlockedCrops().size();
        				
        				return String.valueOf(totalUnlockedCrops);
        			} else if (mainData.equalsIgnoreCase("total_unlocked_fertilizer")) {
        				final int totalUnlockedFertilizer = playerHerbalism.getUnlockedFertilizer().size();
        				
        				return String.valueOf(totalUnlockedFertilizer);
        			}
        		}
        	} else if (key.equalsIgnoreCase("crops")) {
        		if (length > 1) {
        			final String crops = parts[1];
        			final CropsProperties cropsProperties = cropsPropertiesManager.getCropsProperties(crops);
        			
        			if (cropsProperties != null && length > 2) {
        				final String mainData = parts[2];
        				
        				if (mainData.equalsIgnoreCase("name")) {
        					final String name = cropsProperties.getName();
        					
        					return name;
        				} else if (mainData.equalsIgnoreCase("category")) {
        					final String category = cropsProperties.getCategory();
        					
        					return category;
        				} else if (mainData.equalsIgnoreCase("duration")) {
        					final int duration = cropsProperties.getDuration();
        					final String textDuration = TimeUtil.getTime(player, duration);
        					
        					return textDuration;
        				} else if (mainData.equalsIgnoreCase("plant")) {
        					final CropsPlant cropsPlant = cropsProperties.getPlant();
        					final String textPlant = TextUtil.toTitleCase(cropsPlant.toString().replace("_", " "));
        					
        					return textPlant;
        				} else if (mainData.equalsIgnoreCase("requirement")) {
        					final CropsRequirement cropsRequirement = cropsProperties.getCropsRequirement();
        					
        					if (cropsRequirement != null && length > 3) {
        						final String requirement = parts[3];
        						
        						if (requirement.equalsIgnoreCase("level")) {
        							final int level = cropsRequirement.getLevel();
        							
        							return String.valueOf(level);
        						} else if (requirement.equalsIgnoreCase("permission")) {
        							final String permission = cropsRequirement.getPermission();
        							
        							return permission != null ? permission : getPlaceholder("none");
        						}
        					}
        				}
        			}
        		}
        	} else if (key.equalsIgnoreCase("fertilizer")) {
        		if (length > 1) {
        			final String fertilizer = parts[1];
        			final FertilizerProperties fertilizerProperties = fertilizerManager.getFertilizer(fertilizer);
        			
        			if (fertilizerProperties != null && length > 2) {
        				final String mainData = parts[2];
        				
        				if (mainData.equalsIgnoreCase("price")) {
        					final double price = fertilizerProperties.getPrice();
        					
        					return String.valueOf(price);
        				} else if (mainData.equalsIgnoreCase("exp") || mainData.equalsIgnoreCase("experience")) {
        					final double exp = fertilizerProperties.getExp();
        					
        					return String.valueOf(exp);
        				} else if (mainData.equalsIgnoreCase("requirement")) {
        					final ItemRequirement itemRequirement = fertilizerProperties.getRequirement();
        					
        					if (itemRequirement != null && length > 3) {
        						final String requirement = parts[3];
        						
        						if (requirement.equalsIgnoreCase("level")) {
        							final int level = itemRequirement.getLevel();
        							
        							return String.valueOf(level);
        						} else if (requirement.equalsIgnoreCase("permission")) {
        							final String permission = itemRequirement.getPermission();
        							
        							return permission != null ? permission : getPlaceholder("none");
        						}
        					}
        				} else if (mainData.equalsIgnoreCase("effect")) {
        					final FertilizerEffect fertilizerEffect = fertilizerProperties.getEffect();
        					
        					if (fertilizerEffect != null && length > 3) {
        						final String effect = parts[3];
        						
        						if (effect.equalsIgnoreCase("growth_additional") || effect.equalsIgnoreCase("additional")) {
        							final double growthAdditional = fertilizerEffect.getGrowthAdditional();
        							final String textGrowhAddition = TimeUtil.getTime(player, (int) growthAdditional);
        							
        							return String.valueOf(textGrowhAddition);
        						} else if (effect.equalsIgnoreCase("growth_percent") || effect.equalsIgnoreCase("percent")) {
        							final double growthPercent = fertilizerEffect.getGrowthPercent();
        							
        							return String.valueOf(growthPercent + "%");
        						}
        					}
        				}
        			}
        		}
        	}
    	}
        
        return null;
	}
}