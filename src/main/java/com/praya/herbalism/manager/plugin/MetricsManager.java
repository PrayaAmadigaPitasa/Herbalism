package com.praya.herbalism.manager.plugin;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.handler.HandlerManager;
import com.praya.herbalism.metrics.service.BStats;

public abstract class MetricsManager extends HandlerManager {
	
	protected MetricsManager(Herbalism plugin) {
		super(plugin);
	}
	
	public abstract BStats getMetricsBStats();
}
