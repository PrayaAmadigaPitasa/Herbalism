package com.praya.herbalism.manager.player;

import java.util.Collection;
import java.util.UUID;

import org.bukkit.OfflinePlayer;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.handler.HandlerManager;
import com.praya.herbalism.player.PlayerHerbalism;

public abstract class PlayerHerbalismManager extends HandlerManager {
	
	protected PlayerHerbalismManager(Herbalism plugin) {
		super(plugin);
	}
	
	public abstract Collection<UUID> getPlayerIds();
	public abstract Collection<PlayerHerbalism> getAllPlayerHerbalism();
	public abstract PlayerHerbalism getPlayerHerbalism(UUID playerId);
	public abstract boolean removeFromCache(UUID playerId);
	
	public final PlayerHerbalism getPlayerHerbalism(OfflinePlayer player) {
		return player != null ? getPlayerHerbalism(player.getUniqueId()) : null;
	}
	
	public final boolean removeFromCache(OfflinePlayer player) {
		return player != null ? removeFromCache(player.getUniqueId()) : false;
	}
}