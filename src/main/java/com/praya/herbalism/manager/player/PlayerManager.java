package com.praya.herbalism.manager.player;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.handler.HandlerManager;

public abstract class PlayerManager extends HandlerManager {
	
	protected PlayerManager(Herbalism plugin) {
		super(plugin);
	}
	
	public abstract PlayerCropsHologramManager getPlayerCropsHologramManager();
	public abstract PlayerHerbalismManager getPlayerHerbalismManager();
}