package com.praya.herbalism.manager.player;

import java.util.Collection;
import java.util.UUID;

import org.bukkit.OfflinePlayer;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.crops.CropsHologram;
import com.praya.herbalism.handler.HandlerManager;

public abstract class PlayerCropsHologramManager extends HandlerManager {
	
	protected PlayerCropsHologramManager(Herbalism plugin) {
		super(plugin);
	}
	
	public abstract Collection<UUID> getPlayerIds();
	public abstract Collection<CropsHologram> getAllPlayerCropsHologram();
	public abstract CropsHologram getPlayerCropsHologram(UUID playerId);
	
	public final CropsHologram getPlayerCropsHologram(OfflinePlayer player) {
		if (player != null) {
			final UUID playerId = player.getUniqueId();
			
			return getPlayerCropsHologram(playerId);
		} else {
			return null;
		}
	}
	
	public final boolean isExists(UUID playerId) {
		return getPlayerCropsHologram(playerId) != null;
	}
	
	public final boolean isExists(OfflinePlayer player) {
		return getPlayerCropsHologram(player) != null;
	}
}
