package com.praya.herbalism.manager.task;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.handler.HandlerManager;

public abstract class TaskPlayerHerbalismManager extends HandlerManager {
	
	protected TaskPlayerHerbalismManager(Herbalism plugin) {
		super(plugin);
	}
	
	public abstract void reloadTaskPlayerHerbalismDatabase();
	
	public final void reloadAllTask() {
		reloadTaskPlayerHerbalismDatabase();
	}
}
