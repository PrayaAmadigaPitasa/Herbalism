package com.praya.herbalism.manager.task;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.handler.HandlerManager;

public abstract class TaskCropsBlockManager extends HandlerManager {
	
	protected TaskCropsBlockManager(Herbalism plugin) {
		super(plugin);
	}
	
	public abstract void reloadTaskCropsBlockDatabase();
	public abstract void reloadTaskCropsBlockUpdate();
	
	public final void reloadAllTask() {
		reloadTaskCropsBlockDatabase();
		reloadTaskCropsBlockUpdate();
	}
}