package com.praya.herbalism.manager.task;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.handler.HandlerManager;

public abstract class TaskManager extends HandlerManager {
	
	protected TaskManager(Herbalism plugin) {
		super(plugin);
	}
	
	public abstract TaskCropsBlockManager getTaskCropsBlockManager();
	public abstract TaskCropsHologramManager getTaskCropsHologramManager();
	public abstract TaskPlayerBossBarManager getTaskPlayerBossBarManager();
	public abstract TaskPlayerHerbalismManager getTaskPlayerHerbalismManager();
}
