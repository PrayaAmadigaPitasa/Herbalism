package com.praya.herbalism.manager.task;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.handler.HandlerManager;

public abstract class TaskCropsHologramManager extends HandlerManager {
	
	protected TaskCropsHologramManager(Herbalism plugin) {
		super(plugin);
	}

	public abstract void reloadTaskCropsHologram();
	
	public final void reloadAllTask() {
		reloadTaskCropsHologram();
	}
}
