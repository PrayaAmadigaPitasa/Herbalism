package com.praya.herbalism.manager.task;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.handler.HandlerManager;

public abstract class TaskPlayerBossBarManager extends HandlerManager {
	
	protected TaskPlayerBossBarManager(Herbalism plugin) {
		super(plugin);
	}
	
	public abstract void reloadTaskPlayerBossBar();
	
	public final void reloadAllTask() {
		reloadTaskPlayerBossBar();
	}
}