package com.praya.herbalism.manager.game;

import java.util.Collection;
import org.bukkit.block.Block;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.crops.CropsHologram;
import com.praya.herbalism.handler.HandlerManager;

public abstract class CropsHologramManager extends HandlerManager {
	
	protected CropsHologramManager(Herbalism plugin) {
		super(plugin);
	}
	
	public abstract Collection<Block> getAllBlocks();
	public abstract Collection<CropsHologram> getAllCropsHologram();
	public abstract CropsHologram getCropsHologram(Block block);
	
	public final boolean isCropHologramExists(Block block) {
		return getCropsHologram(block) != null;
	}
}
