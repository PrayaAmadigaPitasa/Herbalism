package com.praya.herbalism.manager.game;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.handler.HandlerManager;
import com.praya.herbalism.menu.gui.MenuHerbalism;

public abstract class MenuManager extends HandlerManager {
	
	protected MenuManager(Herbalism plugin) {
		super(plugin);
	}
	
	public abstract MenuHerbalism getMenuHerbalism();
}