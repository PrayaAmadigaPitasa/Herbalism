package com.praya.herbalism.manager.game;

import java.util.Collection;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.handler.HandlerManager;
import com.praya.herbalism.tabcompleter.TabCompleterTree;

public abstract class TabCompleterTreeManager extends HandlerManager {
	
	protected TabCompleterTreeManager(Herbalism plugin) {
		super(plugin);
	}
	
	public abstract Collection<String> getCommands();
	public abstract Collection<TabCompleterTree> getAllTabCompleterTree();
	public abstract TabCompleterTree getTabCompleterTree(String command);
}
