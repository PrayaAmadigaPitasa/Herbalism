package com.praya.herbalism.manager.game;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.command.CommandArgument;
import com.praya.herbalism.command.CommandTree;
import com.praya.herbalism.handler.HandlerManager;

public abstract class CommandTreeManager extends HandlerManager {
	
	protected CommandTreeManager(Herbalism plugin) {
		super(plugin);
	}
	
	public abstract Collection<String> getCommands();
	public abstract Collection<CommandTree> getAllCommandTree();
	public abstract CommandTree getCommandTree(String command);
	
	public final List<CommandArgument> getAllCommandArgument() {
		final List<CommandArgument> allCommandArgument = new ArrayList<CommandArgument>();
		
		for (CommandTree commandTree : getAllCommandTree()) {
			for (CommandArgument commandArgument : commandTree.getAllCommandArgument()) {
				allCommandArgument.add(commandArgument);
			}
		}
		
		return allCommandArgument;
	}
}
