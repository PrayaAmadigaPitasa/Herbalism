package com.praya.herbalism.manager.game;

import java.util.Collection;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.ability.Ability;
import com.praya.herbalism.handler.HandlerManager;

public abstract class AbilityManager extends HandlerManager {
	
	protected AbilityManager(Herbalism plugin) {
		super(plugin);
	}
	
	public abstract Collection<String> getAbilityIds();
	public abstract Collection<Ability> getAllAbility();
	public abstract Ability getAbility(String ability);
	
	public final boolean isRegistered(String ability) {
		return getAbility(ability) != null;
	}
	
	public final boolean isRegistered(Ability ability) {
		return ability != null ? getAllAbility().contains(ability) : false;
	}
}
