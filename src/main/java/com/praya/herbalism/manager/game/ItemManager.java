package com.praya.herbalism.manager.game;

import java.util.ArrayList;
import java.util.Collection;
import org.bukkit.inventory.ItemStack;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.handler.HandlerManager;
import com.praya.herbalism.item.ItemProperties;

public abstract class ItemManager extends HandlerManager {
	
	protected ItemManager(Herbalism plugin) {
		super(plugin);
	}	
	
	public abstract Collection<String> getItemIds();
	public abstract Collection<ItemProperties> getAllItemProperties();
	public abstract ItemProperties getItemProperties(String id);
	
	public final ItemProperties getItemProperties(ItemStack item) {
		if (item != null) {
			for (ItemProperties key : getAllItemProperties()) {
				if (key.getItem().isSimilar(item)) {
					return key;
				}
			}
		}
		
		return null;
	}
	
	public final boolean isExists(String id) {
		return getItemProperties(id) != null;
	}
	
	public final boolean isExists(ItemStack item) {
		return getItemProperties(item) != null;
	}
	
	public final Collection<String> getAllCategory() {
		final Collection<String> allCategory = new ArrayList<String>();
		
		for (ItemProperties itemProperties : getAllItemProperties()) {
			final String itemCategory = itemProperties.getCategory();
			
			label_category: {
				for (String category : allCategory) {
					if (category.equalsIgnoreCase(itemCategory)) {
						break label_category;
					}
				}
				
				allCategory.add(itemCategory);
			}
		}
		
		return allCategory;
	}
	
	public final boolean isCategoryExists(String category) {
		if (category != null) {
			for (String key : getAllCategory()) {
				if (key.equalsIgnoreCase(category)) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	public final Collection<ItemProperties> getAllItemProperties(String category) {
		final Collection<ItemProperties> allItemProperties = new ArrayList<ItemProperties>();
		
		if (category != null) {
			for (ItemProperties itemProperties : getAllItemProperties()) {
				if (itemProperties.getCategory().equalsIgnoreCase(category)) {
					allItemProperties.add(itemProperties);
				}
			}
		}
		
		return allItemProperties;
	}
}
