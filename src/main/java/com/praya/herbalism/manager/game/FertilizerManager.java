package com.praya.herbalism.manager.game;

import java.util.Collection;

import org.bukkit.inventory.ItemStack;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.fertilizer.FertilizerProperties;
import com.praya.herbalism.handler.HandlerManager;

public abstract class FertilizerManager extends HandlerManager {
	
	protected FertilizerManager(Herbalism plugin) {
		super(plugin);
	}
	
	public abstract Collection<String> getFertilizerIds();
	public abstract Collection<FertilizerProperties> getAllFertilizer();
	public abstract FertilizerProperties getFertilizer(String id);
	public abstract FertilizerProperties getFertilizer(ItemStack item);
	
	public final boolean isExists(String id) {
		return getFertilizer(id) != null;
	}
	
	public final boolean isExists(ItemStack item) {
		return getFertilizer(item) != null;
	}
}
