package com.praya.herbalism.manager.game;

import java.util.Collection;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.ability.AbilityProperties;
import com.praya.herbalism.handler.HandlerManager;

public abstract class AbilityPropertiesManager extends HandlerManager {
	
	protected AbilityPropertiesManager(Herbalism plugin) {
		super(plugin);
	}
	
	public abstract Collection<String> getAbilityPropertyIds();
	public abstract Collection<AbilityProperties> getAllAbilityProperties();
	public abstract AbilityProperties getAbilityProperties(String ability);
	
	public final boolean isExists(String ability) {
		return getAbilityProperties(ability) != null;
	}
}
