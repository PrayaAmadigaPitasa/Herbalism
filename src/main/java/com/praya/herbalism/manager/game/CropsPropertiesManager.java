package com.praya.herbalism.manager.game;

import java.util.ArrayList;
import java.util.Collection;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.crops.CropsProperties;
import com.praya.herbalism.handler.HandlerManager;

public abstract class CropsPropertiesManager extends HandlerManager {
	
	protected CropsPropertiesManager(Herbalism plugin) {
		super(plugin);
	}
	public abstract Collection<String> getCropsIds();
	public abstract Collection<CropsProperties> getAllCropsProperties();
	public abstract CropsProperties getCropsProperties(String crops);
	
	public final boolean isExists(String crops) {
		return getCropsProperties(crops) != null;
	}
	
	public final Collection<String> getAllCategory() {
		final Collection<String> allCategory = new ArrayList<String>();
		
		for (CropsProperties cropsProperties : getAllCropsProperties()) {
			final String cropsCategory = cropsProperties.getCategory();
			
			label_category: {
				for (String category : allCategory) {
					if (category.equalsIgnoreCase(cropsCategory)) {
						break label_category;
					}
				}
				
				allCategory.add(cropsCategory);
			}
		}
		
		return allCategory;
	}
	
	public final boolean isCategoryExists(String category) {
		if (category != null) {
			for (String key : getAllCategory()) {
				if (key.equalsIgnoreCase(category)) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	public final Collection<CropsProperties> getAllCropsProperties(String category) {
		final Collection<CropsProperties> allCropsProperties = new ArrayList<CropsProperties>();
		
		if (category != null) {
			for (CropsProperties cropsProperties : getAllCropsProperties()) {
				if (cropsProperties.getCategory().equalsIgnoreCase(category)) {
					allCropsProperties.add(cropsProperties);
				}
			}
		}
		
		return allCropsProperties;
	}
}
