package com.praya.herbalism.manager.game;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.handler.HandlerManager;

public abstract class GameManager extends HandlerManager {

	protected GameManager(Herbalism plugin) {
		super(plugin);
	}
	
	public abstract AbilityManager getAbilityManager();
	public abstract AbilityPropertiesManager getAbilityPropertiesManager();
	public abstract ItemManager getItemManager();
	public abstract FertilizerManager getFertilizerManager();
	public abstract CropsPropertiesManager getCropsPropertiesManager();
	public abstract CropsBlockManager getCropsBlockManager();
	public abstract CropsHologramManager getCropsHologramManager();
	public abstract MenuManager getMenuManager();
	public abstract CommandTreeManager getCommandTreeManager();
	public abstract TabCompleterTreeManager getTabCompleterTreeManager();
}
