package com.praya.herbalism.manager.game;

import java.util.Collection;
import org.bukkit.block.Block;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.crops.CropsBlock;
import com.praya.herbalism.handler.HandlerManager;

public abstract class CropsBlockManager extends HandlerManager {
	
	protected CropsBlockManager(Herbalism plugin) {
		super(plugin);
	}
	
	public abstract Collection<Block> getBlocks();
	public abstract Collection<CropsBlock> getAllCropsBlock();
	public abstract CropsBlock getCropsBlock(Block block);
	
	public final boolean isSet(Block block) {
		return getCropsBlock(block) != null;
	}
}
