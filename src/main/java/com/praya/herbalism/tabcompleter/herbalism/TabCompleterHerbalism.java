package com.praya.herbalism.tabcompleter.herbalism;

import com.praya.herbalism.tabcompleter.TabCompleterArgument;
import com.praya.herbalism.tabcompleter.TabCompleterTree;

public final class TabCompleterHerbalism extends TabCompleterTree {

	private static final String COMMAND = "Herbalism";
	
	private TabCompleterHerbalism() {
		super(COMMAND);
		
		final TabCompleterArgument tabCompleterArgumentExp = TabCompleterHerbalismExp.getInstance();
		final TabCompleterArgument tabCompleterArgumentHelp = TabCompleterHerbalismHelp.getInstance();
		final TabCompleterArgument tabCompleterArgumentLevel = TabCompleterHerbalismLevel.getInstance();
		final TabCompleterArgument tabCompleterArgumentLoad = TabCompleterHerbalismLoad.getInstance();
		final TabCompleterArgument tabCompleterArgumentMenu = TabCompleterHerbalismMenu.getInstance();
		
		register(tabCompleterArgumentExp);
		register(tabCompleterArgumentHelp);
		register(tabCompleterArgumentLevel);
		register(tabCompleterArgumentLoad);
		register(tabCompleterArgumentMenu);
	}
	
	private static class CommandHerbalismSingleton {
		private static final TabCompleterHerbalism instance = new TabCompleterHerbalism();
	}
	
	public static final TabCompleterHerbalism getInstance() {
		return CommandHerbalismSingleton.instance;
	}
}