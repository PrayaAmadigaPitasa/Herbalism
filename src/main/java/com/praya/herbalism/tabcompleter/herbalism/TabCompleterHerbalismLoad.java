package com.praya.herbalism.tabcompleter.herbalism;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.command.Command;
import com.praya.herbalism.manager.game.FertilizerManager;
import com.praya.herbalism.manager.game.GameManager;
import com.praya.herbalism.manager.game.ItemManager;
import com.praya.herbalism.tabcompleter.TabCompleterArgument;

public final class TabCompleterHerbalismLoad extends TabCompleterArgument {

	private static final Command COMMAND = Command.HERBALISM_LOAD;
	
	private TabCompleterHerbalismLoad(Plugin plugin, String mainArgument) {
		super(plugin, mainArgument);
	}
	
	private static class TabCompleterHerbalismMenuSingleton {
		private static final TabCompleterHerbalismLoad instance;
		
		static {
			final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
			final String mainArgument = COMMAND.getMain();
			
			instance = new TabCompleterHerbalismLoad(plugin, mainArgument);
		}
	}
	
	protected static final TabCompleterHerbalismLoad getInstance() {
		return TabCompleterHerbalismMenuSingleton.instance;
	}

	@Override
	public List<String> execute(CommandSender sender, String[] args) {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final GameManager gameManager = plugin.getGameManager();
		final ItemManager itemManager = gameManager.getItemManager();
		final FertilizerManager fertilizerManager = gameManager.getFertilizerManager();
		final List<String> tabList = new ArrayList<String>();
		
		if (args.length == 2) {
			tabList.add("Harvest");
			tabList.add("Fertilizer");
		} else if (args.length == 3) {
			final String type = args[1];
			
			if (type.equalsIgnoreCase("Harvest") || type.equalsIgnoreCase("Item")) {
				tabList.addAll(itemManager.getItemIds());
			} else if (type.equalsIgnoreCase("Fertilizer")) {
				tabList.addAll(fertilizerManager.getFertilizerIds());
			}
		}
		
		return tabList;
	}
}
