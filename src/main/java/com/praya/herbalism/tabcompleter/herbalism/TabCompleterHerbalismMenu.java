package com.praya.herbalism.tabcompleter.herbalism;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.command.Command;
import com.praya.herbalism.tabcompleter.TabCompleterArgument;

public final class TabCompleterHerbalismMenu extends TabCompleterArgument {

	private static final Command COMMAND = Command.HERBALISM_MENU;
	
	private TabCompleterHerbalismMenu(Plugin plugin, String mainArgument) {
		super(plugin, mainArgument);
	}
	
	private static class TabCompleterHerbalismMenuSingleton {
		private static final TabCompleterHerbalismMenu instance;
		
		static {
			final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
			final String mainArgument = COMMAND.getMain();
			
			instance = new TabCompleterHerbalismMenu(plugin, mainArgument);
		}
	}
	
	protected static final TabCompleterHerbalismMenu getInstance() {
		return TabCompleterHerbalismMenuSingleton.instance;
	}

	@Override
	public List<String> execute(CommandSender sender, String[] args) {
		final List<String> tabList = new ArrayList<String>();
		
		if (args.length == 2) {
			tabList.add("Home");
			tabList.add("Player");
			tabList.add("Progress");
			tabList.add("Progress_Ability");
			tabList.add("Progress_Crops");
			tabList.add("Progress_Fertilizer");
			tabList.add("Shop");
			tabList.add("Shop_Harvest_Buy");
			tabList.add("Shop_Harvest_Sell");
			tabList.add("Shop_Fertilizer_Buy");
		}
		
		return tabList;
	}
}
