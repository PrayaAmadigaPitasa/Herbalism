package com.praya.herbalism.tabcompleter.herbalism;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.command.Command;
import com.praya.herbalism.tabcompleter.TabCompleterArgument;

public final class TabCompleterHerbalismExp extends TabCompleterArgument {

	private static final Command COMMAND = Command.HERBALISM_EXP;
	
	private TabCompleterHerbalismExp(Plugin plugin, String mainArgument) {
		super(plugin, mainArgument);
	}
	
	private static class TabCompleterHerbalismMenuSingleton {
		private static final TabCompleterHerbalismExp instance;
		
		static {
			final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
			final String mainArgument = COMMAND.getMain();
			
			instance = new TabCompleterHerbalismExp(plugin, mainArgument);
		}
	}
	
	protected static final TabCompleterHerbalismExp getInstance() {
		return TabCompleterHerbalismMenuSingleton.instance;
	}

	@Override
	public List<String> execute(CommandSender sender, String[] args) {
		final List<String> tabList = new ArrayList<String>();
		
		if (args.length == 2) {
			tabList.add("Set");
			tabList.add("Add");
			tabList.add("Take");
		}
		
		return tabList;
	}
}
