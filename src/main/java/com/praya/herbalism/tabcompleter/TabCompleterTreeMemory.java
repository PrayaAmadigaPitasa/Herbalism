package com.praya.herbalism.tabcompleter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.manager.game.TabCompleterTreeManager;

public final class TabCompleterTreeMemory extends TabCompleterTreeManager {

	private final HashMap<String, TabCompleterTree> mapCommandTree = new HashMap<String, TabCompleterTree>();
	
	private TabCompleterTreeMemory(Herbalism plugin) {
		super(plugin);
		
		final TabCompleterTree tabCompleterHerbalism = TabCompleterTree.TAB_COMPLETER_HERBALISM;
		
		register(tabCompleterHerbalism);
	}
	
	private static class CommandTreeMemorySingleton {
		private static final TabCompleterTreeMemory instance;
		
		static {
			final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
			
			instance = new TabCompleterTreeMemory(plugin);
		}
	}
	
	public static final TabCompleterTreeMemory getInstance() {
		return CommandTreeMemorySingleton.instance;
	}
	
	@Override
	public final Collection<String> getCommands() {
		return getCommands(true);
	}
	
	protected final Collection<String> getCommands(boolean clone) {
		final Collection<String> commands = this.mapCommandTree.keySet();
		
		return clone ? new ArrayList<String>(commands) : commands;
	}
	
	@Override
	public final Collection<TabCompleterTree> getAllTabCompleterTree() {
		return this.mapCommandTree.values();
	}
	
	protected final Collection<TabCompleterTree> getAllTabCompleterTree(boolean clone) {
		final Collection<TabCompleterTree> allTabCompleterTree = this.mapCommandTree.values();
		
		return clone ? new ArrayList<TabCompleterTree>(allTabCompleterTree) : allTabCompleterTree;
	}
	
	@Override
	public final TabCompleterTree getTabCompleterTree(String command) {
		if (command != null) {
			for (String key : getCommands(false)) {
				if (key.equalsIgnoreCase(command)) {
					return this.mapCommandTree.get(key);
				}
			}
		}
		
		return null;
	}
	
	protected final boolean register(TabCompleterTree commandTree) {
		if (commandTree != null && !getAllTabCompleterTree(false).contains(commandTree)) {
			final String command = commandTree.getCommand();
			final PluginCommand pluginCommand = Bukkit.getPluginCommand(command);
			
			if (pluginCommand != null) {
			
				this.mapCommandTree.put(command, commandTree);
				
				pluginCommand.setTabCompleter(commandTree);
				return true;
			}
		}
		
		return false;
	}
}
