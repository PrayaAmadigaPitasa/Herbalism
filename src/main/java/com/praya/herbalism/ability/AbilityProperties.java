package com.praya.herbalism.ability;

public class AbilityProperties {

	private final String id;
	private final int maxGrade;
	private final int maxGradeLevel;
	private final AbilityPropertiesEffect effect;
	private final AbilityRequirement requirement;
	
	protected AbilityProperties(String id, int maxGrade, int maxGradeLevel, AbilityPropertiesEffect effect, AbilityRequirement requirement) {
		if (id == null) {
			throw new IllegalArgumentException();
		} else {
			this.id = id;
			this.maxGrade = maxGrade;
			this.maxGradeLevel = maxGradeLevel;
			this.effect = effect != null ? effect : new AbilityPropertiesEffect();
			this.requirement = requirement != null ? requirement : new AbilityRequirement();
		}
	}
	
	public final String getId() {
		return this.id;
	}
	
	public final int getMaxGrade() {
		return this.maxGrade;
	}
	
	public final int getMaxGradeLevel() {
		return this.maxGradeLevel;
	}
	
	public final AbilityPropertiesEffect getEffect() {
		return this.effect;
	}
	
	public final AbilityRequirement getRequirement() {
		return this.requirement;
	}
}
