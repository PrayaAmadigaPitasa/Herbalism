package com.praya.herbalism.ability;

import com.praya.agarthalib.utility.MathUtil;

public class AbilityPropertiesEffect {

	private double maxValue;
	private double baseValue;
	private double maxChance;
	private double baseChance;
	
	public AbilityPropertiesEffect() {
		this(0, 0, 0, 0);
	}
	
	public AbilityPropertiesEffect(double maxValue, double baseValue, double maxChance, double baseChance) {
		setMaxValue(maxValue);
		setBaseValue(baseValue);
		setMaxChance(maxChance);
		setBaseChance(baseChance);
	}
	
	public final double getMaxValue() {
		return this.maxValue;
	}
	
	public final double getBaseValue() {
		return this.baseValue;
	}
	
	public final double getMaxChance() {
		return this.maxChance;
	}
	
	public final double getBaseChance() {
		return this.baseChance;
	}
	
	public final void setMaxValue(double value) {
		this.maxValue = Math.max(0, value);
	}
	
	public final void setBaseValue(double value) {
		this.baseValue = Math.max(0, value);
	}
	
	public final void setMaxChance(double chance) {
		this.maxChance = MathUtil.limitDouble(chance, 0, 100);
	}
	
	public final void setBaseChance(double chance) {
		this.baseChance = MathUtil.limitDouble(chance, 0, 100);
	}
	
}
