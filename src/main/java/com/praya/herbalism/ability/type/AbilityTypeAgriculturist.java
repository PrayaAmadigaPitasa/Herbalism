package com.praya.herbalism.ability.type;

import java.util.HashMap;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import core.praya.agarthalib.enums.main.RomanNumber;

import com.praya.agarthalib.utility.TextUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.ability.AbilityPropertiesEffect;
import com.praya.herbalism.language.Language;
import com.praya.herbalism.ability.AbilityProperties;
import com.praya.herbalism.manager.game.AbilityPropertiesManager;
import com.praya.herbalism.manager.game.GameManager;
import com.praya.herbalism.manager.player.PlayerHerbalismManager;
import com.praya.herbalism.manager.player.PlayerManager;
import com.praya.herbalism.player.PlayerHerbalism;

public final class AbilityTypeAgriculturist extends AbilityType { 
	
	private static final String ID = "Agriculturist";
	
	private AbilityTypeAgriculturist(Plugin plugin, String id) {
		super(plugin, id);
	}
	
	private static class AbilityAgriculturistSingleton {
		private static final AbilityTypeAgriculturist instance;
		
		static {
			final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
			
			instance = new AbilityTypeAgriculturist(plugin, ID);
		}
	}
	
	public static final AbilityTypeAgriculturist getInstance() {
		return AbilityAgriculturistSingleton.instance;
	}

	@Override
	public String getName(Player player) {
		final int abilityGrade = getPlayerGrade(player);
		final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
		
		String abilityName = Language.ABILITY_NAME_AGRICULTURIST.getText(player);
		
		mapPlaceholder.put("ability_grade", abilityGrade > 0 ? " " + RomanNumber.getRomanNumber(abilityGrade) : "");
		
		abilityName = TextUtil.placeholder(mapPlaceholder, abilityName);
		
		return abilityName;
	}

	

	@Override
	public List<String> getDescription(Player player) {
		final double abilityEffectValue = getPlayerEffectValue(player);
		final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
		
		List<String> abilityDescription = Language.ABILITY_DESCRIPTION_AGRICULTURIST.getListText(player);
		
		mapPlaceholder.put("ability_effect_value", String.valueOf(abilityEffectValue));
		
		abilityDescription = TextUtil.placeholder(mapPlaceholder, abilityDescription);
		
		return abilityDescription;
	}
	
	public final int getMaxGrade() {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityPropertiesManager abilityPropertiesManager = gameManager.getAbilityPropertiesManager();
		final AbilityProperties abilityProperties = abilityPropertiesManager.getAbilityProperties(getId());
		
		return abilityProperties != null ? abilityProperties.getMaxGrade() : 1;
	}
	
	public final int getMaxGradeLevel() {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityPropertiesManager abilityPropertiesManager = gameManager.getAbilityPropertiesManager();
		final AbilityProperties abilityProperties = abilityPropertiesManager.getAbilityProperties(getId());
		
		return abilityProperties != null ? abilityProperties.getMaxGradeLevel() : 1;
	}
	
	public final int getPlayerGrade(Player player) {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerHerbalismManager playerHerbalismManager = playerManager.getPlayerHerbalismManager();
		
		if (player != null) {
			final PlayerHerbalism playerHerbalism = playerHerbalismManager.getPlayerHerbalism(player);
			final int abilityLevel = getRequirementLevel();
			final int playerLevel = playerHerbalism.getLevel();
			
			if (playerLevel >= abilityLevel) {
				final int maxGrade = getMaxGrade();
				final int maxGradeLevel = getMaxGradeLevel();
				final int grade = (int) (((double) (Math.min(maxGradeLevel, playerLevel) - abilityLevel) / (maxGradeLevel - abilityLevel) * (maxGrade - 1)) + 1);
				
				return grade;
			}			
		}
		
		return 0;
	}
	
	public final double getPlayerEffectValue(Player player) {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityPropertiesManager abilityPropertiesManager = gameManager.getAbilityPropertiesManager();
		final AbilityProperties abilityProperties = abilityPropertiesManager.getAbilityProperties(getId());
		
		if (player != null && abilityProperties != null) {
			final AbilityPropertiesEffect abilityEffect = abilityProperties.getEffect();
			final int grade = getPlayerGrade(player);
			
			if (grade > 0) {
				final int maxGrade = getMaxGrade();
				final double maxValue = abilityEffect.getMaxValue();
				
				if (maxGrade > 1) {
					final double baseValue = abilityEffect.getBaseValue();
					final double value = baseValue + (((maxValue - baseValue) * (grade - 1)) / (maxGrade - 1));
					
					return value;
				} else {
					return maxValue;
				}
			}
		}
		
		return 0;
	}
}
