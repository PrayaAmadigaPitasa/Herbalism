package com.praya.herbalism.ability.type;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.ability.Ability;
import com.praya.herbalism.ability.AbilityProperties;
import com.praya.herbalism.ability.AbilityRequirement;
import com.praya.herbalism.manager.game.AbilityPropertiesManager;
import com.praya.herbalism.manager.game.GameManager;

public abstract class AbilityType extends Ability {
	
	protected AbilityType(Plugin plugin, String id) {
		super(plugin, id);
	}
	
	@Override
	public String getRequirementPermission() {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityPropertiesManager abilityPropertiesManager = gameManager.getAbilityPropertiesManager();
		final AbilityProperties abilityProperties = abilityPropertiesManager.getAbilityProperties(getId());
		
		if (abilityProperties != null) {
			final AbilityRequirement requirement = abilityProperties.getRequirement();
			
			return requirement.getPermission();
		} else {
			return null;
		}
	}

	@Override
	public int getRequirementLevel() {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityPropertiesManager abilityPropertiesManager = gameManager.getAbilityPropertiesManager();
		final AbilityProperties abilityProperties = abilityPropertiesManager.getAbilityProperties(getId());
		
		if (abilityProperties != null) {
			final AbilityRequirement requirement = abilityProperties.getRequirement();
			
			return requirement.getLevel();
		} else {
			return 1;
		}
	}
}
