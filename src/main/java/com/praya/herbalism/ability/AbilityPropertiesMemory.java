package com.praya.herbalism.ability;

import java.util.ArrayList;
import java.util.Collection;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.ability.AbilityConfig;
import com.praya.herbalism.ability.AbilityProperties;
import com.praya.herbalism.manager.game.AbilityPropertiesManager;

public final class AbilityPropertiesMemory extends AbilityPropertiesManager {
	
	private final AbilityConfig abilityConfig;
	
	private AbilityPropertiesMemory(Herbalism plugin) {
		super(plugin);
		
		this.abilityConfig = new AbilityConfig(plugin);
	}
	
	private static class AbilityMemorySingleton {
		private static final AbilityPropertiesMemory instance;
		
		static {
			final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
			
			instance = new AbilityPropertiesMemory(plugin);
		}
	}
	
	public static final AbilityPropertiesMemory getInstance() {
		return AbilityMemorySingleton.instance;
	}
	
	protected final AbilityConfig getAbilityConfig() {
		return this.abilityConfig;
	}
	
	@Override
	public final Collection<String> getAbilityPropertyIds() {
		return getAbilityPropertyIds(true);
	}
	
	protected final Collection<String> getAbilityPropertyIds(boolean clone) {
		final Collection<String> abilityPropertyIds = getAbilityConfig().mapAbilityProperties.keySet();
		
		return clone ? new ArrayList<String>(abilityPropertyIds) : abilityPropertyIds;
	}
	
	@Override
	public final Collection<AbilityProperties> getAllAbilityProperties() {
		return getAllAbilityProperties(true);
	}
	
	protected final Collection<AbilityProperties> getAllAbilityProperties(boolean clone) {
		final Collection<AbilityProperties> allAbilityProperties = getAbilityConfig().mapAbilityProperties.values();
		
		return clone ? new ArrayList<AbilityProperties>(allAbilityProperties) : allAbilityProperties;
	}
	
	@Override
	public final AbilityProperties getAbilityProperties(String ability) {
		if (ability != null) {
			for (String key : getAbilityPropertyIds(false)) {
				if (key.equalsIgnoreCase(ability)) {
					return getAbilityConfig().mapAbilityProperties.get(key);
				}
			}
		}
		
		return null;
	}
}