package com.praya.herbalism.ability;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.bukkit.plugin.java.JavaPlugin;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.ability.type.AbilityTypeAgriculturist;
import com.praya.herbalism.ability.type.AbilityTypeCultivator;
import com.praya.herbalism.ability.type.AbilityTypeLuck;
import com.praya.herbalism.manager.game.AbilityManager;

public final class AbilityMemory extends AbilityManager {
	
	private final HashMap<String, Ability> mapAbility = new HashMap<String, Ability>();
	
	private AbilityMemory(Herbalism plugin) {
		super(plugin);
		
		final Ability abilityAgriculturist = AbilityTypeAgriculturist.getInstance();
		final Ability abilityCultivator = AbilityTypeCultivator.getInstance();
		final Ability abilityLuck = AbilityTypeLuck.getInstance();
		
		register(abilityAgriculturist);
		register(abilityCultivator);
		register(abilityLuck);
	}
	
	private static class AbilityMemorySingleton {
		private static final AbilityMemory instance;
		
		static {
			final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
			
			instance = new AbilityMemory(plugin);
		}
	}
	
	public static final AbilityMemory getInstance() {
		return AbilityMemorySingleton.instance;
	}
	
	@Override
	public final Collection<String> getAbilityIds() {
		return getAbilityIds(true);
	}
	
	protected final Collection<String> getAbilityIds(boolean clone) {
		final Collection<String> abilityIds = this.mapAbility.keySet();
		
		return clone ? new ArrayList<String>(abilityIds) : abilityIds;
	}
	
	@Override
	public final Collection<Ability> getAllAbility() {
		return getAllAbility(true);
	}
	
	protected final Collection<Ability> getAllAbility(boolean clone) {
		final Collection<Ability> allAbility = this.mapAbility.values();
		
		return clone ? new ArrayList<Ability>(allAbility) : allAbility;
	}
	
	@Override
	public final Ability getAbility(String ability) {
		if (ability != null) {
			for (String key : getAbilityIds(false)) {
				if (key.equalsIgnoreCase(ability)) {
					return this.mapAbility.get(key);
				}
			}
		}
		
		return null;
	}
	
	protected final boolean register(Ability ability) {
		if (ability != null) {
			final String abilityId = ability.getId();
			
			if (!isRegistered(abilityId)) {
				
				this.mapAbility.put(abilityId, ability);
				
				return true;
			}
		}
		
		return false;
	}
	
	protected final boolean unregister(Ability ability) {
		if (ability != null && getAllAbility(false).contains(ability)) {
			final String id = ability.getId();
			
			this.mapAbility.remove(id);
			
			return true;
		} else {
			return false;
		}
	}
}
