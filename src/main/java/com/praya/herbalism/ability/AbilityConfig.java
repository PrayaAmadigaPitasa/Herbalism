package com.praya.herbalism.ability;

import java.io.File;
import java.util.HashMap;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import com.praya.agarthalib.utility.FileUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.HerbalismConfig;
import com.praya.herbalism.handler.HandlerConfig;

public final class AbilityConfig extends HandlerConfig {

	private static final String PATH_FILE = "Configuration/ability.yml";
	
	protected final HashMap<String, AbilityProperties> mapAbilityProperties = new HashMap<String, AbilityProperties>();
	
	protected AbilityConfig(Herbalism plugin) {
		super(plugin);
		
		setup();
	}
	
	@Override
	public final void setup() {
		reset();
		loadConfig();
	}
	
	private final void reset() {
		this.mapAbilityProperties.clear();
	}
	
	private final void loadConfig() {
		final File file = FileUtil.getFile(plugin, PATH_FILE);
		final HerbalismConfig mainConfig = plugin.getMainConfig();
		
		if (!file.exists()) {
			FileUtil.saveResource(plugin, PATH_FILE);
		}
		
		final FileConfiguration config = FileUtil.getFileConfiguration(file);
		
		for (String key : config.getKeys(false)) {
			final ConfigurationSection abilityDataSection = config.getConfigurationSection(key);
			final AbilityPropertiesEffect effect = new AbilityPropertiesEffect();
			final AbilityRequirement requirement = new AbilityRequirement();
			
			int maxGrade = 1;
			int maxGradeLevel = mainConfig.getHerbalismMaxLevel();
			
			for (String abilityData : abilityDataSection.getKeys(false)) {
				if (abilityData.equalsIgnoreCase("Max_Grade")) {
					maxGrade = Math.max(1, abilityDataSection.getInt(abilityData));
				} else if (abilityData.equalsIgnoreCase("Max_Grade_Level")) {
					maxGradeLevel = Math.max(1, abilityDataSection.getInt(abilityData));
				} else if (abilityData.equalsIgnoreCase("Effect")) {
					final ConfigurationSection effectDataSection = abilityDataSection.getConfigurationSection(abilityData);
					
					double maxValue = 0;
					double baseValue = 0;
					double maxChance = 0;
					double baseChance = 0;
					
					for (String effectData : effectDataSection.getKeys(false)) {
						if (effectData.equalsIgnoreCase("Max_Value")) {
							maxValue = effectDataSection.getDouble(effectData);
						} else if (effectData.equalsIgnoreCase("Base_Value")) {
							baseValue = effectDataSection.getDouble(effectData);
						} else if (effectData.equalsIgnoreCase("Max_Chance")) {
							maxChance = effectDataSection.getDouble(effectData);
						} else if (effectData.equalsIgnoreCase("Base_Chance")) {
							baseChance = effectDataSection.getDouble(effectData);
						}
					}
					
					baseValue = Math.max(0, maxValue);
					maxValue = Math.max(baseValue, maxValue);
					baseChance = MathUtil.limitDouble(baseChance, 0, 100);
					maxChance = MathUtil.limitDouble(maxChance, baseChance, 100);
					
					effect.setMaxValue(maxValue);
					effect.setBaseValue(baseValue);
					effect.setMaxChance(maxChance);
					effect.setBaseChance(baseChance);
				} else if (abilityData.equalsIgnoreCase("Requirement")) {
					final ConfigurationSection requirementDataSection = abilityDataSection.getConfigurationSection(abilityData);
					
					for (String requirementData : requirementDataSection.getKeys(false)) {
						if (requirementData.equalsIgnoreCase("Permission")) {
							final String permission = requirementDataSection.getString(requirementData);
							
							requirement.setPermission(permission);
						} else if (requirementData.equalsIgnoreCase("Level")) {
							final int level = requirementDataSection.getInt(requirementData);
							
							requirement.setLevel(level);;
						}
					}
				}
			}
			
			final AbilityProperties abilityProperties = new AbilityProperties(key, maxGrade, maxGradeLevel, effect, requirement);
			
			this.mapAbilityProperties.put(key, abilityProperties);
		}
	}
}
