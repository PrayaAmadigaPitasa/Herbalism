package com.praya.herbalism.ability;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.SenderUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.manager.player.PlayerHerbalismManager;
import com.praya.herbalism.manager.player.PlayerManager;
import com.praya.herbalism.player.PlayerHerbalism;

public abstract class Ability {
	
	private final Plugin plugin;
	private final String id;
	
	public Ability(Plugin plugin, String id) {
		if (plugin == null || id == null) {
			throw new IllegalArgumentException();
		} else {
			this.plugin = plugin;
			this.id = id;
		}
	}
	
	public final Plugin getPlugin() {
		return this.plugin;
	}
	
	public final String getId() {
		return this.id;
	}
	
	public final String getName() {
		return getName(null);
	}
	
	public String getName(Player player) {
		return getId();
	}
	
	public final List<String> getDescription() {
		return getDescription(null);
	}
	
	public List<String> getDescription(Player player) {
		return null;
	}
	
	public String getRequirementPermission() {
		return null;
	}
	
	public int getRequirementLevel() {
		return 1;
	}
	
	public final boolean isRegistered() {
		final AbilityMemory abilityMemory = AbilityMemory.getInstance();
		
		return abilityMemory.isRegistered(this);
	}
	
	public final boolean register() {
		final AbilityMemory abilityMemory = AbilityMemory.getInstance();
		
		return abilityMemory.register(this);
	}
	
	public final boolean isAllowed(Player player) {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerHerbalismManager playerHerbalismManager = playerManager.getPlayerHerbalismManager();
		
		if (player != null) {
			final PlayerHerbalism playerHerbalism = playerHerbalismManager.getPlayerHerbalism(player);
			
			if (playerHerbalism != null) {
				final int playerLevel = playerHerbalism.getLevel();
				final boolean allowedLevel = getRequirementLevel() <= playerLevel;
				final boolean allowedPermission = SenderUtil.hasPermission(player, getRequirementPermission());
				
				return allowedLevel && allowedPermission;
			}
		}
		
		return false;
	}
}
