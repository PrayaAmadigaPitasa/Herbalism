package com.praya.herbalism.ability;

public class AbilityRequirement {

	private String permission;
	private int level;
	
	public AbilityRequirement() {
		this(null, 1);
	}
	
	public AbilityRequirement(String permission) {
		this(permission, 1);
	}
	
	public AbilityRequirement(String permission, int level) {
		setPermission(permission);
		setLevel(level);
	}
	
	public final String getPermission() {
		return this.permission;
	}
	
	public final int getLevel() {
		return this.level;
	}
	
	public final void setPermission(String permission) {
		this.permission = permission;
	}
	
	public final void setLevel(int level) {
		this.level = Math.max(0, level);
	}
}
