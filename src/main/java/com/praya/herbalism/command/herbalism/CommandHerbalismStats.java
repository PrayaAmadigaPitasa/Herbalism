package com.praya.herbalism.command.herbalism;

import java.util.HashMap;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.command.Command;
import com.praya.herbalism.command.CommandArgument;
import com.praya.herbalism.language.Language;
import com.praya.herbalism.manager.player.PlayerHerbalismManager;
import com.praya.herbalism.manager.player.PlayerManager;
import com.praya.herbalism.player.PlayerHerbalism;

import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;

public final class CommandHerbalismStats extends CommandArgument {

	private static final Command COMMAND = Command.HERBALISM_STATS;
	
	protected CommandHerbalismStats(Plugin plugin) {
		super(plugin, COMMAND.getMain(), COMMAND.getPermission(), COMMAND.getAliases());
	}
	
	@Override
	public String getDescription(CommandSender sender) {
		return Language.TOOLTIP_HERBALISM_ABOUT.getText();
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerHerbalismManager playerHerbalismManager = playerManager.getPlayerHerbalismManager();
		
		if (!(sender instanceof Player) && args.length < 2) {
			final String tooltip = TextUtil.getJsonTooltip(Language.TOOLTIP_HERBALISM_STATS.getText(sender));
			final MessageBuild message = Language.ARGUMENT_HERBALISM_STATS.getMessage(sender);
			
			message.sendMessage(sender, "tooltip_stats", tooltip);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else {
			final OfflinePlayer player;
			
			if (args.length > 1) {
				final String textPlayer = args[1];
				
				player = PlayerUtil.getPlayer(textPlayer);
				
				if (player == null) {
					final MessageBuild message = Language.PLAYER_NOT_EXISTS.getMessage(sender);
					
					message.sendMessage(sender, "player", textPlayer);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return;
				}
			} else {
				player = (OfflinePlayer) sender; 
			}
			
			final PlayerHerbalism playerHerbalism = playerHerbalismManager.getPlayerHerbalism(player);
			
			if (playerHerbalism == null) {
				final MessageBuild message = Language.ARGUMENT_DATABASE_PLAYER_NOT_REGISTERED.getMessage(sender);
				
				message.sendMessage(sender);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return;
			} else {
				final MessageBuild message = Language.HERBALISM_STATS.getMessage(sender);
				final String playerName = player.getName();
				final int playerLevel = playerHerbalism.getLevel();
				final int playerCrops = playerHerbalism.getTotalCrops();
				final int playerMaxCrops = playerHerbalism.getMaxCrops();
				final float playerExp = playerHerbalism.getExp();
				final float playerExpUp = playerHerbalism.getExpToUp();
				final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
				
				mapPlaceholder.put("player_name", playerName);
				mapPlaceholder.put("player_level", String.valueOf(playerLevel));
				mapPlaceholder.put("player_crops", String.valueOf(playerCrops));
				mapPlaceholder.put("player_max_crops", String.valueOf(playerMaxCrops));
				mapPlaceholder.put("player_exp", String.valueOf(MathUtil.roundNumber(playerExp, 1)));
				mapPlaceholder.put("player_exp_up", String.valueOf(MathUtil.roundNumber(playerExpUp, 1)));
				
				message.sendMessage(sender, mapPlaceholder);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
				return;
			}
		}
	}
}
