package com.praya.herbalism.command.herbalism;

import org.bukkit.plugin.java.JavaPlugin;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.command.CommandArgument;
import com.praya.herbalism.command.CommandTree;

public final class CommandHerbalism extends CommandTree {

	private static final String COMMAND = "Herbalism";
	private static final String DEFAULT_ARGUMENT = "Menu";
	
	private CommandHerbalism(Herbalism plugin) {
		super(COMMAND, DEFAULT_ARGUMENT);
		
		final CommandArgument commandArgumentAbout = new CommandHerbalismAbout(plugin);
		final CommandArgument commandArgumentExp = new CommandHerbalismExp(plugin);
		final CommandArgument commandArgumentHelp = new CommandHerbalismHelp(plugin);
		final CommandArgument commandArgumentLevel = new CommandHerbalismLevel(plugin);
		final CommandArgument commandArgumentLoad = new CommandHerbalismLoad(plugin);
		final CommandArgument commandArgumentMenu = new CommandHerbalismMenu(plugin);
		final CommandArgument commandArgumentReload = new CommandHerbalismReload(plugin);
		final CommandArgument commandArgumentStats = new CommandHerbalismStats(plugin);
		
		register(commandArgumentAbout);
		register(commandArgumentExp);
		register(commandArgumentHelp);
		register(commandArgumentLevel);
		register(commandArgumentLoad);
		register(commandArgumentMenu);
		register(commandArgumentReload);
		register(commandArgumentStats);
	}
	
	private static class CommandDreamFishSingleton {
		private static final CommandHerbalism INSTANCE;
		
		static {
			final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
			
			INSTANCE = new CommandHerbalism(plugin);
		}
	}
	
	public static final CommandHerbalism getInstance() {
		return CommandDreamFishSingleton.INSTANCE;
	}
}