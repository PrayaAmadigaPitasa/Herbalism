package com.praya.herbalism.command.herbalism;

import java.util.HashMap;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.command.Command;
import com.praya.herbalism.command.CommandArgument;
import com.praya.herbalism.language.Language;
import com.praya.herbalism.manager.plugin.PlaceholderManager;
import com.praya.herbalism.manager.plugin.PluginManager;
import com.praya.herbalism.manager.plugin.PluginPropertiesManager;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;

public final class CommandHerbalismAbout extends CommandArgument {

	private static final Command COMMAND = Command.HERBALISM_ABOUT;
	
	protected CommandHerbalismAbout(Plugin plugin) {
		super(plugin, COMMAND.getMain(), COMMAND.getPermission(), COMMAND.getAliases());
	}
	
	@Override
	public String getDescription(CommandSender sender) {
		return Language.TOOLTIP_HERBALISM_ABOUT.getText();
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final PluginManager pluginManager = plugin.getPluginManager();
		final PlaceholderManager placeholderManager = pluginManager.getPlaceholderManager();
		
		if (!COMMAND.checkPermission(sender)) {
			final String permission = COMMAND.getPermission();
			final MessageBuild message = Language.PERMISSION_LACK.getMessage(sender);;
			
			message.sendMessage(sender, "permission", permission);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else {
			final PluginPropertiesManager pluginPropertiesManager = pluginManager.getPluginPropertiesManager();
			final String prefix = placeholderManager.getPlaceholder("prefix") + " ";
			final String pluginType = plugin.getPluginType();
			final String pluginName = plugin.getPluginName();
			final String pluginVersion = plugin.getPluginVersion();
			final String pluginAuthor = pluginPropertiesManager.getPluginAuthor();
			final List<String> pluginDevelopers = pluginPropertiesManager.getPluginDevelopers();
			final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
			
			String aboutHeader = prefix + "&7=-=-=-=-=-=-= &6About&7 =-=-=-=-=-=-=";
			String aboutFooter = prefix + "&7=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=";
			String aboutBlank = prefix + "";
			String aboutPlugin = prefix + "Plugin&f: &c{plugin}";
			String aboutType = prefix + "Type&f: &c{type}";
			String aboutVersion = prefix + "Version&f: &c{version}";
			String aboutAuthor = prefix + "Author&f: &c{author}";
			
			mapPlaceholder.put("plugin", pluginName);
			mapPlaceholder.put("type", pluginType);
			mapPlaceholder.put("version", pluginVersion);
			mapPlaceholder.put("author", pluginAuthor);
			
			aboutHeader = TextUtil.placeholder(mapPlaceholder, aboutHeader);
			aboutFooter = TextUtil.placeholder(mapPlaceholder, aboutFooter);
			aboutPlugin = TextUtil.placeholder(mapPlaceholder, aboutPlugin);
			aboutType = TextUtil.placeholder(mapPlaceholder, aboutType);
			aboutVersion = TextUtil.placeholder(mapPlaceholder, aboutVersion);
			aboutAuthor = TextUtil.placeholder(mapPlaceholder, aboutAuthor);
			
			SenderUtil.sendMessage(sender, aboutHeader);
			SenderUtil.sendMessage(sender, aboutBlank);
			SenderUtil.sendMessage(sender, aboutPlugin);
			SenderUtil.sendMessage(sender, aboutType);
			SenderUtil.sendMessage(sender, aboutVersion);
			SenderUtil.sendMessage(sender, aboutAuthor);
			
			if (!pluginDevelopers.isEmpty()){
				final String aboutDeveloper = prefix + "Developer&7:";
				
				SenderUtil.sendMessage(sender, aboutDeveloper);
				
				for (String developer : pluginDevelopers) {
					
					String aboutListDeveloper = prefix + "&7&l➨ &d{developer}";
					
					mapPlaceholder.clear();
					mapPlaceholder.put("developer", developer);
					
					aboutListDeveloper = TextUtil.placeholder(mapPlaceholder, aboutListDeveloper);
					
					SenderUtil.sendMessage(sender, aboutListDeveloper);
				}
			}
			
			SenderUtil.sendMessage(sender, aboutBlank);
			SenderUtil.sendMessage(sender, aboutFooter);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
			return;
		}
	}
}
