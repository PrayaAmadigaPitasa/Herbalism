package com.praya.herbalism.command.herbalism;

import java.util.HashMap;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.HerbalismConfig;
import com.praya.herbalism.command.Command;
import com.praya.herbalism.command.CommandArgument;
import com.praya.herbalism.event.PlayerHerbalismLevelChangeEvent.LevelChangeReason;
import com.praya.herbalism.language.Language;
import com.praya.herbalism.manager.player.PlayerHerbalismManager;
import com.praya.herbalism.manager.player.PlayerManager;
import com.praya.herbalism.player.PlayerHerbalism;

import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;

public final class CommandHerbalismLevel extends CommandArgument {

	private static final Command COMMAND = Command.HERBALISM_LEVEL;
	
	protected CommandHerbalismLevel(Plugin plugin) {
		super(plugin, COMMAND.getMain(), COMMAND.getPermission(), COMMAND.getAliases());
	}
	
	@Override
	public String getDescription(CommandSender sender) {
		return Language.TOOLTIP_HERBALISM_ABOUT.getText();
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerHerbalismManager playerHerbalismManager = playerManager.getPlayerHerbalismManager();
		final HerbalismConfig mainConfig = plugin.getMainConfig();
		
		if (args.length < (sender instanceof Player ? 3 : 4)) {
			final String tooltip = TextUtil.getJsonTooltip(Language.TOOLTIP_HERBALISM_LEVEL.getText(sender));
			final MessageBuild message = Language.ARGUMENT_HERBALISM_LEVEL.getMessage(sender);
			
			message.sendMessage(sender, "tooltip_level", tooltip);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else {
			final Player target;
			
			if (args.length > 3) {
				final String textTarget = args[3];
				
				target = PlayerUtil.getOnlinePlayer(textTarget);
			} else {
				target = PlayerUtil.parse(sender);
			}
			
			if (target == null) {
				final MessageBuild message = Language.PLAYER_TARGET_OFFLINE.getMessage(sender);
				
				message.sendMessage(sender);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return;
			} else {
				final PlayerHerbalism playerHerbalism = playerHerbalismManager.getPlayerHerbalism(target);
				
				if (playerHerbalism == null) {
					final MessageBuild message = Language.ARGUMENT_DATABASE_PLAYER_NOT_REGISTERED.getMessage(sender);
					
					message.sendMessage(sender);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return;
				} else {
					final String textValue = args[2];
					
					if (!MathUtil.isNumber(textValue)) {
						final MessageBuild message = Language.ARGUMENT_INVALID_VALUE.getMessage(sender);
						
						message.sendMessage(sender);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return;
					} else {
						final String action = args[1];
						final int value = MathUtil.parseInteger(textValue);
						final int playerLevel = playerHerbalism.getLevel();
						final int maxLevel = mainConfig.getHerbalismMaxLevel();
						final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
						final MessageBuild messageTarget;
						final MessageBuild messageSender;
						final int level;
						
						if (action.equalsIgnoreCase("Set")) {
							level = MathUtil.limitInteger(value, 1, maxLevel);
							messageTarget = Language.COMMAND_HERBALISM_LEVEL_SET_SUCCESS_TO_TARGET.getMessage(sender);
							messageSender = Language.COMMAND_HERBALISM_LEVEL_SET_SUCCESS_TO_SENDER.getMessage(sender);
							
							playerHerbalism.setLevel(level, LevelChangeReason.COMMAND);
						} else if (action.equalsIgnoreCase("Add")) {
							level = MathUtil.limitInteger(value, 0, maxLevel - playerLevel);
							messageTarget = Language.COMMAND_HERBALISM_LEVEL_ADD_SUCCESS_TO_TARGET.getMessage(sender);
							messageSender = Language.COMMAND_HERBALISM_LEVEL_ADD_SUCCESS_TO_SENDER.getMessage(sender);
							
							playerHerbalism.addLevel(level, LevelChangeReason.COMMAND);
						}  else if (action.equalsIgnoreCase("Take")) {
							level = MathUtil.limitInteger(value, 0, playerLevel - 1);
							messageTarget = Language.COMMAND_HERBALISM_LEVEL_TAKE_SUCCESS_TO_TARGET.getMessage(sender);
							messageSender = Language.COMMAND_HERBALISM_LEVEL_TAKE_SUCCESS_TO_SENDER.getMessage(sender);
							
							playerHerbalism.takeLevel(level, LevelChangeReason.COMMAND);
						} else {
							final MessageBuild message = Language.ARGUMENT_INVALID_COMMAND.getMessage(sender);
							
							message.sendMessage(sender);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
							return;
						}
						
						mapPlaceholder.put("sender", sender.getName());
						mapPlaceholder.put("target", target.getName());
						mapPlaceholder.put("level", String.valueOf(level));
						
						if (!sender.equals(target)) {
							messageSender.sendMessage(sender, mapPlaceholder);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
						}
						
						playerHerbalism.save();
						messageTarget.sendMessage(target, mapPlaceholder);
						SenderUtil.playSound(target, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
						return;
					}
				}
			}
		}
	}
}
