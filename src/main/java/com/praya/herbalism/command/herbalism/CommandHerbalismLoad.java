package com.praya.herbalism.command.herbalism;

import java.util.HashMap;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.command.Command;
import com.praya.herbalism.command.CommandArgument;
import com.praya.herbalism.fertilizer.FertilizerProperties;
import com.praya.herbalism.item.ItemProperties;
import com.praya.herbalism.language.Language;
import com.praya.herbalism.manager.game.FertilizerManager;
import com.praya.herbalism.manager.game.GameManager;
import com.praya.herbalism.manager.game.ItemManager;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;

public final class CommandHerbalismLoad extends CommandArgument {

	private static final Command COMMAND = Command.HERBALISM_LOAD;
	
	protected CommandHerbalismLoad(Plugin plugin) {
		super(plugin, COMMAND.getMain(), COMMAND.getPermission(), COMMAND.getAliases());
	}
	
	@Override
	public String getDescription(CommandSender sender) {
		return Language.TOOLTIP_HERBALISM_ABOUT.getText();
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final GameManager gameManager = plugin.getGameManager();
		final ItemManager itemManager = gameManager.getItemManager();
		final FertilizerManager fertilizerManager = gameManager.getFertilizerManager();
		
		if (args.length < (sender instanceof Player ? 3 : 4)) {
			final String tooltip = TextUtil.getJsonTooltip(Language.TOOLTIP_HERBALISM_LOAD.getText(sender));
			final MessageBuild message = Language.ARGUMENT_HERBALISM_LOAD.getMessage(sender);
			
			message.sendMessage(sender, "tooltip_load", tooltip);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else {
			final String category = args[1];
			final String textItemID = args[2];
			final ItemStack item;
			
			if (category.equalsIgnoreCase("Item") || category.equalsIgnoreCase("Harvest")) {
				final ItemProperties itemProperties = itemManager.getItemProperties(textItemID);
				
				item = itemProperties != null ? itemProperties.getItem() : null;
			} else if (category.equalsIgnoreCase("Fertilizer")) {
				final FertilizerProperties fertilizerProperties = fertilizerManager.getFertilizer(textItemID);
				
				item = fertilizerProperties != null ? fertilizerProperties.getItem() : null;
			} else {
				final MessageBuild message = Language.ARGUMENT_CATEGORY_NOT_EXISTS.getMessage(sender);
				
				message.sendMessage(sender, "category", category);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return;
			}
			
			if (item == null) {
				final MessageBuild message = Language.ARGUMENT_ITEM_NOT_EXISTS.getMessage(sender);
				
				message.sendMessage(sender, "item_id", textItemID);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return;
			} else {
				final Player target;
				final int amount;
				
				if (args.length > 3) {
					final String textTarget = args[3];
					
					target = PlayerUtil.getOnlinePlayer(textTarget);
				} else {
					target = PlayerUtil.parse(sender);
				}
				
				if (args.length > 4) {
					final String textAmount = args[4];
					
					if (!MathUtil.isNumber(textAmount)) {
						amount = MathUtil.parseInteger(textAmount);
					} else {
						final MessageBuild message = Language.ARGUMENT_INVALID_VALUE.getMessage(sender);
						
						message.sendMessage(sender);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return;
					}
				} else {
					amount = 1;
				}
				
				if (target == null) {
					final MessageBuild message = Language.PLAYER_TARGET_OFFLINE.getMessage(sender);
					
					message.sendMessage(sender);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return;
				} else {
					final ItemStack clone = item.clone();
					final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
					
					mapPlaceholder.put("item_id", textItemID);
					mapPlaceholder.put("amount", String.valueOf(amount));
					
					if (sender.equals(target)) {
						final MessageBuild message = Language.COMMAND_HERBALISM_LOAD_SUCCESS_SELF.getMessage(sender);
						
						message.sendMessage(target, mapPlaceholder);
						SenderUtil.playSound(target, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
					} else {
						final MessageBuild messageTarget = Language.COMMAND_HERBALISM_LOAD_SUCCESS_TO_TARGET.getMessage(sender);
						final MessageBuild messageSender = Language.COMMAND_HERBALISM_LOAD_SUCCESS_TO_SENDER.getMessage(sender);
						
						mapPlaceholder.put("target", target.getName());
						mapPlaceholder.put("sender", sender.getName());
						
						messageTarget.sendMessage(target, mapPlaceholder);
						messageSender.sendMessage(sender, mapPlaceholder);
						SenderUtil.playSound(target, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
					}
					
					clone.setAmount(amount);
					PlayerUtil.addItem(target, clone);
					return;
				}
			}
		}
	}
}
