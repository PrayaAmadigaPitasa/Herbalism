package com.praya.herbalism.command.herbalism;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.command.Command;
import com.praya.herbalism.command.CommandArgument;
import com.praya.herbalism.language.Language;
import com.praya.herbalism.manager.game.GameManager;
import com.praya.herbalism.manager.game.MenuManager;
import com.praya.herbalism.menu.gui.MenuHerbalism;

import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;

public final class CommandHerbalismMenu extends CommandArgument {

	private static final Command COMMAND = Command.HERBALISM_MENU;
	
	protected CommandHerbalismMenu(Plugin plugin) {
		super(plugin, COMMAND.getMain(), COMMAND.getPermission(), COMMAND.getAliases());
	}
	
	@Override
	public String getDescription(CommandSender sender) {
		return Language.TOOLTIP_HERBALISM_ABOUT.getText();
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final GameManager gameManager = plugin.getGameManager();
		final MenuManager menuManager = gameManager.getMenuManager();
		
		if (!SenderUtil.isPlayer(sender)) {
			final MessageBuild message = Language.CONSOLE_COMMAND_FORBIDEN.getMessage(sender);
			
			message.sendMessage(sender);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else {
			final Player player = PlayerUtil.parse(sender);
			final MenuHerbalism menuHerbalism = menuManager.getMenuHerbalism();
			final String menu = args.length > 1 ? args[1] : "Home";
			
			if (menu.equalsIgnoreCase("Home")) {
				menuHerbalism.openMenuHome(player);
				return;
			} else if (menu.equalsIgnoreCase("Player")) {
				menuHerbalism.openMenuPlayer(player);
				return;
			} else if (menu.equalsIgnoreCase("Progress")) {
				menuHerbalism.openMenuProgress(player);
				return;
			} else if (menu.equalsIgnoreCase("Progress_Ability")) {
				menuHerbalism.openMenuProgressAbility(player);
				return;	
			} else if (menu.equalsIgnoreCase("Progress_Crops")) {
				menuHerbalism.openMenuProgressCrops(player);
				return;	
			} else if (menu.equalsIgnoreCase("Progress_Fertilizer")) {
				menuHerbalism.openMenuBuyFertilizer(player);
				return;	
			} else if (menu.equalsIgnoreCase("Shop")) {
				menuHerbalism.openMenuShop(player);
				return;
			} else if (menu.equalsIgnoreCase("Shop_Harvest_Buy")) {
				menuHerbalism.openMenuBuyHarvest(player);
				return;
			} else if (menu.equalsIgnoreCase("Shop_Harvest_Sell")) {
				menuHerbalism.openMenuSellHarvest(player);
				return;	
			} else if (menu.equalsIgnoreCase("Shop_Fertilizer_Buy")) {
				menuHerbalism.openMenuBuyFertilizer(player);
				return;
			} else {
				final MessageBuild message = Language.COMMAND_HERBALISM_MENU_NOT_EXIST.getMessage(sender);
				
				message.sendMessage(player, "menu", menu);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return;
			}
		}
	}
}
