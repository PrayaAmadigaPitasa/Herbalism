package com.praya.herbalism.command.herbalism;

import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.SenderUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.HerbalismConfig;
import com.praya.herbalism.ability.AbilityConfig;
import com.praya.herbalism.command.Command;
import com.praya.herbalism.command.CommandArgument;
import com.praya.herbalism.command.CommandConfig;
import com.praya.herbalism.crops.CropsBlockMemory;
import com.praya.herbalism.crops.CropsConfig;
import com.praya.herbalism.fertilizer.FertilizerConfig;
import com.praya.herbalism.handler.Handler;
import com.praya.herbalism.item.ItemConfig;
import com.praya.herbalism.language.Language;
import com.praya.herbalism.language.LanguageConfig;
import com.praya.herbalism.manager.task.TaskManager;
import com.praya.herbalism.placeholder.PlaceholderConfig;
import com.praya.herbalism.player.PlayerHerbalismMemory;

import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;

public final class CommandHerbalismReload extends CommandArgument {

	private static final Command COMMAND = Command.HERBALISM_RELOAD;
	
	protected CommandHerbalismReload(Plugin plugin) {
		super(plugin, COMMAND.getMain(), COMMAND.getPermission(), COMMAND.getAliases());
	}
	
	@Override
	public String getDescription(CommandSender sender) {
		return Language.TOOLTIP_HERBALISM_RELOAD.getText();
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final TaskManager taskManager = plugin.getTaskManager();
		final HerbalismConfig mainConfig = plugin.getMainConfig();
		
		if (!COMMAND.checkPermission(sender)) {
			final String permission = COMMAND.getPermission();
			final MessageBuild message = Language.PERMISSION_LACK.getMessage(sender);
			
			message.sendMessage(sender, "permission", permission);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else {
			final MessageBuild message = Language.COMMAND_HERBALISM_RELOAD_SUCCESS.getMessage(sender);
			final PlaceholderConfig placeholderConfig = Handler.getHandler(PlaceholderConfig.class);
			final LanguageConfig languageConfig = Handler.getHandler(LanguageConfig.class);
			final CommandConfig commandConfig = Handler.getHandler(CommandConfig.class);
			final ItemConfig itemConfig = Handler.getHandler(ItemConfig.class);
			final FertilizerConfig fertilizerConfig = Handler.getHandler(FertilizerConfig.class);
			final CropsConfig cropsConfig = Handler.getHandler(CropsConfig.class);
			final AbilityConfig abilityConfig = Handler.getHandler(AbilityConfig.class);
			final CropsBlockMemory cropsBlockMemory = CropsBlockMemory.getInstance();
			final PlayerHerbalismMemory playerHerbalismMemory = PlayerHerbalismMemory.getInstance();
			
			mainConfig.setup();
			placeholderConfig.setup();
			languageConfig.setup();
			commandConfig.setup();
			
			itemConfig.setup();
			fertilizerConfig.setup();
			cropsConfig.setup();
			abilityConfig.setup();
			
			taskManager.getTaskCropsBlockManager().reloadAllTask();
			taskManager.getTaskPlayerHerbalismManager().reloadAllTask();
			
			cropsBlockMemory.setupDatabase();
			playerHerbalismMemory.setupDatabase();
			
			message.sendMessage(sender);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
			return;
		}
	}
}
