package com.praya.herbalism.player;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.plugin.java.JavaPlugin;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.crops.CropsHologram;
import com.praya.herbalism.manager.player.PlayerCropsHologramManager;

public final class PlayerCropsHologramMemory extends PlayerCropsHologramManager {

	private final HashMap<UUID, CropsHologram> mapPlayerCropsHologram = new HashMap<UUID, CropsHologram>();
	
	private PlayerCropsHologramMemory(Herbalism plugin) {
		super(plugin);
	}
	
	private static class PlayerCropsHologramMemorySingleton {
		private static final PlayerCropsHologramMemory instance;
		
		static {
			final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
			
			instance = new PlayerCropsHologramMemory(plugin);
		}
	}
	
	public static final PlayerCropsHologramMemory getInstance() {
		return PlayerCropsHologramMemorySingleton.instance;
	}
	
	@Override
	public final Collection<UUID> getPlayerIds() {
		return getPlayerIds(true);
	}
	
	protected final Collection<UUID> getPlayerIds(boolean clone) {
		final Collection<UUID> playerIds = this.mapPlayerCropsHologram.keySet();
		
		return clone ? new ArrayList<UUID>(playerIds) : playerIds;
	}
	
	@Override
	public final Collection<CropsHologram> getAllPlayerCropsHologram() {
		return this.mapPlayerCropsHologram.values();
	}
	
	protected final Collection<CropsHologram> getAllPlayerCropsHologram(boolean clone) {
		final Collection<CropsHologram> allPlayerCropsHologram = this.mapPlayerCropsHologram.values();
		
		return clone ? new ArrayList<CropsHologram>(allPlayerCropsHologram) : allPlayerCropsHologram;
	}
	
	@Override
	public final CropsHologram getPlayerCropsHologram(UUID playerId) {
		return this.mapPlayerCropsHologram.get(playerId);
	}
	
	public final boolean register(CropsHologram cropsHologram) {
		if (cropsHologram != null && !getAllPlayerCropsHologram(false).contains(cropsHologram)) {
			final UUID viewerId = cropsHologram.getViewerId();
			
			this.mapPlayerCropsHologram.put(viewerId, cropsHologram);
			
			return true;
		} else {
			return false;
		}
	}
	
	public final boolean unregister(CropsHologram cropsHologram) {
		if (cropsHologram != null && getAllPlayerCropsHologram(false).contains(cropsHologram)) {
			final UUID viewerId = cropsHologram.getViewerId();
			
			this.mapPlayerCropsHologram.remove(viewerId);
			
			return true;
		} else {
			return false;
		}
	}
}
