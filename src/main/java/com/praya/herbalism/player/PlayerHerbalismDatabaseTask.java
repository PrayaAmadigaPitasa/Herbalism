package com.praya.herbalism.player;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;
import java.util.Map.Entry;

import com.praya.agarthalib.database.DatabaseMarkType;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.HerbalismConfig;
import com.praya.herbalism.handler.HandlerTask;

public final class PlayerHerbalismDatabaseTask extends HandlerTask implements Runnable {

	private long tick = 0;
	
	public PlayerHerbalismDatabaseTask(Herbalism plugin) {
		super(plugin);
	}

	@Override
	public void run() {
		final PlayerHerbalismMemory playerHerbalismMemory = PlayerHerbalismMemory.getInstance();
		final PlayerHerbalismDatabase playerHerbalismDatabase = playerHerbalismMemory.getPlayerHerbalismDatabase();
		final HerbalismConfig mainConfig = plugin.getMainConfig();
		final long period = mainConfig.getDatabasePeriodSave() * 20;
		
		if (tick % period == 0) {
			for (PlayerHerbalism playerHerbalism : playerHerbalismMemory.getAllPlayerHerbalism()) {
				playerHerbalismMemory.addDataEntry(playerHerbalism, DatabaseMarkType.SAVE);
			}
		}
		
		if (!playerHerbalismMemory.isListDataEntryEmpty()) {
			final List<Entry<Object, DatabaseMarkType>> listDataEntry = playerHerbalismMemory.getListDataEntry();
			
			playerHerbalismMemory.clearListDataEntry();
			
			for (Entry<Object, DatabaseMarkType> entry : listDataEntry) {
				final Object object = entry.getKey();
				final DatabaseMarkType mark = entry.getValue();
				
				if (object instanceof UUID) {
					final UUID playerId = (UUID) object;
					final String id = playerId.toString();
					
					if (mark.equals(DatabaseMarkType.LOAD)) {
						try {
							final PlayerHerbalism playerHerbalism = playerHerbalismDatabase.getPlayerHerbalism(id);
							
							if (playerHerbalism != null) {
								playerHerbalismMemory.mapPlayerHerbalism.put(playerId, playerHerbalism);
							} else {
								final PlayerHerbalism playerHerbalismNew = new PlayerHerbalism(playerId);
								
								playerHerbalismMemory.mapPlayerHerbalism.put(playerId, playerHerbalismNew);
							}
						} catch (SQLException e) {
							e.printStackTrace();
						}
					} else if (mark.equals(DatabaseMarkType.REMOVE)) {
						playerHerbalismDatabase.deleteData(id);
						playerHerbalismMemory.removeFromCache(playerId);
					}
				} else if (object instanceof PlayerHerbalism) {
					final PlayerHerbalism playerHerbalism = (PlayerHerbalism) object;
				
					if (mark.equals(DatabaseMarkType.SAVE)) {
						playerHerbalismDatabase.saveData(playerHerbalism);
					}
				}
			}
		}	
		
		tick++;
	}
}
