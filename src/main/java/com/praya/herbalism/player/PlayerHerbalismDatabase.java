package com.praya.herbalism.player;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import org.bukkit.OfflinePlayer;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.handler.HandlerDatabase;

public abstract class PlayerHerbalismDatabase extends HandlerDatabase {

	protected PlayerHerbalismDatabase(Herbalism plugin) {
		super(plugin);
	}
	
	protected abstract List<String> getAllId() throws SQLException;
	protected abstract List<String> getAllData() throws SQLException;
	protected abstract String getData(String id) throws SQLException;
	protected abstract boolean isExists(String id) throws SQLException;
	protected abstract PlayerHerbalism getPlayerHerbalism(OfflinePlayer player) throws SQLException;
	protected abstract PlayerHerbalism getPlayerHerbalism(String id) throws SQLException;
	protected abstract boolean saveData(PlayerHerbalism playerHerbalism);
	protected abstract boolean deleteData(String id);
	
	public final boolean deleteData(PlayerHerbalism playerHerbalism) {
		if (playerHerbalism != null) {
			final UUID playerId = playerHerbalism.getPlayerId();
			
			return deleteData(playerId.toString());
		} else {
			return false;
		}
	}
}
