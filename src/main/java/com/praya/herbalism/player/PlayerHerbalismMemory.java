package com.praya.herbalism.player;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import com.praya.agarthalib.database.DatabaseMarkType;
import com.praya.agarthalib.database.DatabaseType;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.HerbalismConfig;
import com.praya.herbalism.manager.player.PlayerHerbalismManager;
import com.praya.herbalism.player.PlayerHerbalism;

public final class PlayerHerbalismMemory extends PlayerHerbalismManager {

	protected final HashMap<UUID, PlayerHerbalism> mapPlayerHerbalism;
	protected final List<Entry<Object, DatabaseMarkType>> listDataEntry;
	
	private PlayerHerbalismDatabase playerHerbalismDatabase;;
	
	private PlayerHerbalismMemory(Herbalism plugin) {
		super(plugin);

		this.mapPlayerHerbalism = new HashMap<UUID, PlayerHerbalism>();
		this.listDataEntry = new ArrayList<Entry<Object, DatabaseMarkType>>();
		
		setupDatabase();
	}
	
	private static class PlayerHerbalismMemorySingleton {
		private static final PlayerHerbalismMemory instance;
		
		static {
			final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
			
			instance = new PlayerHerbalismMemory(plugin);
		}
	}
	
	public static final PlayerHerbalismMemory getInstance() {
		return PlayerHerbalismMemorySingleton.instance;
	}
	
	protected final PlayerHerbalismDatabase getPlayerHerbalismDatabase() {
		return this.playerHerbalismDatabase;
	}
	
	public final void setupDatabase() {
		final HerbalismConfig mainConfig = plugin.getMainConfig();
		final DatabaseType databaseType = mainConfig.getDatabaseServerType();
		
		if (this.playerHerbalismDatabase != null) {
			this.playerHerbalismDatabase.closeConnection();
		}
		
		switch (databaseType) {
		case MySQL:
		case SQLite:
			this.playerHerbalismDatabase = new PlayerHerbalismDatabaseSQL(plugin);
			break;
		default: 
			this.playerHerbalismDatabase = null;
		}
		
		reloadDataPlayerHerbalism();
	}
	
	@Override
	public final Collection<UUID> getPlayerIds() {
		return getPlayerIds(true);
	}
	
	protected final Collection<UUID> getPlayerIds(boolean clone) {
		final Collection<UUID> playerIds = this.mapPlayerHerbalism.keySet();
		
		return clone ? new ArrayList<UUID>(playerIds) : playerIds;
	}
	
	@Override
	public final Collection<PlayerHerbalism> getAllPlayerHerbalism() {
		return getAllPlayerHerbalism(false);
	}
	
	protected final Collection<PlayerHerbalism> getAllPlayerHerbalism(boolean clone) {
		final Collection<PlayerHerbalism> allPlayerHerbalism = this.mapPlayerHerbalism.values();
		
		return clone ? new ArrayList<PlayerHerbalism>(allPlayerHerbalism) : allPlayerHerbalism;
	}
	
	@Override
	public final PlayerHerbalism getPlayerHerbalism(UUID playerId) {
		return this.mapPlayerHerbalism.get(playerId);
	}
	
	public final boolean removeFromCache(UUID playerId) {
		return this.mapPlayerHerbalism.remove(playerId) != null;
	}
	
	protected final boolean saveToCache(PlayerHerbalism playerHerbalism) {
		if (getPlayerHerbalismDatabase().saveData(playerHerbalism)) {
			final UUID playerId = playerHerbalism.getPlayerId();
			
			this.mapPlayerHerbalism.put(playerId, playerHerbalism);
			
			return true;
		} else {
			return false;
		}
	}
	
	public final List<Entry<Object, DatabaseMarkType>> getListDataEntry() {
		return new ArrayList<Entry<Object, DatabaseMarkType>>(this.listDataEntry);
	}
	
	public final boolean addDataEntry(Object object, DatabaseMarkType type) {
		if (object != null && type != null) {
			final boolean matchSave = type.equals(DatabaseMarkType.SAVE) && object instanceof PlayerHerbalism;
			final boolean matchRemove = type.equals(DatabaseMarkType.REMOVE) && object instanceof UUID;
			final boolean matchLoad = type.equals(DatabaseMarkType.LOAD) && object instanceof UUID;
			
			if (matchSave || matchRemove || matchLoad) {
				final SimpleEntry<Object, DatabaseMarkType> entry = new SimpleEntry<Object, DatabaseMarkType>(object, type);
				
				this.listDataEntry.add(entry);
				
				return true;
			}
		}
		
		return false;
	}
	
	public final boolean isListDataEntryEmpty() {
		return this.listDataEntry.isEmpty();
	}
	
	protected final void clearListDataEntry() {
		this.listDataEntry.clear();
	}
	
	protected final void reloadDataPlayerHerbalism() {
		
		this.mapPlayerHerbalism.clear();
		
		for (Player player : PlayerUtil.getOnlinePlayers()) {
			final UUID playerId = player.getUniqueId();
			
			addDataEntry(playerId, DatabaseMarkType.LOAD);
		}
	}
}