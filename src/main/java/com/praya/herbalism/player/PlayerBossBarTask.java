package com.praya.herbalism.player;

import org.bukkit.entity.Player;

import core.praya.agarthalib.bridge.unity.Bridge;
import com.praya.agarthalib.utility.MetadataUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.handler.HandlerTask;

public final class PlayerBossBarTask extends HandlerTask implements Runnable {

	protected PlayerBossBarTask(Herbalism plugin) {
		super(plugin);
	}

	@Override
	public void run() {
		final String informationID = "Herbalism Information";
		
		for (Player player : PlayerUtil.getOnlinePlayers()) {
			if (MetadataUtil.hasMetadata(player, informationID)) {
				if (MetadataUtil.isExpired(player, informationID)) {
					MetadataUtil.removeMetadata(player, informationID);
					Bridge.getBridgeMessage().removeBossBar(player, informationID);
				}
			}
		}
	}
}
