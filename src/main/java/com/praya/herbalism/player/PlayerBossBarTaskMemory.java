package com.praya.herbalism.player;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scheduler.BukkitTask;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.manager.task.TaskPlayerBossBarManager;

public final class PlayerBossBarTaskMemory extends TaskPlayerBossBarManager {

	private BukkitTask taskPlayerBossBar;
	
	private PlayerBossBarTaskMemory(Herbalism plugin) {
		super(plugin);
		
		reloadTaskPlayerBossBar();
	}
	
	private static class PlayerBossBarTaskMemorySingleton {
		private static final PlayerBossBarTaskMemory instance;
		
		static {
			final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
			
			instance = new PlayerBossBarTaskMemory(plugin);
		}
	}
	
	public static final PlayerBossBarTaskMemory getInstance() {
		return PlayerBossBarTaskMemorySingleton.instance;
	}
	
	@Override
	public final void reloadTaskPlayerBossBar() {
		if (this.taskPlayerBossBar != null) {
			this.taskPlayerBossBar.cancel();
		}
		
		this.taskPlayerBossBar = createTaskPlayerBossBar();
	}
	
	private final BukkitTask createTaskPlayerBossBar() {
		final BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
		final Runnable runnable = new PlayerBossBarTask(plugin);
		final int delay = 0;
		final int period = 20;
		final BukkitTask task = scheduler.runTaskTimer(plugin, runnable, delay, period);
		
		return task;
	}
}
