package com.praya.herbalism.player;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scheduler.BukkitTask;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.manager.task.TaskPlayerHerbalismManager;

public final class PlayerHerbalismTaskMemory extends TaskPlayerHerbalismManager {

	private BukkitTask taskPlayerHerbalismDatabase;
	
	private PlayerHerbalismTaskMemory(Herbalism plugin) {
		super(plugin);
		
		reloadTaskPlayerHerbalismDatabase();
	}
	
	private static class PlayerHerbalismTaskMemorySingleton {
		private static final PlayerHerbalismTaskMemory instance;
		
		static {
			final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
			
			instance = new PlayerHerbalismTaskMemory(plugin);
		}
	}
	
	public static final PlayerHerbalismTaskMemory getInstance() {
		return PlayerHerbalismTaskMemorySingleton.instance;
	}
	
	@Override
	public final void reloadTaskPlayerHerbalismDatabase() {
		if (this.taskPlayerHerbalismDatabase != null) {
			this.taskPlayerHerbalismDatabase.cancel();
		}
		
		this.taskPlayerHerbalismDatabase = createTaskPlayerHerbalismDatabase();
	}
	
	private final BukkitTask createTaskPlayerHerbalismDatabase() {
		final BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
		final Runnable runnable = new PlayerHerbalismDatabaseTask(plugin);
		final BukkitTask task = scheduler.runTaskTimerAsynchronously(plugin, runnable, 0, 1);
		
		return task;
	}
}
