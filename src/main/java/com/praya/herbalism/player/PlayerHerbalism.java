package com.praya.herbalism.player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.database.DatabaseMarkType;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.ServerEventUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.HerbalismConfig;
import com.praya.herbalism.ability.Ability;
import com.praya.herbalism.crops.CropsBlock;
import com.praya.herbalism.crops.CropsProperties;
import com.praya.herbalism.crops.CropsRequirement;
import com.praya.herbalism.event.PlayerHerbalismExpChangeEvent;
import com.praya.herbalism.event.PlayerHerbalismLevelChangeEvent;
import com.praya.herbalism.event.PlayerHerbalismExpChangeEvent.ExpChangeReason;
import com.praya.herbalism.event.PlayerHerbalismLevelChangeEvent.LevelChangeReason;
import com.praya.herbalism.fertilizer.FertilizerProperties;
import com.praya.herbalism.item.ItemRequirement;
import com.praya.herbalism.manager.game.AbilityManager;
import com.praya.herbalism.manager.game.CropsBlockManager;
import com.praya.herbalism.manager.game.CropsPropertiesManager;
import com.praya.herbalism.manager.game.FertilizerManager;
import com.praya.herbalism.manager.game.GameManager;

public class PlayerHerbalism {

	private final UUID playerId;
	
	private int level;
	private float exp;
	
	public PlayerHerbalism(OfflinePlayer player) {
		this(player.getUniqueId());
	}
	
	public PlayerHerbalism(OfflinePlayer player, int level) {
		this(player.getUniqueId(), level);
	}
	
	public PlayerHerbalism(OfflinePlayer player, int level, float exp) {
		this(player.getUniqueId(), level, exp);
	}
	
	protected PlayerHerbalism(UUID playerId) {
		this(playerId, 1);
	}
	
	protected PlayerHerbalism(UUID playerId, int level) {
		this(playerId, level, 0);
	}
	
	protected PlayerHerbalism(UUID playerId, int level, float exp) {
		if (playerId == null) {
			throw new IllegalArgumentException();
		} else {
			final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
			final HerbalismConfig mainConfig = plugin.getMainConfig();
			final int maxLevel = mainConfig.getHerbalismMaxLevel();
			
			this.playerId = playerId;
			this.level = MathUtil.limitInteger(level, 1, maxLevel);
			this.exp = Math.min(exp, getExpToUp(this.level));
		}
	}
	
	public final UUID getPlayerId() {
		return this.playerId;
	}
	
	public final int getLevel() {
		return this.level;
	}
	
	public final float getExp() {
		return this.exp;
	}
	
	public final void addLevel(int level) {
		addLevel(level, null);
	}
	
	public final void addLevel(int level, LevelChangeReason reason) {
		final int finalLevel = getLevel() + level;
		
		setLevel(finalLevel, reason);;
	}
	
	public final void takeLevel(int level) {
		takeExp(level, null);
	}
	
	public final void takeLevel(int level, LevelChangeReason reason) {
		final int finalLevel = getLevel() - level;
		
		setLevel(finalLevel, reason);;
	}
	
	public final void setLevel(int level) {
		setLevel(level, null);
	}
	
	public final void setLevel(int level, LevelChangeReason reason) {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final HerbalismConfig mainConfig = plugin.getMainConfig();
		final int maxLevel = mainConfig.getHerbalismMaxLevel();
		
		this.level = MathUtil.limitInteger(level, 1, maxLevel);
		
		if (reason != null && this.level != level) {
			final PlayerHerbalismLevelChangeEvent playerHerbalismLevelChangeEvent = new PlayerHerbalismLevelChangeEvent(getPlayerId(), level, reason);
			
			ServerEventUtil.callEvent(playerHerbalismLevelChangeEvent);
		}
	}
	
	public final void addExp(float exp) {
		addExp(exp, null);
	}
	
	public final void addExp(float exp, ExpChangeReason reason) {
		final float finalExp = getExp() + exp;
		
		setExp(finalExp, reason);
	}
	
	public final void takeExp(float exp) {
		takeExp(exp, null);
	}
	
	public final void takeExp(float exp, ExpChangeReason reason) {
		final float finalExp = getExp() - exp;
		
		setExp(finalExp, reason);
	}
	
	public final void setExp(float exp) {
		setExp(exp, null);
	}
	
	public final void setExp(float exp, ExpChangeReason reason) {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final HerbalismConfig mainConfig = plugin.getMainConfig();
		final float playerExp = getExp();
		
		if (playerExp != exp) {
			if (reason != null) {
				final PlayerHerbalismExpChangeEvent playerHerbalismExpChangeEvent = new PlayerHerbalismExpChangeEvent(getPlayerId(), exp, reason);
				
				ServerEventUtil.callEvent(playerHerbalismExpChangeEvent);
				
				if (playerHerbalismExpChangeEvent.isCancelled()) {
					return;
				} else {
					exp = playerHerbalismExpChangeEvent.getExp();
				}
			}
			
			final int playerLevel = getLevel();
			final int maxLevel = mainConfig.getHerbalismMaxLevel();
			
			int level = playerLevel;
			
			while (true) {
				final float expToUp = getExpToUp(level);
				
				if (level < maxLevel) {
					if (exp >= expToUp) {
						exp = exp - expToUp;
						level = level + 1;
					} else {
						break;
					}
				} else {
					exp = Math.min(exp, expToUp);
					break;
				}
			}
			
			if (playerLevel != level) {
				final PlayerHerbalismLevelChangeEvent playerHerbalismLevelChangeEvent = new PlayerHerbalismLevelChangeEvent(getPlayerId(), level, LevelChangeReason.EXP_UP);
				
				ServerEventUtil.callEvent(playerHerbalismLevelChangeEvent);
			}
			
			this.exp = exp;
			this.level = level;
		}
	}
	
	public final float getExpToUp() {
		return getExpToUp(getLevel());
	}
	
	public final float getExpToUp(int level) {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final HerbalismConfig mainConfig = plugin.getMainConfig();
		final int maxLevel = mainConfig.getHerbalismMaxLevel();
		final int up = level + 1; 
		
		if (up <= maxLevel) {
			final double factorA = mainConfig.getHerbalismFormulaExpA();
			final double factorB = mainConfig.getHerbalismFormulaExpB();
			final double factorC = mainConfig.getHerbalismFormulaExpC();
			final float expToUp = (float) ((factorA * (up*up)) + (factorB * up) + factorC);
			
			return expToUp;
		} else {
			return 0;
		}
	}
	
	public final List<CropsBlock> getCropsBlock() {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final GameManager gameManager = plugin.getGameManager();
		final CropsBlockManager cropsBlockManager = gameManager.getCropsBlockManager();
		final List<CropsBlock> listCropsBlock = new ArrayList<CropsBlock>();
		
		for (CropsBlock cropsBlock : cropsBlockManager.getAllCropsBlock()) {
			if (cropsBlock.getOwnerId().equals(getPlayerId())) {
				listCropsBlock.add(cropsBlock);
			}
		}
		
		return listCropsBlock;
	}
	
	public final int getTotalCrops() {
		return getCropsBlock().size();
	}
	
	public final int getMaxCrops() {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final HerbalismConfig mainConfig = plugin.getMainConfig();
		final int maxTotalPlant = mainConfig.getHerbalismMaxTotalPlant();
		final int maxTotalLevel = mainConfig.getHerbalismMaxTotalLevel();
		final int baseTotalPlant = mainConfig.getHerbalismBaseTotalPlant();
		final double progress = (double) (Math.min(getLevel(), maxTotalLevel)) / maxTotalLevel; 
		final int result = (int) ((progress * (maxTotalPlant - baseTotalPlant)) + baseTotalPlant);
		
		return result;
	}
	
	public final int getAvailableCrops() {
		return getMaxCrops() - getTotalCrops();
	}
	
	public final List<Ability> getUnlockedAbilities() {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityManager abilityManager = gameManager.getAbilityManager();
		final List<Ability> listAbility = new ArrayList<Ability>();
		final Player player = PlayerUtil.getOnlinePlayer(getPlayerId());
		
		if (player != null) {
			for (Ability ability : abilityManager.getAllAbility()) {
				if (ability.isAllowed(player)) {
					listAbility.add(ability);
				}
			}
		}
		
		return listAbility;
	}
	
	public final List<CropsProperties> getUnlockedCrops() {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final GameManager gameManager = plugin.getGameManager();
		final CropsPropertiesManager cropsManager = gameManager.getCropsPropertiesManager();
		final Player player = PlayerUtil.getOnlinePlayer(getPlayerId());
		final List<CropsProperties> listCropsProperties = new ArrayList<CropsProperties>();
		
		if (player != null) {
			for (CropsProperties cropsProperties : cropsManager.getAllCropsProperties()) {
				final CropsRequirement requirement = cropsProperties.getCropsRequirement();
				
				if (requirement.isAllowed(player)) {
					listCropsProperties.add(cropsProperties);
				}
			}
		}
		
		return listCropsProperties;
	}
	
	public final List<FertilizerProperties> getUnlockedFertilizer() {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final GameManager gameManager = plugin.getGameManager();
		final FertilizerManager fertilizerManager = gameManager.getFertilizerManager();
		final List<FertilizerProperties> listFertilizerProperties = new ArrayList<FertilizerProperties>();
		final Player player = PlayerUtil.getOnlinePlayer(getPlayerId());
		
		if (player != null) {
			for (FertilizerProperties fertilizerProperties : fertilizerManager.getAllFertilizer()) {
				final ItemRequirement requirement = fertilizerProperties.getRequirement();
				
				if (requirement.isAllowed(player)) {
					listFertilizerProperties.add(fertilizerProperties);
				}
			}
		}
		
		return listFertilizerProperties;
	}
	
	public final void save() {
		final PlayerHerbalismMemory playerHerbalismMemory = PlayerHerbalismMemory.getInstance();
		
		playerHerbalismMemory.addDataEntry(this, DatabaseMarkType.SAVE);
	}
	
	public final String serialize() {
		final FileConfiguration config = new YamlConfiguration();
		
		config.set("playerId", this.playerId.toString());
		config.set("level", this.level);
		config.set("exp", this.exp);
		
		return config.saveToString();
	}
	
	public static final PlayerHerbalism deserialize(String serialize) throws InvalidConfigurationException {
		if (serialize != null) {
			final FileConfiguration config = new YamlConfiguration();
			
			config.loadFromString(serialize);
			
			UUID playerId = null;
			int level = 1;
			float exp = 0;
			
			for (String key : config.getKeys(false)) {
				if (key.equalsIgnoreCase("playerId")) {
					playerId = UUID.fromString(config.getString(key));
				} else if (key.equalsIgnoreCase("level")) {
					level = config.getInt(key);
				} else if (key.equalsIgnoreCase("exp")) {
					exp = (float) config.getDouble(key);
				}
			}
			
			if (playerId != null) {
				final PlayerHerbalism playerHerbalism = new PlayerHerbalism(playerId, level, exp); 
				
				return playerHerbalism;
			}
		} 
			
		return null;
	}
	
	public static final PlayerHerbalism deserializeSilent(String serialize) {
		try {
			return deserialize(serialize);
		} catch (InvalidConfigurationException e) {
			return null;
		}
	}
}
