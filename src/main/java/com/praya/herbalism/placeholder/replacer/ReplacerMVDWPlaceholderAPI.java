package com.praya.herbalism.placeholder.replacer;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.manager.plugin.PlaceholderManager;

import be.maximvdw.placeholderapi.PlaceholderAPI;
import be.maximvdw.placeholderapi.PlaceholderReplaceEvent;
import be.maximvdw.placeholderapi.PlaceholderReplacer;

public class ReplacerMVDWPlaceholderAPI {

	private final Herbalism plugin;
	private final String placeholder;
	
	public ReplacerMVDWPlaceholderAPI(Herbalism plugin, String placeholder) {
		this.plugin = plugin;
		this.placeholder = placeholder;
	}
	
	public final String getPlaceholder() {
		return this.placeholder;
	}
	
	public final void register() {
		final PlaceholderManager placeholderManager = plugin.getPluginManager().getPlaceholderManager();
		final String identifier = getPlaceholder() + "_*";
		
		PlaceholderAPI.registerPlaceholder(plugin, identifier, new PlaceholderReplacer() {
			
			@Override
			public String onPlaceholderReplace(PlaceholderReplaceEvent event) {
				return placeholderManager.getReplacement(event.getPlayer(), event.getPlaceholder().split(getPlaceholder() + "_")[1]);
			}
		});
	}
}
