package com.praya.herbalism.placeholder.replacer;

import org.bukkit.entity.Player;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.manager.plugin.PlaceholderManager;

import me.clip.placeholderapi.PlaceholderAPI;
import me.clip.placeholderapi.PlaceholderHook;

public class ReplacerPlaceholderAPI extends PlaceholderHook {
	
	private final String placeholder;
	private final Herbalism plugin;
	
	public ReplacerPlaceholderAPI(Herbalism plugin, String placeholder) {
		this.plugin = plugin;
		this.placeholder = placeholder;
	}
	
	public final String getPlaceholder() {
		return this.placeholder;
	}
	
	public final boolean hook() {
		return PlaceholderAPI.registerPlaceholderHook(this.placeholder, this);
	}

	@Override
	public String onPlaceholderRequest(Player player, String identifier) {
		final PlaceholderManager placeholderManager = plugin.getPluginManager().getPlaceholderManager();
		
		return placeholderManager.getReplacement(player, identifier);
	}
}