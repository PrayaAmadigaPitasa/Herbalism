package com.praya.herbalism.util;

import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.database.DatabaseType;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.WorldUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.HerbalismConfig;

public class SerializeUtil {

	public static final String serializeBlock(Block block) {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final HerbalismConfig mainConfig = plugin.getMainConfig();
		
		if (block != null) {
			final DatabaseType databaseType = mainConfig.getDatabaseServerType();
			final String world = "world: " + block.getWorld().getName();
			final String x = "x: " + block.getX();
			final String y = "y: " + block.getY();
			final String z = "z: " + block.getZ();
			final String serialize;
			
			if (databaseType.isLocal()) {
				serialize = world + "," + x + "," + y + "," + z;
			} else {
				final String serverIdentifier = mainConfig.getDatabaseServerIdentifier();
				final String server = "server:" + serverIdentifier;
				
				serialize = server + "," + world + "," + x + "," + y + "," + z;
			}
			
			return serialize;
		} else {
			return null;
		}
	}
	
	public static final Block deserializeBlock(String serialize) {
		if (serialize != null) {
			final String[] parts = serialize.replace(" ", "").split(",");
			
			World world = null;
			Integer x = null;
			Integer y = null;
			Integer z = null;
			
			for (String part : parts) {
				final String[] relation = part.split(":");
				
				if (relation.length > 1) {
					final String key = relation[0];
					final String value = relation[1];
					
					if (key.equalsIgnoreCase("World")) {
						world = WorldUtil.getWorld(value);
					} else if (key.equalsIgnoreCase("X")) {
						if (MathUtil.isNumber(value)) {
							x = MathUtil.parseInteger(value);
						}
					} else if (key.equalsIgnoreCase("Y")) {
						if (MathUtil.isNumber(value)) {
							y = MathUtil.parseInteger(value);
						}
					} else if (key.equalsIgnoreCase("Z")) {
						if (MathUtil.isNumber(value)) {
							z = MathUtil.parseInteger(value);
						}
					}
				}
			}
			
			if (world != null && x != null && y != null && z != null) {
				final Block block = world.getBlockAt(x, y, z);
				
				return block;
			}
		}
		
		return null;
	}
	
	public static final boolean matchBlockServer(String serialize) {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final HerbalismConfig mainConfig = plugin.getMainConfig();
		
		if (serialize != null) {
			final DatabaseType databaseType = mainConfig.getDatabaseServerType();
			
			if (databaseType.isLocal()) {
				return true;
			} else {
				final String serverIdentifier = mainConfig.getDatabaseServerIdentifier();
				final String[] parts = serialize.replace(" ", "").split(",");
				
				for (String part : parts) {
					final String[] relation = part.split(":");
					
					if (relation.length > 1) {
						final String key = relation[0];
						final String value = relation[1];
						
						if (key.equalsIgnoreCase("Server")) {
							return value.equals(serverIdentifier);
						}
					}
				}
			}
		}
		
		return false;
	}
}
