package com.praya.herbalism.util;

import java.util.HashMap;

import org.bukkit.entity.Player;
import com.praya.agarthalib.utility.TextUtil;
import com.praya.herbalism.language.Language;

public class TimeUtil {

	public static final String getTime(int seconds) {
		return getTime(null, seconds);
	}
	
	public static final String getTime(Player player, int seconds) {
		final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
		final String format;
		final int days = seconds / 86400;
		
		seconds = seconds - (days * 86400);
				
		final int hours = seconds / 3600;
		
		seconds = seconds - (hours * 3600);
				
		final int minutes = seconds / 60;
		
		seconds = seconds - (minutes * 60);
		
		mapPlaceholder.put("time_day", String.valueOf(days));
		mapPlaceholder.put("time_hour", String.valueOf(hours));
		mapPlaceholder.put("time_minutes", String.valueOf(minutes));
		mapPlaceholder.put("time_seconds", String.valueOf(seconds));
		
		if (days > 0) {
			format  = Language.TIME_DURATION_DAYS.getText(player);
		} else if (hours > 0) {
			format  = Language.TIME_DURATION_HOURS.getText(player);
		} else if (minutes > 0) {
			format  = Language.TIME_DURATION_MINUTES.getText(player);
		} else {
			format  = Language.TIME_DURATION_SECONDS.getText(player);
		}
		
		return TextUtil.placeholder(mapPlaceholder, format);
	}
}
