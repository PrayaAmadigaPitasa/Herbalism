package com.praya.herbalism.fertilizer;

public class FertilizerEffect {

	private double growthAdditional;
	private double growthPercent;
	
	public FertilizerEffect() {
		this(0, 0);
	}
	
	public FertilizerEffect(double growthAdditional, double growthPercent) {
		setGrowthAdditional(growthAdditional);
		setGrowthPercent(growthPercent);
	}
	
	public final double getGrowthAdditional() {
		return this.growthAdditional;
	}
	
	public final double getGrowthPercent() {
		return this.growthPercent;
	}
	
	public final void setGrowthAdditional(double seconds) {
		this.growthAdditional = Math.max(0, seconds);
	}
	
	public final void setGrowthPercent(double percent) {
		this.growthPercent = Math.max(0, percent);
	}
}
