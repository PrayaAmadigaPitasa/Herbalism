package com.praya.herbalism.fertilizer;

import org.bukkit.inventory.ItemStack;

import com.praya.herbalism.item.ItemRequirement;

public class FertilizerProperties {

	private final String id;
	private final ItemStack item;
	private final boolean buyable;
	private final double price;
	private final double exp;
	private final ItemRequirement requirement;
	private final FertilizerEffect effect;
	
	public FertilizerProperties(String id, ItemStack item, boolean buyable, double price, double exp, ItemRequirement requirement, FertilizerEffect effect) {
		if (id == null || item == null) {
			throw new IllegalArgumentException();
		} else {
			this.id = id;
			this.item = item;
			this.buyable = buyable;
			this.price = price;
			this.exp = exp;
			this.effect = effect != null ? effect : new FertilizerEffect();
			this.requirement = requirement != null ? requirement : new ItemRequirement();
		}
	}
	
	public final String getId() {
		return this.id;
	}
	
	public final ItemStack getItem() {
		return this.item;
	}
	
	public final boolean isBuyable() {
		return this.buyable;
	}
	
	public final double getPrice() {
		return this.price;
	}
	
	public final double getExp() {
		return this.exp;
	}
	
	public final ItemRequirement getRequirement() {
		return this.requirement;
	}
	
	public final FertilizerEffect getEffect() {
		return this.effect;
	}
}
