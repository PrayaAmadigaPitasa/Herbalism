package com.praya.herbalism.fertilizer;

import java.io.File;
import java.util.HashMap;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

import com.praya.agarthalib.utility.ConfigUtil;
import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.FileUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.handler.HandlerConfig;
import com.praya.herbalism.item.ItemRequirement;

public final class FertilizerConfig extends HandlerConfig {

	private static final String PATH_FILE = "Configuration/fertilizer.yml";
	
	protected final HashMap<String, FertilizerProperties> mapFertilizerProperties = new HashMap<String, FertilizerProperties>();
	
	protected FertilizerConfig(Herbalism plugin) {
		super(plugin);
		
		setup();
	}
	
	@Override
	public final void setup() {
		reset();
		loadConfig();
	}
	
	private final void reset() {
		this.mapFertilizerProperties.clear();
	}
	
	private final void loadConfig() {
		final File file = FileUtil.getFile(plugin, PATH_FILE);		
		
		if (!file.exists()) {
			FileUtil.saveResource(plugin, PATH_FILE);
		}
		
		final FileConfiguration config = FileUtil.getFileConfiguration(file);
		
		for (String key : config.getKeys(false)) {
			final ConfigurationSection itemDataSection = config.getConfigurationSection(key);
			final ItemStack item = ConfigUtil.getItemStack(itemDataSection);
			final ItemRequirement requirement = new ItemRequirement();
			final FertilizerEffect effect = new FertilizerEffect();
			final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
			
			boolean buyable = false;
			double price = 100;
			double exp = 0;
			
			for (String itemData : itemDataSection.getKeys(false)) {
				if (itemData.equalsIgnoreCase("Buyable")) {
					buyable = itemDataSection.getBoolean(itemData);
				} else if (itemData.equalsIgnoreCase("Price")) {
					price = itemDataSection.getDouble(itemData);
				} else if (itemData.equalsIgnoreCase("Exp") || itemData.equalsIgnoreCase("Experience")) {
					exp = itemDataSection.getDouble(itemData);
				} else if (itemData.equalsIgnoreCase("Requirement")) {
					final ConfigurationSection requirementDataSection = itemDataSection.getConfigurationSection(itemData);
					
					for (String requirementData : requirementDataSection.getKeys(false)) {
						if (requirementData.equalsIgnoreCase("Permission")) {
							final String permission = requirementDataSection.getString(requirementData);
							
							requirement.setPermission(permission);
						} else if (requirementData.equalsIgnoreCase("Level")) {
							final int level = requirementDataSection.getInt(requirementData);
							
							requirement.setLevel(level);;
						}
					}
				} else if (itemData.equalsIgnoreCase("Effect") || itemData.equalsIgnoreCase("Effects")) {
					final ConfigurationSection effectDataSection = itemDataSection.getConfigurationSection(itemData);
					
					for (String effectData : effectDataSection.getKeys(false)) {
						if (effectData.equalsIgnoreCase("Growth_Additional") || effectData.equalsIgnoreCase("Additional")) {
							final double growthAdditional = effectDataSection.getDouble(effectData);
							
							effect.setGrowthAdditional(growthAdditional);
						} else if (effectData.equalsIgnoreCase("Growth_Percent") || effectData.equalsIgnoreCase("Percent")) {
							final double growthPercent = effectDataSection.getDouble(effectData);
							
							effect.setGrowthPercent(growthPercent);
						}
					}
				}
			}
			
			mapPlaceholder.put("buyable", String.valueOf(buyable));
			mapPlaceholder.put("price", String.valueOf(price));
			mapPlaceholder.put("exp", String.valueOf(exp));
			mapPlaceholder.put("requirement_permission", requirement.getPermission());
			mapPlaceholder.put("requirement_level", String.valueOf(requirement.getLevel()));
			mapPlaceholder.put("effect_growth_additional", String.valueOf(effect.getGrowthAdditional()));
			mapPlaceholder.put("effect_growth_percent", String.valueOf(effect.getGrowthPercent()));
			
			EquipmentUtil.placeholder(item, mapPlaceholder);
			
			if (item != null) {
				final FertilizerProperties fertilizerProperties = new FertilizerProperties(key, item, buyable, price, exp, requirement, effect);
				
				this.mapFertilizerProperties.put(key, fertilizerProperties);
			}
		}
	}
}
