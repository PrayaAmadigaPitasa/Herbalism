package com.praya.herbalism.fertilizer;

import java.util.ArrayList;
import java.util.Collection;

import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.fertilizer.FertilizerConfig;
import com.praya.herbalism.fertilizer.FertilizerProperties;
import com.praya.herbalism.manager.game.FertilizerManager;

public final class FertilizerMemory extends FertilizerManager {

	private final FertilizerConfig fertilizerConfig;
	
	private FertilizerMemory(Herbalism plugin) {
		super(plugin);
		
		this.fertilizerConfig = new FertilizerConfig(plugin);
	}
	
	private static class FertilizerMemorySingleton {
		private static final FertilizerMemory instance;
		
		static {
			final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
			
			instance = new FertilizerMemory(plugin);
		}
	}
	
	public static final FertilizerMemory getInstance() {
		return FertilizerMemorySingleton.instance;
	}
	
	protected final FertilizerConfig getFertilizerConfig() {
		return this.fertilizerConfig;
	}
	
	@Override
	public final Collection<String> getFertilizerIds() {
		return getFertilizerIds(true);
	}
	
	protected final Collection<String> getFertilizerIds(boolean clone) {
		final Collection<String> fertilizerIds = getFertilizerConfig().mapFertilizerProperties.keySet();
		
		return clone ? new ArrayList<String>(fertilizerIds) : fertilizerIds;
	}
	
	@Override
	public final Collection<FertilizerProperties> getAllFertilizer() {
		return getAllFertilizer(true);
	}
	
	protected final Collection<FertilizerProperties> getAllFertilizer(boolean clone) {
		final Collection<FertilizerProperties> allFertilizer = getFertilizerConfig().mapFertilizerProperties.values();
		
		return clone ? new ArrayList<FertilizerProperties>(allFertilizer) : allFertilizer; 
	}
	
	@Override
	public final FertilizerProperties getFertilizer(String id) {
		if (id != null) {
			for (String key : getFertilizerIds(false)) {
				if (key.equalsIgnoreCase(id)) {
					return getFertilizerConfig().mapFertilizerProperties.get(key);
				}
			}
		}
		
		return null;
	}
	
	@Override
	public final FertilizerProperties getFertilizer(ItemStack item) {
		if (item != null) {
			for (FertilizerProperties key : getAllFertilizer(false)) {
				if (key.getItem().isSimilar(item)) {
					return key;
				}
			}
		}
		
		return null;
	}
}
