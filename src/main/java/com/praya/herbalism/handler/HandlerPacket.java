package com.praya.herbalism.handler;

import java.util.ArrayList;
import java.util.Collection;

import com.praya.herbalism.Herbalism;

public class HandlerPacket extends Handler {
	
	protected HandlerPacket(Herbalism plugin) {
		super(plugin);
	}
	
	public static Collection<HandlerPacket> getAllHandlerPacket() {
		final Collection<Handler> allHandler = getAllHandler();
		final Collection<HandlerPacket> allHandlerPacket = new ArrayList<HandlerPacket>();
		
		for (Handler handler : allHandler) {
			if (handler instanceof HandlerPacket) {
				final HandlerPacket handlerPacket = (HandlerPacket) handler;
				
				allHandlerPacket.add(handlerPacket);
			}
		}
		
		return allHandlerPacket;
	}
}
