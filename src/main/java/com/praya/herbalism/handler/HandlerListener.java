package com.praya.herbalism.handler;

import java.util.ArrayList;
import java.util.Collection;

import com.praya.herbalism.Herbalism;

public abstract class HandlerListener extends Handler {
	
	protected HandlerListener(Herbalism plugin) {
		super(plugin);
	}
	
	public static Collection<HandlerListener> getAllHandlerListener() {
		final Collection<Handler> allHandler = getAllHandler();
		final Collection<HandlerListener> allHandlerListener = new ArrayList<HandlerListener>();
		
		for (Handler handler : allHandler) {
			if (handler instanceof HandlerListener) {
				final HandlerListener handlerListener = (HandlerListener) handler;
				
				allHandlerListener.add(handlerListener);
			}
		}
		
		return allHandlerListener;
	}
}