package com.praya.herbalism.handler;

import java.util.ArrayList;
import java.util.Collection;

import com.praya.herbalism.Herbalism;

public abstract class HandlerTask extends Handler {
	
	protected HandlerTask(Herbalism plugin) {
		super(plugin);
	}
	
	public static Collection<HandlerTask> getAllHandlerTask() {
		final Collection<Handler> allHandler = getAllHandler();
		final Collection<HandlerTask> allHandlerTask = new ArrayList<HandlerTask>();
		
		for (Handler handler : allHandler) {
			if (handler instanceof HandlerTask) {
				final HandlerTask handlerTask = (HandlerTask) handler;
				
				allHandlerTask.add(handlerTask);
			}
		}
		
		return allHandlerTask;
	}
}
