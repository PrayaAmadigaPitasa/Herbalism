package com.praya.herbalism.handler;

import java.util.ArrayList;
import java.util.Collection;

import com.praya.herbalism.Herbalism;

public abstract class HandlerMenuExecutor extends Handler {
	
	protected HandlerMenuExecutor(Herbalism plugin) {
		super(plugin);
	}
	
	public static Collection<HandlerMenuExecutor> getAllHandlerMenuExecutor() {
		final Collection<Handler> allHandler = getAllHandler();
		final Collection<HandlerMenuExecutor> allHandlerMenuExecutor = new ArrayList<HandlerMenuExecutor>();
		
		for (Handler handler : allHandler) {
			if (handler instanceof HandlerMenuExecutor) {
				final HandlerMenuExecutor handlerMenu = (HandlerMenuExecutor) handler;
				
				allHandlerMenuExecutor.add(handlerMenu);
			}
		}
		
		return allHandlerMenuExecutor;
	}
}
