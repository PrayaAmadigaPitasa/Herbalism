package com.praya.herbalism;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.manager.player.PlayerCropsHologramManager;
import com.praya.herbalism.manager.player.PlayerHerbalismManager;
import com.praya.herbalism.manager.player.PlayerManager;
import com.praya.herbalism.player.PlayerCropsHologramMemory;
import com.praya.herbalism.player.PlayerHerbalismMemory;

public final class HerbalismPlayerMemory extends PlayerManager {

	private final PlayerCropsHologramManager playerCropsHologramManager;
	private final PlayerHerbalismManager playerHerbalismManager;
	
	protected HerbalismPlayerMemory(Herbalism plugin) {
		super(plugin);
		
		this.playerCropsHologramManager = PlayerCropsHologramMemory.getInstance();
		this.playerHerbalismManager = PlayerHerbalismMemory.getInstance();
	}
	
	public final PlayerCropsHologramManager getPlayerCropsHologramManager() {
		return this.playerCropsHologramManager;
	}
	
	public final PlayerHerbalismManager getPlayerHerbalismManager() {
		return this.playerHerbalismManager;
	}
}