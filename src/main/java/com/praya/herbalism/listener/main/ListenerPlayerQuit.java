package com.praya.herbalism.listener.main;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import com.praya.agarthalib.database.DatabaseMarkType;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.handler.HandlerEvent;
import com.praya.herbalism.player.PlayerHerbalism;
import com.praya.herbalism.player.PlayerHerbalismMemory;

public final class ListenerPlayerQuit extends HandlerEvent implements Listener {
	
	public ListenerPlayerQuit(Herbalism plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void unloadPlayerHerbalism(PlayerQuitEvent event) {
		final PlayerHerbalismMemory playerHerbalismMemory = PlayerHerbalismMemory.getInstance();
		final Player player = event.getPlayer();
		final PlayerHerbalism playerHerbalism = playerHerbalismMemory.getPlayerHerbalism(player);
		
		if (playerHerbalism != null) {
			playerHerbalismMemory.addDataEntry(playerHerbalism, DatabaseMarkType.SAVE);
			playerHerbalismMemory.removeFromCache(player);
		}
	}
}