package com.praya.herbalism.listener.main;

import java.util.List;
import java.util.UUID;

import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import core.praya.agarthalib.enums.branch.MaterialEnum;
import core.praya.agarthalib.enums.branch.SoundEnum;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.ServerEventUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.crops.CropsBlock;
import com.praya.herbalism.crops.CropsDrop;
import com.praya.herbalism.crops.CropsPlant;
import com.praya.herbalism.crops.CropsProperties;
import com.praya.herbalism.event.CropsCultivateEvent;
import com.praya.herbalism.event.CropsHarvestEvent;
import com.praya.herbalism.handler.HandlerEvent;
import com.praya.herbalism.language.Language;
import com.praya.herbalism.manager.game.CropsBlockManager;
import com.praya.herbalism.manager.game.GameManager;

public final class ListenerBlockBreak extends HandlerEvent implements Listener {
	
	public ListenerBlockBreak(Herbalism plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void blockBreakEvent(BlockBreakEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final CropsBlockManager cropsBlockManager = gameManager.getCropsBlockManager();
		                                                                     
		if (!event.isCancelled()) {
			final Block block = event.getBlock();
			final CropsBlock cropsBlock = cropsBlockManager.getCropsBlock(block);
			
			if (cropsBlock != null) {
				final Player player = event.getPlayer();
				final UUID playerId = player.getUniqueId();
				final UUID ownerId = cropsBlock.getOwnerId();
				
				if (!playerId.equals(ownerId)) {
					final String message = Language.CROPS_BREAK_NOT_OWNER.getText(player);
					
					event.setCancelled(true);
					SenderUtil.sendMessage(player, message);
					SenderUtil.playSound(player, SoundEnum.BLOCK_STONE_BUTTON_CLICK_ON);
					return;
				} else {
					if (cropsBlock.isHarvestTime()) {
						final CropsProperties cropsProperties = cropsBlock.getCropsProperties();
						final CropsDrop cropsDrop = cropsProperties.getCropsDrop();
						final double exp = cropsDrop.getExp();
						final List<ItemStack> items = cropsDrop.generateItems();
						final CropsHarvestEvent cropsHarvestEvent = new CropsHarvestEvent(cropsBlock, player, items, exp);
						
						ServerEventUtil.callEvent(cropsHarvestEvent);
						
						if (cropsHarvestEvent.isCancelled()) {
							event.setCancelled(true);
							return;
						} else {
							final CropsPlant cropsPlant = cropsProperties.getPlant();
							
							block.setType(MaterialEnum.AIR.getMaterial());
							event.setDropItems(false);
							cropsBlock.remove();
							
							if (cropsPlant.isStemPlant()) {
								final Block soil = block.getRelative(BlockFace.DOWN);
								final MaterialEnum materialEnum = MaterialEnum.getMaterialEnum(soil.getType());
								
								if (materialEnum.equals(MaterialEnum.GRASS) || materialEnum.equals(MaterialEnum.DIRT)) {
									soil.setType(MaterialEnum.FARMLAND.getMaterial());
								}
							}
							
							if (cropsProperties.isSustainable()) {
								final String crops = cropsProperties.getId();
								final int duration = cropsProperties.getDuration();
								final CropsCultivateEvent cropsCultivateEvent = new CropsCultivateEvent(block, player, crops, duration);
								
								event.setCancelled(true);
								ServerEventUtil.callEvent(cropsCultivateEvent);
							}
						}
					} else {
						event.setDropItems(false);
						cropsBlock.remove();
						return;
					}
					
				}
			}
		}
	}
}