package com.praya.herbalism.listener.main;

import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockGrowEvent;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.crops.CropsBlock;
import com.praya.herbalism.handler.HandlerEvent;
import com.praya.herbalism.manager.game.CropsBlockManager;
import com.praya.herbalism.manager.game.GameManager;

public final class ListenerBlockGrow extends HandlerEvent implements Listener {
	
	public ListenerBlockGrow(Herbalism plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void blockGrowEvent(BlockGrowEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final CropsBlockManager cropsBlockManager = gameManager.getCropsBlockManager();
		
		if (!event.isCancelled()) {
			final Block block = event.getBlock();
			final CropsBlock cropsBlock = cropsBlockManager.getCropsBlock(block);
			
			if (cropsBlock != null) {
				event.setCancelled(true);
			}
		}
	}
}