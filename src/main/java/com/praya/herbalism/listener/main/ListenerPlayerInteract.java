package com.praya.herbalism.listener.main;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.*;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.MaterialEnum;
import core.praya.agarthalib.enums.branch.SoundEnum;
import core.praya.agarthalib.enums.main.VersionNMS;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.ServerEventUtil;
import com.praya.agarthalib.utility.ServerUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.HerbalismConfig;
import com.praya.herbalism.crops.CropsBlock;
import com.praya.herbalism.event.CropsFertilizeEvent;
import com.praya.herbalism.fertilizer.FertilizerEffect;
import com.praya.herbalism.fertilizer.FertilizerProperties;
import com.praya.herbalism.handler.HandlerEvent;
import com.praya.herbalism.item.ItemRequirement;
import com.praya.herbalism.language.Language;
import com.praya.herbalism.manager.game.CropsBlockManager;
import com.praya.herbalism.manager.game.FertilizerManager;
import com.praya.herbalism.manager.game.GameManager;
import com.praya.herbalism.manager.player.PlayerHerbalismManager;
import com.praya.herbalism.manager.player.PlayerManager;
import com.praya.herbalism.player.PlayerHerbalism;

public final class ListenerPlayerInteract extends HandlerEvent implements Listener {
	
	public ListenerPlayerInteract(Herbalism plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void playerInteractEvent(PlayerInteractEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final PlayerManager playerManager = plugin.getPlayerManager();
		final FertilizerManager fertilizerManager = gameManager.getFertilizerManager();
		final CropsBlockManager cropsBlockManager = gameManager.getCropsBlockManager();
		final PlayerHerbalismManager playerHerbalismManager = playerManager.getPlayerHerbalismManager();
		final HerbalismConfig mainConfig = plugin.getMainConfig();
		
		if (!event.isCancelled()) {
			final Action action = event.getAction();
		
			if (action.equals(Action.RIGHT_CLICK_BLOCK)) {
				final Block block = event.getClickedBlock();
				final CropsBlock cropsBlock = cropsBlockManager.getCropsBlock(block);
				
				if (cropsBlock != null) {
					final ItemStack item = event.getItem();
					final FertilizerProperties fertilizerProperties = fertilizerManager.getFertilizer(item);
					
					if (fertilizerProperties != null) {			
						final Player player = event.getPlayer();
						final PlayerHerbalism playerHerbalism = playerHerbalismManager.getPlayerHerbalism(player);
						
						if (playerHerbalism != null) {
							final ItemRequirement requirement = fertilizerProperties.getRequirement();
							final String requirementPermission = requirement.getPermission();
							final int requirementLevel = requirement.getLevel();
							final int playerLevel = playerHerbalism.getLevel();
							
							event.setCancelled(true);
							
							if (!SenderUtil.hasPermission(player, requirementPermission)) {
								final MessageBuild message = Language.PERMISSION_LACK.getMessage(player);
								
								message.sendMessage(player, "permission", requirementPermission);
								SenderUtil.playSound(player, SoundEnum.BLOCK_STONE_BUTTON_CLICK_ON);
								return;
							} else if (playerLevel < requirementLevel) {
								final MessageBuild message = Language.CROPS_FERTILIZE_LACK_LEVEL.getMessage(player);
								
								message.sendMessage(player, "level", String.valueOf(requirementLevel));
								SenderUtil.playSound(player, SoundEnum.BLOCK_STONE_BUTTON_CLICK_ON);
								return;
							} else if (cropsBlock.isHarvestTime()) {
								final MessageBuild message = Language.CROPS_PLANT_RIPE.getMessage(player);
								
								message.sendMessage(player);
								SenderUtil.playSound(player, SoundEnum.BLOCK_STONE_BUTTON_CLICK_ON);
								return;
							} else if (cropsBlock.alreadyFertilized(player)) {
								final String message = Language.ACTIONBAR_CROPS_FERTILIZE_FAILED.getText(player);
								final String informationID = "Herbalism Information";
								final int priority = mainConfig.getPriorityActionbar();
								
								Bridge.getBridgeMessage().sendProtectedActionbar(player, message, informationID, priority);
								SenderUtil.playSound(player, SoundEnum.BLOCK_STONE_BUTTON_CLICK_ON);
								return;
							} else {
								final FertilizerEffect fertilizerEffect = fertilizerProperties.getEffect();
								final double growthAdditional = fertilizerEffect.getGrowthAdditional();
								final double growthPercent = fertilizerEffect.getGrowthPercent();
								final CropsFertilizeEvent cropsFertilizeEvent = new CropsFertilizeEvent(cropsBlock, player, item, growthAdditional, growthPercent);
								
								ServerEventUtil.callEvent(cropsFertilizeEvent);
							}
						}
					}
				}
			}
		}
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void checkCropsEvent(PlayerInteractEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final CropsBlockManager cropsBlockManager = gameManager.getCropsBlockManager();
		
		if (!event.isCancelled()) {
			final boolean isCompatible_1_9 = ServerUtil.isCompatible(VersionNMS.V1_9_R1);
			
			if (!isCompatible_1_9 || (event.getHand() != null && event.getHand().equals(EquipmentSlot.HAND))) {
				final Action action = event.getAction();
				
				if (action.equals(Action.RIGHT_CLICK_BLOCK)) {
					final Player player = event.getPlayer();
					final Block block = event.getClickedBlock();
					final CropsBlock cropsBlock = cropsBlockManager.getCropsBlock(block);
					
					if (cropsBlock != null) {
						cropsBlock.sendNotification(player);
						cropsBlock.update();
					}
				}
			}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void antiCropsTrample(PlayerInteractEvent event) {
		final HerbalismConfig mainConfig = plugin.getMainConfig();
		
		if (!event.isCancelled()) {
			final Action action = event.getAction();
			
			if (action.equals(Action.PHYSICAL)) {
				final Block block = event.getClickedBlock();
				
				if (block != null) {
					final MaterialEnum materialEnum = MaterialEnum.getMaterialEnum(block.getType());
					
					if (materialEnum.equals(MaterialEnum.FARMLAND)) {
						final boolean enableCropsTrample = mainConfig.getCropsEnableTrample();
						
						if (!enableCropsTrample) {
							event.setCancelled(true);
						}
					}
				}
			}
		}
	}
}