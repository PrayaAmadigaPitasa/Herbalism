package com.praya.herbalism.listener.main;

import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.praya.agarthalib.database.DatabaseMarkType;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.handler.HandlerEvent;
import com.praya.herbalism.player.PlayerHerbalismMemory;

public final class ListenerPlayerJoin extends HandlerEvent implements Listener {
	
	public ListenerPlayerJoin(Herbalism plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void loadPlayerHerbalism(PlayerJoinEvent event) {
		final PlayerHerbalismMemory playerHerbalismMemory = PlayerHerbalismMemory.getInstance();
		final Player player = event.getPlayer();
		final UUID playerId = player.getUniqueId();
		
		playerHerbalismMemory.addDataEntry(playerId, DatabaseMarkType.LOAD);
	}
}