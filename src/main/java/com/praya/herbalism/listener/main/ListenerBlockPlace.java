package com.praya.herbalism.listener.main;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;

import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.ServerEventUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.crops.CropsProperties;
import com.praya.herbalism.event.CropsCultivateEvent;
import com.praya.herbalism.handler.HandlerEvent;
import com.praya.herbalism.item.ItemProperties;
import com.praya.herbalism.language.Language;
import com.praya.herbalism.manager.game.CropsPropertiesManager;
import com.praya.herbalism.manager.game.GameManager;
import com.praya.herbalism.manager.game.ItemManager;
import com.praya.herbalism.manager.player.PlayerHerbalismManager;
import com.praya.herbalism.manager.player.PlayerManager;
import com.praya.herbalism.player.PlayerHerbalism;

public final class ListenerBlockPlace extends HandlerEvent implements Listener {
	
	public ListenerBlockPlace(Herbalism plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void blockPlaceEvent(BlockPlaceEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final PlayerManager playerManager = plugin.getPlayerManager();
		final CropsPropertiesManager cropsPropertiesManager = gameManager.getCropsPropertiesManager();
		final ItemManager itemManager = gameManager.getItemManager();
		final PlayerHerbalismManager playerHerbalismManager = playerManager.getPlayerHerbalismManager();
		
		if (!event.isCancelled()) {
			final ItemStack item = event.getItemInHand();
			final ItemProperties itemProperties = itemManager.getItemProperties(item);
			
			if (itemProperties != null) {
				final Player player = event.getPlayer();
				final PlayerHerbalism playerHerbalism = playerHerbalismManager.getPlayerHerbalism(player);
				
				if (playerHerbalism != null) {
					if (playerHerbalism.getAvailableCrops() < 1) {
						final MessageBuild message = Language.CROPS_PLANT_LIMIT.getMessage(player);
						
						event.setCancelled(true);
						message.sendMessage(player);
						SenderUtil.playSound(player, SoundEnum.BLOCK_STONE_BUTTON_CLICK_ON);
						return;
					} else {
						final String crops = itemProperties.generateCrops(player);
						final CropsProperties cropsProperties = cropsPropertiesManager.getCropsProperties(crops);
						
						if (cropsProperties == null) {
							final MessageBuild message = Language.CROPS_PLANT_NOT_ALLOWED.getMessage(player);
							
							event.setCancelled(true);
							message.sendMessage(player);
							SenderUtil.playSound(player, SoundEnum.BLOCK_STONE_BUTTON_CLICK_ON);
							return;
						} else {
							final Block block = event.getBlock();
							final int duration = cropsProperties.getDuration();
							final CropsCultivateEvent cropsCultivateEvent = new CropsCultivateEvent(block, player, crops, duration);
							
							ServerEventUtil.callEvent(cropsCultivateEvent);
							
							if (cropsCultivateEvent.isCancelled()) {
								event.setCancelled(true);
							}
						}
					}
				}
			}
		}
	}
}