package com.praya.herbalism.listener.custom;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import api.praya.agarthalib.builder.event.MenuOpenEvent;
import core.praya.agarthalib.builder.menu.Menu;
import core.praya.agarthalib.builder.menu.MenuGUI;
import core.praya.agarthalib.enums.branch.SoundEnum;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.handler.HandlerEvent;

public final class ListenerMenuOpen extends HandlerEvent implements Listener {

	public ListenerMenuOpen(Herbalism plugin) {
		super(plugin);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
    public void menuOpenEvent(MenuOpenEvent event) {
		final Menu menu = event.getMenu();
		final Player player = event.getPlayer();
		final String id = menu.getID();
		
		if (menu instanceof MenuGUI) {
			if (id.startsWith("Herbalism")) {
				SenderUtil.playSound(player, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
			}
		}
	}
}
