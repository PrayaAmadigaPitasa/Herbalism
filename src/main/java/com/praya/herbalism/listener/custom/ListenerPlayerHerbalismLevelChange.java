package com.praya.herbalism.listener.custom;

import java.util.HashMap;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.branch.SoundEnum;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.event.PlayerHerbalismLevelChangeEvent;
import com.praya.herbalism.event.PlayerHerbalismLevelChangeEvent.LevelChangeReason;
import com.praya.herbalism.handler.HandlerEvent;
import com.praya.herbalism.language.Language;
import com.praya.herbalism.manager.player.PlayerHerbalismManager;
import com.praya.herbalism.manager.player.PlayerManager;
import com.praya.herbalism.player.PlayerHerbalism;

public final class ListenerPlayerHerbalismLevelChange extends HandlerEvent implements Listener {
	
	public ListenerPlayerHerbalismLevelChange(Herbalism plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void playerHerbalismLevelChangeEvent(PlayerHerbalismLevelChangeEvent event) {
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerHerbalismManager playerHerbalismManager = playerManager.getPlayerHerbalismManager();
		final LevelChangeReason reason = event.getReason();
		
		if (reason.equals(LevelChangeReason.EXP_UP)) {
			final Player player = event.getOnlinePlayer();
			
			if (player != null) {
				final PlayerHerbalism playerHerbalism = playerHerbalismManager.getPlayerHerbalism(player);
				
				if (playerHerbalism != null) {
					final int levelPlayer = playerHerbalism.getLevel();
					final int level = event.getLevel();
					
					if (level > levelPlayer) {
						final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
						
						String messageTitle = Language.TITLE_PLAYER_LEVEL_UP.getText(player);
						String messageSubtitle = Language.SUBTITLE_PLAYER_LEVEL_UP.getText(player);
						
						mapPlaceholder.put("herbalism_level", String.valueOf(level));
						
						messageTitle = TextUtil.placeholder(mapPlaceholder, messageTitle);
						messageSubtitle = TextUtil.placeholder(mapPlaceholder, messageSubtitle);
						messageTitle = TextUtil.colorful(messageTitle);
						messageSubtitle = TextUtil.colorful(messageSubtitle);
						
						
						Bridge.getBridgeMessage().sendTitle(player, messageTitle, 5, 40, 5);
						Bridge.getBridgeMessage().sendSubtitle(player, messageSubtitle, 5, 40, 5);
						SenderUtil.playSound(player, SoundEnum.ENTITY_PLAYER_LEVELUP);
					}
				}
			}
		}
	}
}