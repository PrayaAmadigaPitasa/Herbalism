package com.praya.herbalism.listener.custom;

import java.util.HashMap;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.branch.SoundEnum;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;
import com.praya.agarthalib.utility.TimeUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.HerbalismConfig;
import com.praya.herbalism.ability.type.AbilityTypeAgriculturist;
import com.praya.herbalism.crops.CropsBlock;
import com.praya.herbalism.event.CropsFertilizeEvent;
import com.praya.herbalism.event.PlayerHerbalismExpChangeEvent.ExpChangeReason;
import com.praya.herbalism.fertilizer.FertilizerProperties;
import com.praya.herbalism.handler.HandlerEvent;
import com.praya.herbalism.language.Language;
import com.praya.herbalism.manager.game.FertilizerManager;
import com.praya.herbalism.manager.game.GameManager;
import com.praya.herbalism.manager.player.PlayerHerbalismManager;
import com.praya.herbalism.manager.player.PlayerManager;
import com.praya.herbalism.player.PlayerHerbalism;

public final class ListenerCropsFertilize extends HandlerEvent implements Listener {
	
	public ListenerCropsFertilize(Herbalism plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void cropsFertilizeEvent(CropsFertilizeEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final PlayerManager playerManager = plugin.getPlayerManager();
		final FertilizerManager fertilizerManager = gameManager.getFertilizerManager();
		final PlayerHerbalismManager playerHerbalismManager = playerManager.getPlayerHerbalismManager();
		final HerbalismConfig mainConfig = plugin.getMainConfig();
		
		if (!event.isCancelled()) {
			final CropsBlock cropsBlock = event.getCropsBlock();
			final ItemStack item = event.getFertilizer();
			final FertilizerProperties fertilizerProperties = fertilizerManager.getFertilizer(item);
			final Player player = event.getPlayer();
			final String informationID = "Herbalism Information";
			final double duration = event.getDuration();
			final double percent = event.getPercent();
			final double exp = fertilizerProperties.getExp();
			final long timeCut = cropsBlock.fertilize(player, duration, percent);
			final int amount = item.getAmount();
			final int priority = mainConfig.getPriorityActionbar();
			final String timeText = TimeUtil.getTextTime(timeCut);
			final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
			
			if (exp > 0) {
				final PlayerHerbalism playerHerbalism = playerHerbalismManager.getPlayerHerbalism(player);
				
				if (playerHerbalism != null) {
					playerHerbalism.addExp((float) exp, ExpChangeReason.FERTILIZE);
				}
			}
			
			String message = Language.ACTIONBAR_CROPS_FERTILIZE_SUCCESS.getText(player);
			
			mapPlaceholder.put("time", timeText);
			mapPlaceholder.put("exp", String.valueOf(exp));
			
			message = TextUtil.placeholder(mapPlaceholder, message);
			
			item.setAmount(amount-1);
			cropsBlock.update();
			cropsBlock.sendNotification(player);
			Bridge.getBridgeMessage().sendProtectedActionbar(player, message, informationID, priority);
			SenderUtil.playSound(player, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
			return;
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void abilityAgriculturist(CropsFertilizeEvent event) {
		if (!event.isCancelled()) {
			final AbilityTypeAgriculturist ability = AbilityTypeAgriculturist.getInstance();
			
			if (ability.isRegistered()) {
				final Player player = event.getPlayer();
				
				if (ability.isAllowed(player)) {
					final double abilityEffectValue = ability.getPlayerEffectValue(player);
					final double duration = event.getDuration();
					final double durationFinal = duration * ((100 + abilityEffectValue) / 100);
					final double percent = event.getPercent();
					final double percentFinal = percent * ((100 + abilityEffectValue) / 100);
					
					event.setDuration(durationFinal);
					event.setPercent(percentFinal);
				}
			}
		}
	}
}