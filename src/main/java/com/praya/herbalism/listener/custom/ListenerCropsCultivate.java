package com.praya.herbalism.listener.custom;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.material.CocoaPlant;
import org.bukkit.material.CocoaPlant.CocoaPlantSize;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.ability.type.AbilityTypeCultivator;
import com.praya.herbalism.crops.CropsBlock;
import com.praya.herbalism.crops.CropsPlant;
import com.praya.herbalism.crops.CropsProperties;
import com.praya.herbalism.event.CropsCultivateEvent;
import com.praya.herbalism.handler.HandlerEvent;
import com.praya.herbalism.manager.game.CropsPropertiesManager;
import com.praya.herbalism.manager.game.GameManager;

public final class ListenerCropsCultivate extends HandlerEvent implements Listener {
	
	public ListenerCropsCultivate(Herbalism plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void cropsCultivateEvent(CropsCultivateEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final CropsPropertiesManager cropsPropertiesManager = gameManager.getCropsPropertiesManager();
		final Player player = event.getPlayer();
		
		if (!event.isCancelled()) {
			final String crops = event.getCrops();
			final CropsProperties cropsProperties = cropsPropertiesManager.getCropsProperties(crops);
			
			if (cropsProperties != null) {
				final Block block = event.getBlock();
				final int duration = event.getDuration();
				final CropsBlock cropsBlock = new CropsBlock(crops, block, player, duration);
				final CropsPlant cropsPlant = cropsProperties.getPlant();
				
				if (cropsPlant.equals(CropsPlant.COCOA)) {
					final BlockState blockState = block.getState();
					final BlockFace blockFace = cropsBlock.getBlockFace();
					final CocoaPlant cocoaPlant = new CocoaPlant(CocoaPlantSize.SMALL, blockFace);
					
					blockState.setData(cocoaPlant);
					blockState.update();
				} else {
					final Material cropsMaterialPlant = cropsPlant.getMaterialPlant();
					
					block.setType(cropsMaterialPlant);
				}
				
				cropsBlock.register();
				cropsBlock.sendNotification(player);
			}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void ablityCultivator(CropsCultivateEvent event) {
		if (!event.isCancelled()) {
			final AbilityTypeCultivator ability = AbilityTypeCultivator.getInstance();
			
			if (ability.isRegistered()) {
				final Player player = event.getPlayer();
				
				if (ability.isAllowed(player)) {
					final double abilityEffectValue = ability.getPlayerEffectValue(player);
					final int duration = event.getDuration();
					final int durationFinal = (int) (duration * ((100 - abilityEffectValue) / 100));
					
					event.setDuration(durationFinal);
				}
			}
		}
	}
}