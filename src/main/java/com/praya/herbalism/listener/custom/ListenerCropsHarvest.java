package com.praya.herbalism.listener.custom;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

import com.praya.agarthalib.utility.MathUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.ability.type.AbilityTypeLuck;
import com.praya.herbalism.event.CropsHarvestEvent;
import com.praya.herbalism.event.PlayerHerbalismExpChangeEvent.ExpChangeReason;
import com.praya.herbalism.handler.HandlerEvent;
import com.praya.herbalism.manager.player.PlayerHerbalismManager;
import com.praya.herbalism.manager.player.PlayerManager;
import com.praya.herbalism.player.PlayerHerbalism;

public final class ListenerCropsHarvest extends HandlerEvent implements Listener {
	
	public ListenerCropsHarvest(Herbalism plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void cropsHarvestEvent(CropsHarvestEvent event) {
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerHerbalismManager playerHerbalismManager = playerManager.getPlayerHerbalismManager();
		
		if (!event.isCancelled()) {
			final Block block = event.getBlock();
			final Location location = block.getLocation().add(0.5, 0, 0.5);
			final World world = block.getWorld();
			final Player player = event.getPlayer();
			final PlayerHerbalism playerHerbalism = playerHerbalismManager.getPlayerHerbalism(player);
			
			if (playerHerbalism != null) {
				final float exp = (float) event.getExp();
				final List<ItemStack> items = event.getItems();
				
				for (ItemStack item : items) {
					world.dropItemNaturally(location, item);
				}
				
				playerHerbalism.addExp(exp, ExpChangeReason.CROPS);
			}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void abilityLuck(CropsHarvestEvent event) {
		if (!event.isCancelled()) {
			final AbilityTypeLuck ability = AbilityTypeLuck.getInstance();
			
			if (ability.isRegistered()) {
				final Player player = event.getPlayer();
				
				if (ability.isAllowed(player)) {
					final double chance = ability.getPlayerEffectChance(player);
					
					if (MathUtil.chanceOf(chance)) {
						final List<ItemStack> items = event.getItems();
						final int multiplier = ability.getPlayerEffectValue(player);
						
						for (ItemStack item : items) {
							final int amount = item.getAmount() * multiplier;
							
							item.setAmount(amount);
						}
					}
				}
			}
		}
	}
}