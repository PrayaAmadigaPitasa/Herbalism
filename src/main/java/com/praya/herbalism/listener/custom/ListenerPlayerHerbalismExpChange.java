package com.praya.herbalism.listener.custom;

import java.util.HashMap;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.branch.SoundEnum;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.HerbalismConfig;
import com.praya.herbalism.event.PlayerHerbalismExpChangeEvent;
import com.praya.herbalism.event.PlayerHerbalismExpChangeEvent.ExpChangeReason;
import com.praya.herbalism.handler.HandlerEvent;
import com.praya.herbalism.language.Language;
import com.praya.herbalism.manager.player.PlayerHerbalismManager;
import com.praya.herbalism.manager.player.PlayerManager;
import com.praya.herbalism.player.PlayerHerbalism;

public final class ListenerPlayerHerbalismExpChange extends HandlerEvent implements Listener {
	
	public ListenerPlayerHerbalismExpChange(Herbalism plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void playerHerbalismExpChangeEvent(PlayerHerbalismExpChangeEvent event) {
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerHerbalismManager playerHerbalismManager = playerManager.getPlayerHerbalismManager();
		final HerbalismConfig mainConfig = plugin.getMainConfig();
		final ExpChangeReason reason = event.getReason();
		
		if (reason.equals(ExpChangeReason.CROPS)) {
			final Player player = event.getOnlinePlayer();
			
			if (player != null) {
				final PlayerHerbalism playerHerbalism = playerHerbalismManager.getPlayerHerbalism(player);
				
				if (playerHerbalism != null) {
					final float expPlayer = playerHerbalism.getExp();
					final float exp = event.getExp();
					
					if (exp > expPlayer) {
						final String informationID = "Herbalism Exp";
						final int cooldown = 2500;
						final int priority = mainConfig.getPriorityActionbar();
						final float expGain = exp - expPlayer;
						final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
							
						String message = Language.ACTIONBAR_PLAYER_EXP_GAIN.getText(player);
						
						mapPlaceholder.put("exp_gain", String.valueOf(MathUtil.roundNumber(expGain)));
						
						message = TextUtil.placeholder(mapPlaceholder, message);
						
						Bridge.getBridgeMessage().sendProtectedActionbar(player, message, informationID, priority, cooldown, true);
						SenderUtil.playSound(player, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
					}
				}
			}
		}
	}
}