package com.praya.herbalism.plugin;

import java.util.Collection;

import org.bukkit.plugin.java.JavaPlugin;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.manager.plugin.PluginPropertiesManager;

import core.praya.agarthalib.builder.plugin.PluginPropertiesBuild;
import core.praya.agarthalib.builder.plugin.PluginPropertiesResourceBuild;
import core.praya.agarthalib.builder.plugin.PluginPropertiesStreamBuild;

public final class PluginPropertiesMemory extends PluginPropertiesManager {
	
	private final PluginPropertiesConfig pluginPropertiesConfig;
	private final PluginPropertiesResourceBuild pluginPropertiesResource;
	private final PluginPropertiesStreamBuild pluginPropertiesStream;
	
	private PluginPropertiesMemory(Herbalism plugin) {
		super(plugin);
		
		final String name = plugin.getName();
		final PluginPropertiesConfig pluginPropertiesConfig = new PluginPropertiesConfig(plugin);
		final PluginPropertiesResourceBuild pluginPropertiesResource = PluginPropertiesBuild.getPluginPropertiesResource(plugin, plugin.getPluginType(), plugin.getPluginVersion());
		final PluginPropertiesStreamBuild pluginPropertiesStream = pluginPropertiesConfig.mapPluginProperties.get(name);
		
		this.pluginPropertiesConfig = pluginPropertiesConfig;
		this.pluginPropertiesResource = pluginPropertiesResource;
		this.pluginPropertiesStream = pluginPropertiesStream != null ? pluginPropertiesStream : new PluginPropertiesStreamBuild();
	}
	
	private static class PluginPropertiesMemorySingleton {
		private static final PluginPropertiesMemory instance;
		
		static {
			final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
			
			instance = new PluginPropertiesMemory(plugin);
		}
	}
	
	public static final PluginPropertiesMemory getInstance() {
		return PluginPropertiesMemorySingleton.instance;
	}
	
	public final PluginPropertiesConfig getPluginPropertiesConfig() {
		return this.pluginPropertiesConfig;
	}
	
	@Override
	public final PluginPropertiesResourceBuild getPluginPropertiesResource() {
		return this.pluginPropertiesResource;
	}
	
	@Override
	public final PluginPropertiesStreamBuild getPluginPropertiesStream() {
		return this.pluginPropertiesStream;
	}
	
	@Override
	public final Collection<String> getPluginIds() {
		return getPluginPropertiesConfig().mapPluginProperties.keySet();
	}
	
	@Override
	public final Collection<PluginPropertiesStreamBuild> getAllPluginProperties() {
		return getPluginPropertiesConfig().mapPluginProperties.values();
	}
	
	@Override
	public final PluginPropertiesStreamBuild getPluginProperties(String id) {
		if (id != null) {
			for (String key : getPluginIds()) {
				if (key.equalsIgnoreCase(id)) {
					return getPluginPropertiesConfig().mapPluginProperties.get(key);
				}
			}
		}
		
		return null;
	}
}
