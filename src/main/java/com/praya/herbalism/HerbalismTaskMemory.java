package com.praya.herbalism;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.crops.CropsBlockTaskMemory;
import com.praya.herbalism.crops.CropsHologramTaskMemory;
import com.praya.herbalism.manager.task.TaskCropsBlockManager;
import com.praya.herbalism.manager.task.TaskCropsHologramManager;
import com.praya.herbalism.manager.task.TaskManager;
import com.praya.herbalism.manager.task.TaskPlayerBossBarManager;
import com.praya.herbalism.manager.task.TaskPlayerHerbalismManager;
import com.praya.herbalism.player.PlayerBossBarTaskMemory;
import com.praya.herbalism.player.PlayerHerbalismTaskMemory;

public final class HerbalismTaskMemory extends TaskManager {

	private final TaskCropsBlockManager taskCropsBlockManager;
	private final TaskCropsHologramManager taskCropsHologramManager;
	private final TaskPlayerBossBarManager taskPlayerBossBarManager;
	private final TaskPlayerHerbalismManager taskPlayerHerbalismManager;
	
	protected HerbalismTaskMemory(Herbalism plugin) {
		super(plugin);
		
		this.taskCropsBlockManager = CropsBlockTaskMemory.getInstance();
		this.taskCropsHologramManager = CropsHologramTaskMemory.getInstance();
		this.taskPlayerBossBarManager = PlayerBossBarTaskMemory.getInstance();
		this.taskPlayerHerbalismManager = PlayerHerbalismTaskMemory.getInstance();
	}
	
	public final TaskCropsBlockManager getTaskCropsBlockManager() {
		return this.taskCropsBlockManager;
	}
	
	public final TaskCropsHologramManager getTaskCropsHologramManager() {
		return this.taskCropsHologramManager;
	}
	
	public final TaskPlayerBossBarManager getTaskPlayerBossBarManager() {
		return this.taskPlayerBossBarManager;
	}
	
	public final TaskPlayerHerbalismManager getTaskPlayerHerbalismManager() {
		return this.taskPlayerHerbalismManager;
	}
}
