package com.praya.herbalism.metrics;

import org.bukkit.plugin.java.JavaPlugin;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.manager.plugin.MetricsManager;
import com.praya.herbalism.metrics.service.BStats;

public class MetricsMemory extends MetricsManager {

	private BStats metricsBStats;
	
	private MetricsMemory(Herbalism plugin) {
		super(plugin);
		
		this.metricsBStats = new BStats(plugin);
	}
	
	private static class MetricsMemorySingleton {
		private static final MetricsMemory instance;
		
		static {
			final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
			
			instance = new MetricsMemory(plugin);
		}
	}
	
	public static final MetricsMemory getInstance() {
		return MetricsMemorySingleton.instance;
	}
	
	@Override
	public final BStats getMetricsBStats() {
		return this.metricsBStats;
	}
}
