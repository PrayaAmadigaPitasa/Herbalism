package com.praya.herbalism.event;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.block.BlockEvent;

public class CropsCultivateEvent extends BlockEvent implements Cancellable {
	
	private static final HandlerList handlers = new HandlerList();
	
	private final Player player;
	private final String crops;
	
	private int duration;
	private boolean cancel;

    public CropsCultivateEvent(Block block, Player player, String crops, int duration) {
        super(block);
        
        this.player = player;
        this.crops = crops;
        
        setDuration(duration);
        setCancelled(false);
    }
    
    public final Player getPlayer() {
    	return this.player;
    }
    
    public final String getCrops() {
    	return this.crops;
    }
    
    public final int getDuration() {
    	return this.duration;
    }
    
    public final void setDuration(int duration) {
    	this.duration = Math.max(0, duration);
    }

	@Override
	public boolean isCancelled() {
		return this.cancel;
	}

	@Override
	public void setCancelled(boolean cancel) {
		this.cancel = cancel;
	}
	
	public HandlerList getHandlers() {
        return handlers;
    }

    public static final HandlerList getHandlerList() {
        return handlers;
    }
}
