package com.praya.herbalism.event;

import java.util.UUID;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.praya.agarthalib.utility.PlayerUtil;

public class PlayerHerbalismLevelChangeEvent extends Event {
	
	private static final HandlerList handlers = new HandlerList();
	
	private final UUID playerID;
	private final int level;
	private final LevelChangeReason reason;

	public PlayerHerbalismLevelChangeEvent(OfflinePlayer player, int level) {
		this(player, level, LevelChangeReason.CUSTOM);
	}

    public PlayerHerbalismLevelChangeEvent(OfflinePlayer player, int level, LevelChangeReason reason) {
    	this(player.getUniqueId(), level, reason);
    }
	
	public PlayerHerbalismLevelChangeEvent(UUID playerID, int level) {
		this(playerID, level, LevelChangeReason.CUSTOM);
	}

    public PlayerHerbalismLevelChangeEvent(UUID playerID, int level, LevelChangeReason reason) {
        this.playerID = playerID;
        this.level = level;
        this.reason = reason;
    }
    
    public final OfflinePlayer getPlayer() {
    	return PlayerUtil.getPlayer(playerID);
    }
    
    public final Player getOnlinePlayer() {
    	return PlayerUtil.getOnlinePlayer(playerID);
    }
    
    public final boolean isPlayerOnline() {
    	return getOnlinePlayer() != null;
    }
    
    public final int getLevel() {
    	return this.level;
    }
    
    public final LevelChangeReason getReason() {
    	return this.reason;
    }
	
	public HandlerList getHandlers() {
        return handlers;
    }

    public static final HandlerList getHandlerList() {
        return handlers;
    }
    
    public enum LevelChangeReason {
    	
    	EXP_UP,
    	COMMAND,
    	CUSTOM;
    }
}
