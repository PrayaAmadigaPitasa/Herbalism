package com.praya.herbalism.event;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.block.BlockEvent;
import org.bukkit.inventory.ItemStack;

import com.praya.herbalism.crops.CropsBlock;

public class CropsHarvestEvent extends BlockEvent implements Cancellable {
	
	private static final HandlerList handlers = new HandlerList();
	
	private final CropsBlock cropsBlock; 
	private final Player player;
	private final List<ItemStack> items;
	
	private double exp;
	private boolean cancel;

    public CropsHarvestEvent(CropsBlock cropsBlock, Player player, List<ItemStack> items, double exp) {
        super(cropsBlock.getBlock());
        
        this.cropsBlock = cropsBlock;
        this.player = player;
        this.items = items;
        
        setExp(exp);
        setCancelled(false);
    }
    
    public final CropsBlock getCropsBlock() {
    	return this.cropsBlock;
    }
    
    public final Player getPlayer() {
    	return this.player;
    }
    
    public final List<ItemStack> getItems() {
    	return this.items;
    }
    
    public final double getExp() {
    	return this.exp;
    }
    
    public final void setExp(double exp) {
    	this.exp = Math.max(0, exp);
    }

	@Override
	public boolean isCancelled() {
		return this.cancel;
	}

	@Override
	public void setCancelled(boolean cancel) {
		this.cancel = cancel;
	}
	
	public HandlerList getHandlers() {
        return handlers;
    }

    public static final HandlerList getHandlerList() {
        return handlers;
    }
}
