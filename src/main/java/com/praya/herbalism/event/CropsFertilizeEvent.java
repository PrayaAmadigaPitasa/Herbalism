package com.praya.herbalism.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.block.BlockEvent;
import org.bukkit.inventory.ItemStack;

import com.praya.agarthalib.utility.MathUtil;
import com.praya.herbalism.crops.CropsBlock;

public class CropsFertilizeEvent extends BlockEvent implements Cancellable {
	
	private static final HandlerList handlers = new HandlerList();
	
	private final CropsBlock cropsBlock; 
	private final Player player;
	private final ItemStack fertilizer;
	
	private double duration;
	private double percent;
	private boolean cancel;

    public CropsFertilizeEvent(CropsBlock cropsBlock, Player player, ItemStack fertilizer, double seconds, double percent) {
        super(cropsBlock.getBlock());
        
        this.cropsBlock = cropsBlock;
        this.player = player;
        this.fertilizer = fertilizer;
        
        setDuration(seconds);
        setPercent(percent);
        setCancelled(false);
    }
    
    public final CropsBlock getCropsBlock() {
    	return this.cropsBlock;
    }
    
    public final Player getPlayer() {
    	return this.player;
    }
    
    public final ItemStack getFertilizer() {
    	return this.fertilizer;
    }
    
    public final double getDuration() {
    	return this.duration;
    }
    
    public final double getPercent() {
    	return this.percent;
    }
    
    public final void setDuration(double duration) {
    	this.duration = Math.max(0, duration);
    }
    
    public final void setPercent(double percent) {
    	this.percent = MathUtil.limitDouble(percent, 0, 100);
    }

	@Override
	public boolean isCancelled() {
		return this.cancel;
	}

	@Override
	public void setCancelled(boolean cancel) {
		this.cancel = cancel;
	}
	
	public HandlerList getHandlers() {
        return handlers;
    }

    public static final HandlerList getHandlerList() {
        return handlers;
    }
}
