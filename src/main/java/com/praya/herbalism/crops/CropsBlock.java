package com.praya.herbalism.crops;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.CropState;
import org.bukkit.Material;
import org.bukkit.NetherWartsState;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.material.CocoaPlant;
import org.bukkit.material.CocoaPlant.CocoaPlantSize;
import org.bukkit.material.Crops;
import org.bukkit.material.MaterialData;
import org.bukkit.material.NetherWarts;
import org.bukkit.material.Pumpkin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.database.DatabaseMarkType;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.MetadataUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.TextUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.HerbalismConfig;
import com.praya.herbalism.language.Language;
import com.praya.herbalism.manager.game.CropsHologramManager;
import com.praya.herbalism.manager.game.CropsPropertiesManager;
import com.praya.herbalism.manager.game.GameManager;
import com.praya.herbalism.manager.player.PlayerCropsHologramManager;
import com.praya.herbalism.manager.player.PlayerManager;
import com.praya.herbalism.util.SerializeUtil;

import api.praya.agarthalib.builder.support.SupportHolographicDisplays;
import api.praya.agarthalib.main.AgarthaLibAPI;
import api.praya.agarthalib.manager.plugin.SupportManagerAPI;
import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.builder.message.BossBar.Color;
import core.praya.agarthalib.builder.message.BossBar.Style;
import core.praya.agarthalib.enums.branch.MaterialEnum;

@SuppressWarnings("deprecation")
public class CropsBlock {
	
	private final String crops;
	private final Block block;
	private final BlockFace blockFace;
	private final UUID ownerId;
	
	private final long timePlant;
	private final long timeStop;
	private final List<UUID> fertilizerPlayer;
	
	private String ownerName;
	private long timeHarvest;
	
	public CropsBlock(String crops, Block block, OfflinePlayer owner, int seconds) {
		if (crops == null || block == null || owner == null) {
			throw new IllegalArgumentException();
		} else {
			final String ownerName = owner.getName();
			final UUID ownerId = owner.getUniqueId();
			final long timePlant = System.currentTimeMillis();
			final long timeStop = timePlant + (seconds * 1000);
			final BlockState blockState = block.getState();
			final MaterialData materialData = blockState.getData();
			final BlockFace blockFace = materialData instanceof CocoaPlant ? ((CocoaPlant) materialData).getFacing() : BlockFace.UP;
			
			this.crops = crops;
			this.ownerName = ownerName;
			this.ownerId = ownerId;
			this.block = block;
			this.blockFace = blockFace;
			this.timePlant = timePlant;
			this.timeStop = timeStop;
			this.timeHarvest = timeStop;
			this.fertilizerPlayer = new ArrayList<>();
		}
	}
	
	protected CropsBlock(String crops, Block block, BlockFace blockFace, UUID ownerId, long timePlant, long timeStop, long timeHarvest) {
		this(crops, block, blockFace, ownerId, timePlant, timeStop, timeHarvest, null);
	}
	
	protected CropsBlock(String crops, Block block, BlockFace blockFace, UUID ownerId, long timePlant, long timeStop, long timeHarvest, String ownerName) {
		this(crops, block, blockFace, ownerId, timePlant, timeStop, timeHarvest, ownerName, null);
	}
	
	protected CropsBlock(String crops, Block block, BlockFace blockFace, UUID ownerId, long timePlant, long timeStop, long timeHarvest, String ownerName, List<UUID> fertilizerPlayer) {
		if (crops == null || ownerId == null || block == null || blockFace == null) {
			throw new IllegalArgumentException();
		} else {
			this.crops = crops;
			this.ownerId = ownerId;
			this.block = block;
			this.blockFace = blockFace;
			this.timePlant = timePlant;
			this.timeStop = timeStop;
			this.timeHarvest = timeHarvest;
			this.ownerName = ownerName != null ? ownerName : "Unknown";
			this.fertilizerPlayer = fertilizerPlayer;
		}
	}
	
	public final String getCrops() {
		return this.crops;
	}
	
	public final Block getBlock() {
		return this.block;
	}
	
	public final BlockFace getBlockFace() {
		return this.blockFace;
	}
	
	public final UUID getOwnerId() {
		return this.ownerId;
	}
	
	public final String getOwnerName() {
		final UUID ownerId = getOwnerId();
		final Player owner = PlayerUtil.getOnlinePlayer(ownerId);
		
		if (owner != null) {
			final String ownerName = owner.getName();
			
			this.ownerName = ownerName;
		}
		
		return this.ownerName;
	}
	
	public final List<UUID> getFertilizerPlayer() {
		return this.fertilizerPlayer;
	}
	
	public final long getTimeDuration() {
		return this.timeStop - this.timePlant;
	}
	
	public final long getTimeHarvest() {
		return this.timeHarvest;
	}
	
	public final long getTimeDecay() {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final HerbalismConfig mainConfig = plugin.getMainConfig();
		final double timeDecayRelative = mainConfig.getCropsTimeDecayRelative();
		final long timeDecay = (long) (getTimeHarvest() + (getTimeDuration() * timeDecayRelative));
		
		return timeDecay;
	}
	
	public final long getTimeLeftHarvest() {
		final long now = System.currentTimeMillis();
		final long timeLeftHarvest = Math.max(0, getTimeHarvest() - now); 
		
		return timeLeftHarvest;
	}
	
	public final long getTimeLeftDecay() {
		final long now = System.currentTimeMillis();
		final long timeLeftDecay = Math.max(0, getTimeDecay() - now); 
		
		return timeLeftDecay;
	}
	
	public final long fertilize(double seconds) {
		return fertilize(seconds, 0);
	}
	
	public final long fertilize(double seconds, double percent) {
		return fertilize(null, seconds, percent);
	}
	
	public final long fertilize(Player player, double seconds) {
		return fertilize(player, seconds, 0);
	}
	
	public final long fertilize(Player player, double seconds, double percent) {
		if (player != null) {
			if (alreadyFertilized(player)) {
				return 0;
			} else {
				final UUID playerID = player.getUniqueId();
				
				this.fertilizerPlayer.add(playerID);
			}
		}
		
		final long timeLeftHarvest = getTimeLeftHarvest();
		final long timeCut = (long) Math.min(timeLeftHarvest, ((seconds * 1000) + (getTimeDuration() * percent / 100)));
		
		this.timeHarvest -= timeCut;
		
		return timeCut;
	}
	
	public final boolean isHarvestTime() {
		final long now = System.currentTimeMillis();
		
		return this.timeHarvest <= now;
	}
	
	public final boolean isDecayTime() {
		final long now = System.currentTimeMillis();
		
		return getTimeDecay() <= now;
	}
	
	public final boolean alreadyFertilized(Player player) {
		if (player != null) {
			final UUID playerID = player.getUniqueId();
			
			return this.fertilizerPlayer.contains(playerID);
		} else {
			return false;
		}
	}
	
	public final CropsProperties getCropsProperties() {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final GameManager gameManager = plugin.getGameManager();
		final CropsPropertiesManager cropsPropertiesManager = gameManager.getCropsPropertiesManager();
		final CropsProperties cropsProperties = cropsPropertiesManager.getCropsProperties(getCrops());
		
		return cropsProperties;
	}
	
	public final void update() {
		final CropsProperties cropsProperties = getCropsProperties();
		
		if (cropsProperties == null) {
			remove();
		} else {
			final CropsPlant cropsPlant = cropsProperties.getPlant();
			final MaterialEnum materialEnumPlant = cropsPlant.getMaterialEnumPlant();
			
			if (isDecayTime() || !checkSoil()) {
				remove(true);
			} else if (isHarvestTime()) {
				if (!isBlockHarvest()) {
					if (materialEnumPlant.matchMaterial(block)) {
						setBlockHarvest();
					} else {
						remove();
					}
				}
			} else {				
				if (materialEnumPlant.matchMaterial(block)) {
					final Block block = getBlock();
					final BlockState blockState = block.getState();
					final MaterialData materialData = blockState.getData();
					final double duration = getTimeDuration();
					
					if (cropsPlant.isStemPlant()) {
						final byte data = (byte) (((duration - getTimeLeftHarvest()) / duration) * 7);
						
						materialData.setData(data);
						
						blockState.setData(materialData);
						blockState.update();
					} else if (materialData instanceof Crops) {
						final Crops crops = (Crops) materialData;
						final int data = (int) (((duration - getTimeLeftHarvest()) / duration) * 7);
						
						switch (data) {
						case 0 : crops.setState(CropState.SEEDED); break;
						case 1 : crops.setState(CropState.GERMINATED); break;
						case 2 : crops.setState(CropState.VERY_SMALL); break;
						case 3 : crops.setState(CropState.SMALL); break;
						case 4 : crops.setState(CropState.MEDIUM); break;
						case 5 : crops.setState(CropState.TALL); break;
						case 6 : crops.setState(CropState.VERY_TALL); break;
						case 7 : crops.setState(CropState.RIPE); break;
						}
						
						blockState.setData(crops);
						blockState.update();
					} else if (materialData instanceof NetherWarts) {
						final NetherWarts netherWarts = (NetherWarts) materialData;
						final int data = (int) (((duration - getTimeLeftHarvest()) / duration) * 3);
						
						switch (data) {
						case 0 : netherWarts.setState(NetherWartsState.SEEDED); break;
						case 1 : netherWarts.setState(NetherWartsState.STAGE_ONE); break;
						case 2 : netherWarts.setState(NetherWartsState.STAGE_TWO); break;
						case 3 : netherWarts.setState(NetherWartsState.RIPE); break;
						}
						
						blockState.setData(netherWarts);
						blockState.update();
					} else if (materialData instanceof CocoaPlant) {
						final CocoaPlant cocoaPlant = (CocoaPlant) materialData;
						final int data = (int) (((duration - getTimeLeftHarvest()) / duration) * 2);
						
						switch (data) {
						case 0 : cocoaPlant.setSize(CocoaPlantSize.SMALL); break;
						case 1 : cocoaPlant.setSize(CocoaPlantSize.MEDIUM); break;
						case 2 : cocoaPlant.setSize(CocoaPlantSize.LARGE); break;
						}
						
						blockState.setData(cocoaPlant);
						blockState.update();
					}
				} else {
					remove();
				}
			}
		}
	}
	
	public final void sendNotification(Player player) {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final AgarthaLibAPI agarthaLibAPI = AgarthaLibAPI.getInstance();
		final SupportManagerAPI supportManagerAPI = agarthaLibAPI.getPluginManagerAPI().getSupportManager();
		final SupportHolographicDisplays supportHolographicDisplays = supportManagerAPI.getSupportHolographicDisplays();
		final GameManager gameManager = plugin.getGameManager();
		final PlayerManager playerManager = plugin.getPlayerManager();
		final CropsHologramManager cropsHologramManager = gameManager.getCropsHologramManager();
		final PlayerCropsHologramManager playerCropsHologramManager = playerManager.getPlayerCropsHologramManager();
		final HerbalismConfig mainConfig = plugin.getMainConfig();
		
		if (player != null) {
			final boolean enableHologram = mainConfig.getCropsEnableHologram();
			final boolean enableBossBar = mainConfig.getCropsEnableBossBar();
			final boolean enableActionbar = mainConfig.getCropsEnableActionbar();
			
			if (enableHologram && supportHolographicDisplays != null) {
				final CropsHologram cropsHologramPlayer = playerCropsHologramManager.getPlayerCropsHologram(player);
				final CropsHologram cropsHologram = cropsHologramManager.getCropsHologram(block);
				
				if (cropsHologramPlayer != null && cropsHologramPlayer != cropsHologram) {
					cropsHologramPlayer.remove();
				}
				
				if (cropsHologram != null) {
					cropsHologram.updateDuration();
					cropsHologram.updateLines(player);
				} else {
					final CropsHologram cropsHologramNew = new CropsHologram(this, player);
					
					cropsHologramNew.register();
					
				}
			}
			
			if (enableBossBar || enableActionbar) {
				final CropsProperties cropsProperties = getCropsProperties();
				final String informationID = "Herbalism Information";
				final String cropsName = cropsProperties.getName();
				final String cropsOwner = getOwnerName();
				final int cooldown = 2500;
				final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
				
				mapPlaceholder.put("crops_name", cropsName);
				mapPlaceholder.put("crops_owner", cropsOwner);
				
				if (enableBossBar) {
					final int priorityBossBar = mainConfig.getPriorityBossBar();
					final float progress;
	                final Color color;
	                final Style style;
	                
	                String message;
	                
	                if (isHarvestTime()) {
	                	progress = (float) ((double) getTimeLeftDecay() / (getTimeDecay() - getTimeHarvest()));
	                	message = Language.BOSSBAR_CROPS_INFORMATION_HARVEST.getText(player);
	                } else {
	                	progress = (float) (1 - (double) getTimeLeftHarvest() / getTimeDuration());
	                	message = Language.BOSSBAR_CROPS_INFORMATION_GROW.getText(player);
	                }
	                
	                if (progress < 1D / 3) {
	                    color = mainConfig.getBossBarColorSmall();
	                    style = mainConfig.getBossBarStyleSmall();
	                } else if (progress < 2D / 3) {
	                    color = mainConfig.getBossBarColorMedium();
	                    style = mainConfig.getBossBarStyleMedium();
	                } else {
	                    color = mainConfig.getBossBarColorTall();
	                    style = mainConfig.getBossBarStyleTall();
	                }
	                
	                message = TextUtil.placeholder(mapPlaceholder, message);
	                
                    MetadataUtil.setCooldown(player, informationID, cooldown);
                    Bridge.getBridgeMessage().sendProtectedBossBar(player, message, informationID, priorityBossBar, cooldown, color, style, progress);
				}
				
				if (enableActionbar) {
					final int priorityActionbar = mainConfig.getPriorityActionbar();
                    
                    String message = isHarvestTime() ? Language.ACTIONBAR_CROPS_INFORMATION_HARVEST.getText(player) : Language.ACTIONBAR_CROPS_INFORMATION_GROW.getText(player);
                    
                    message = TextUtil.placeholder(mapPlaceholder, message);
                    
                    Bridge.getBridgeMessage().sendProtectedActionbar(player, message, informationID, priorityActionbar, cooldown);
				}
			}
		}
	}
	
	public final boolean save() {
		final CropsBlockMemory cropsBlockMemory = CropsBlockMemory.getInstance();
		
		return cropsBlockMemory.addDataEntry(this, DatabaseMarkType.SAVE);
	}
	
	public final boolean register() {
		final CropsBlockMemory cropsBlockMemory = CropsBlockMemory.getInstance();
		
		return cropsBlockMemory.register(this);
	}
	
	public final boolean remove() {
		return remove(false);
	}
	
	public final boolean remove(boolean clean) {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final AgarthaLibAPI agarthaLibAPI = AgarthaLibAPI.getInstance();
		final SupportManagerAPI supportManagerAPI = agarthaLibAPI.getPluginManagerAPI().getSupportManager();
		final SupportHolographicDisplays supportHolographicDisplays = supportManagerAPI.getSupportHolographicDisplays();
		final GameManager gameManager = plugin.getGameManager();
		final PlayerManager playerManager = plugin.getPlayerManager();
		final CropsHologramManager cropsHologramManager = gameManager.getCropsHologramManager();
		final PlayerCropsHologramManager playerCropsHologramManager = playerManager.getPlayerCropsHologramManager();
		final CropsBlockMemory cropsBlockMemory = CropsBlockMemory.getInstance();
		final String informationID = "Herbalism Information";
		final Player player = PlayerUtil.getOnlinePlayer(getOwnerId());
		final Block block = getBlock();
		
		if (cropsBlockMemory.unregister(this)) {
			if (clean) {
				getBlock().setType(Material.AIR);
			}
			
			if (supportHolographicDisplays != null) {
				if (player != null) {
					final CropsHologram cropsHologram = playerCropsHologramManager.getPlayerCropsHologram(player);
					
					if (cropsHologram != null) {
						cropsHologram.remove();
					}
				}
				
				final CropsHologram cropsHologram = cropsHologramManager.getCropsHologram(block);
				
				if (cropsHologram != null) {
					cropsHologram.remove();
				}
			}
			
			if (player != null) {
				Bridge.getBridgeMessage().removeBossBar(player, informationID);
				
			}
			
			return true;
		} else {
			return false;
		}
	}
	
	private final boolean isBlockHarvest() {
		final Block block = getBlock();
		final CropsProperties cropsProperties = getCropsProperties();
		final CropsPlant plant = cropsProperties.getPlant();
		final Material materialBlock = block.getType();
		final Material materialHarvest = plant.getMaterialHarvest();
		
		if (materialBlock.equals(materialHarvest)) {
			final BlockState blockState = block.getState();
			final MaterialData materialData = blockState.getData();
			
			if (materialData instanceof Crops) {
				final Crops crops = (Crops) materialData;
				final CropState state = crops.getState();
				
				return state.equals(CropState.RIPE);
			} if (materialData instanceof NetherWarts) {
				final NetherWarts netherWarts = (NetherWarts) materialData;
				final NetherWartsState state = netherWarts.getState();
				
				return state.equals(NetherWartsState.RIPE);
			} else if (materialData instanceof CocoaPlant) {
				final CocoaPlant cocoaPlant = (CocoaPlant) materialData;
				final CocoaPlantSize size = cocoaPlant.getSize();
				
				return size.equals(CocoaPlantSize.LARGE);
			} else {
				return true;
			}
		}
		
		return false;
	}
	
	private final void setBlockHarvest() {
		final Block block = getBlock();
		final CropsProperties cropsProperties = getCropsProperties();
		final CropsPlant plant = cropsProperties.getPlant();
		final Material materialHarvest = plant.getMaterialHarvest();
		
		block.setType(materialHarvest);
		
		final BlockState blockState = block.getState();
		final MaterialData materialData = blockState.getData();
		
		if (materialData instanceof Crops) {
			final Crops crops = (Crops) materialData;
			
			crops.setState(CropState.RIPE);
			blockState.setData(crops);
			blockState.update();
		} else if (materialData instanceof NetherWarts) {
			final NetherWarts netherWarts = (NetherWarts) materialData;
			
			netherWarts.setState(NetherWartsState.RIPE);
			blockState.setData(netherWarts);
			blockState.update();
		} else if (materialData instanceof CocoaPlant) {
			final CocoaPlant cocoaPlant = (CocoaPlant) materialData;
			final BlockFace face = getBlockFace();
			
			cocoaPlant.setSize(CocoaPlantSize.LARGE);
			cocoaPlant.setFacingDirection(face);
			blockState.setData(cocoaPlant);
			blockState.update();
		} else if (materialData instanceof Pumpkin) {
			final Pumpkin pumpkin = (Pumpkin) materialData;
			final int data = MathUtil.random(4);
			final BlockFace face;
			
			if (data <= 1) {
				face = BlockFace.NORTH;
			} else if (data <= 2) {
				face = BlockFace.SOUTH;
			} else if (data <= 3) {
				face = BlockFace.WEST;
			} else {
				face = BlockFace.SOUTH;
			}
			
			pumpkin.setFacingDirection(face);
			blockState.setData(pumpkin);
			blockState.update();
		}
	}
	
	private final boolean checkSoil() {
		final Block block = getBlock();
		final BlockState blockState = block.getState();
		final MaterialData materialData = blockState.getData();
		
		if (materialData instanceof CocoaPlant) {
			final CocoaPlant cocoaPlant = (CocoaPlant) materialData;
			final BlockFace face = cocoaPlant.getFacing();
			final Block blockRelative = block.getRelative(face);
			final Material materialRelative = blockRelative.getType();
			
			if (materialRelative.equals(Material.AIR)) {
				return false;
			}
		} else {
			final Block blockRelative = block.getRelative(BlockFace.DOWN);
			final Material materialRelative = blockRelative.getType();
			
			if (materialRelative.equals(Material.AIR)) {
				return false;
			}
		}
		
		return true;
	}
	
	public final String serialize() {
		final FileConfiguration config = new YamlConfiguration();
		final String serializeBlock = SerializeUtil.serializeBlock(this.block);
		final List<String> listFertilizer = new ArrayList<String>();
		
		for (UUID playerId : this.fertilizerPlayer) {
			listFertilizer.add(playerId.toString());
		}
		
		config.set("crops", this.crops);
		config.set("ownerName", this.ownerName);
		config.set("ownerId", this.ownerId.toString());
		config.set("block", serializeBlock);
		config.set("blockFace", this.blockFace.toString());
		config.set("timePlant", this.timePlant);
		config.set("timeStop", this.timeStop);
		config.set("timeHarvest", this.timeHarvest);
		config.set("fertilizerPlayer", listFertilizer);
		
		return config.saveToString();
	}
	
	public static final CropsBlock deserialize(String serialize) throws InvalidConfigurationException {
		if (serialize != null) {
			final FileConfiguration config = new YamlConfiguration();
			final long now = System.currentTimeMillis();
			final List<UUID> listFertilizer = new ArrayList<UUID>(); 
			
			config.loadFromString(serialize);
			
			String crops = null;
			String ownerName = null;
			UUID ownerId = null;
			Block block = null;
			BlockFace blockFace = null;
			long timePlant = now;
			long timeStop = now;
			long timeHarvest = now;
			
			for (String key : config.getKeys(false)) {
				if (key.equalsIgnoreCase("crops")) {
					crops = config.getString(key);
				} else if (key.equalsIgnoreCase("ownerName")) {
					ownerName = config.getString(key);
				} else if (key.equalsIgnoreCase("ownerId")) {
					ownerId = UUID.fromString(config.getString(key));
				} else if (key.equalsIgnoreCase("block")) {
					block = SerializeUtil.deserializeBlock(config.getString(key)); 
				} else if (key.equalsIgnoreCase("blockFace")) {
					final String textBlockFace = config.getString(key);
					
					for (BlockFace face : BlockFace.values()) {
						if (face.toString().equalsIgnoreCase(textBlockFace)) {
							blockFace = face;
							break;
						}
					}
				} else if (key.equalsIgnoreCase("timePlant")) {
					timePlant = config.getLong(key);
				} else if (key.equalsIgnoreCase("timeStop")) {
					timeStop = config.getLong(key);
				} else if (key.equalsIgnoreCase("timeHarvest")) {
					timeHarvest = config.getLong(key);
				} else if (key.equalsIgnoreCase("fertilizerPlayer")) {
					for (String textPlayerID : config.getStringList(key)) {
						final UUID playerID = UUID.fromString(textPlayerID);
						
						listFertilizer.add(playerID);
					}
				}
			}
			
			if (crops != null && block != null && blockFace != null && ownerId != null) {
				final CropsBlock cropsBlock = new CropsBlock(crops, block, blockFace, ownerId, timePlant, timeStop, timeHarvest, ownerName, listFertilizer);
				
				return cropsBlock;
			}
		}
		
		return null;
	}
	
	public static final CropsBlock deserializeSilent(String serialize) {
		try {
			return deserialize(serialize);
		} catch (InvalidConfigurationException e) {
			return null;
		}
	}
}
