package com.praya.herbalism.crops;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.handler.HandlerTask;
import com.praya.herbalism.manager.game.CropsHologramManager;
import com.praya.herbalism.manager.game.GameManager;

public final class CropsHologramTask extends HandlerTask implements Runnable {

	protected CropsHologramTask(Herbalism plugin) {
		super(plugin);
	}

	@Override
	public void run() {
		final GameManager gameManager = plugin.getGameManager();
		final CropsHologramManager cropsHologramManager = gameManager.getCropsHologramManager();
		
		for (CropsHologram cropsHologram : cropsHologramManager.getAllCropsHologram()) {
			if (cropsHologram.isExpired()) {
				cropsHologram.remove();
			}
		}
	}
}
