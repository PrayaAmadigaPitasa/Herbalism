package com.praya.herbalism.crops;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.handler.HandlerTask;

public final class CropsBlockUpdateTask extends HandlerTask implements Runnable{

	protected CropsBlockUpdateTask(Herbalism plugin) {
		super(plugin);
	}

	@Override
	public void run() {
		final CropsBlockMemory cropsBlockMemory = CropsBlockMemory.getInstance();
		
		for (CropsBlock cropsBlock : cropsBlockMemory.getAllCropsBlock()) {
			cropsBlock.update();
		}
	}
}
