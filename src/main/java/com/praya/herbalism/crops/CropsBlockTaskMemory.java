package com.praya.herbalism.crops;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scheduler.BukkitTask;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.HerbalismConfig;
import com.praya.herbalism.manager.task.TaskCropsBlockManager;

public final class CropsBlockTaskMemory extends TaskCropsBlockManager {

	private BukkitTask taskCropsBlockDatabase;
	private BukkitTask taskCropsBlockUpdate;
	
	private CropsBlockTaskMemory(Herbalism plugin) {
		super(plugin);
		
		reloadTaskCropsBlockDatabase();
		reloadTaskCropsBlockUpdate();
	}
	
	private static class CropsBlockTaskMemorySingleton {
		private static final CropsBlockTaskMemory instance;
		
		static {
			final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
			
			instance = new CropsBlockTaskMemory(plugin);
		}
	}
	
	public static final CropsBlockTaskMemory getInstance() {
		return CropsBlockTaskMemorySingleton.instance;
	}
	
	@Override
	public final void reloadTaskCropsBlockDatabase() {
		if (this.taskCropsBlockDatabase != null) {
			this.taskCropsBlockDatabase.cancel();
		}
		
		this.taskCropsBlockDatabase = createTaskCropsBlockDatabase();
	}
	
	@Override
	public final void reloadTaskCropsBlockUpdate() {
		if (this.taskCropsBlockUpdate != null) {
			this.taskCropsBlockUpdate.cancel();
		}
		
		this.taskCropsBlockUpdate = createTaskCropsBlockUpdate();
	}
	
	private final BukkitTask createTaskCropsBlockDatabase() {
		final BukkitScheduler scheduler = Bukkit.getScheduler();
		final Runnable runnable = new CropsBlockDatabaseTask(plugin);
		final BukkitTask task = scheduler.runTaskTimerAsynchronously(plugin, runnable, 0, 1);
		
		return task;
	}
	
	private final BukkitTask createTaskCropsBlockUpdate() {
		final HerbalismConfig mainConfig = plugin.getMainConfig();
		final BukkitScheduler scheduler = Bukkit.getScheduler();
		final Runnable runnable = new CropsBlockUpdateTask(plugin);
		final int period = mainConfig.getCropsPeriodUpdate();
		final BukkitTask task = scheduler.runTaskTimer(plugin, runnable, period, period);
		
		return task;
	}
}
