package com.praya.herbalism.crops;

import org.bukkit.inventory.ItemStack;

import core.praya.agarthalib.enums.branch.MaterialEnum;
import com.praya.agarthalib.utility.EquipmentUtil;

public class CropsProperties {

	private final String id;
	private final String name;
	private final String category;
	private final boolean sustainable;
	private final CropsRequirement cropsRequirement;
	private final CropsDrop cropsDrop;
	
	private CropsPlant plant;
	private ItemStack icon;
	private int duration;
	
	public CropsProperties(String id, String name, String category, boolean sustainable, CropsRequirement cropsRequirement, CropsDrop cropsDrop, CropsPlant plant, ItemStack icon, int seconds) {
		if (id == null) {
			throw new IllegalArgumentException();
		} else {
			this.id = id;
			this.name = name;
			this.category = category;
			this.sustainable = sustainable;
			this.cropsRequirement = cropsRequirement;
			this.cropsDrop = cropsDrop;
	
			setPlant(plant);
			setIcon(icon);
			setDuration(seconds);
		}
	}
	
	public final String getId() {
		return this.id;
	}
	
	public final String getName() {
		return this.name;
	}
	
	public final String getCategory() {
		return this.category;
	}
	
	public final boolean isSustainable() {
		return this.sustainable;
	}
	
	public final CropsRequirement getCropsRequirement() {
		return this.cropsRequirement;
	}
	
	public final CropsDrop getCropsDrop() {
		return this.cropsDrop;
	}
	
	public final CropsPlant getPlant() {
		return this.plant;
	}
	
	public final ItemStack getIcon() {
		return this.icon;
	}
	
	public final int getDuration() {
		return this.duration;
	}
	
	public final void setPlant(CropsPlant plant) {
		this.plant = plant != null ? plant : CropsPlant.CROPS;
	}
	
	public final void setIcon(ItemStack icon) {
		if (EquipmentUtil.isSolid(icon)) {
			this.icon = icon.clone();
		} else {
			this.icon = EquipmentUtil.createItem(MaterialEnum.WHEAT);
		}
	}
	
	public final void setDuration(int seconds) {
		this.duration = Math.max(0, seconds);
	}
}
