package com.praya.herbalism.crops;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import com.gmail.filoghost.holographicdisplays.api.Hologram;

import api.praya.agarthalib.builder.support.SupportHolographicDisplays;
import api.praya.agarthalib.main.AgarthaLibAPI;
import api.praya.agarthalib.manager.plugin.SupportManagerAPI;
import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.TextUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.HerbalismConfig;
import com.praya.herbalism.language.Language;
import com.praya.herbalism.player.PlayerCropsHologramMemory;
import com.praya.herbalism.util.TimeUtil;

public class CropsHologram {
	
	private final CropsBlock cropsBlock;
	private final Hologram hologram;
	private final UUID viewerId;
	
	private long expired;
	
	protected CropsHologram(CropsBlock cropsBlock, Player player) {
		if (cropsBlock == null || player == null) {
			throw new IllegalArgumentException();
		} else {
			final AgarthaLibAPI agarthaLibAPI = AgarthaLibAPI.getInstance();
			final SupportManagerAPI supportManagerAPI = agarthaLibAPI.getPluginManagerAPI().getSupportManager();
			final SupportHolographicDisplays supportHolographicDisplays = supportManagerAPI.getSupportHolographicDisplays();
			final CropsProperties cropsProperties = cropsBlock.getCropsProperties();
			final CropsPlant cropsPlant = cropsProperties.getPlant();
			final UUID viewerId = player.getUniqueId();
			final Block block = cropsBlock.getBlock();
			final Location location = block.getLocation();
			final double height = cropsPlant.getHeight();
			final List<String> listText = cropsBlock.isHarvestTime() ? Language.HOLOGRAM_CROPS_INFORMATION_HARVEST.getListText(player) : Language.HOLOGRAM_CROPS_INFORMATION_GROW.getListText(player);
			
			location.add(0.5, (listText.size() * 0.25) + height, 0.5);
			
			this.cropsBlock = cropsBlock;
			this.hologram = supportHolographicDisplays.createHologram(location);
			this.viewerId = viewerId;
			
			updateDuration();
			updateLines(player);
		}
	}
	
	public final CropsBlock getCropsBlock() {
		return this.cropsBlock;
	}
	
	public final Hologram getHologram() {
		return this.hologram;
	}
	
	public final UUID getViewerId() {
		return this.viewerId;
	}
	
	public final boolean isExpired() {
		final long now = System.currentTimeMillis();
		
		return this.expired <= now;
	}
	
	public final void updateDuration() {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final HerbalismConfig mainConfig = plugin.getMainConfig();
		final int duration = mainConfig.getCropsDurationHologram();
		final long now = System.currentTimeMillis();
		final long expired = now + (duration * 50);
		
		this.expired = expired;
	}
	
	public final void updateLines(Player player) {
		final AgarthaLibAPI agarthaLibAPI = AgarthaLibAPI.getInstance();
		final SupportManagerAPI supportManagerAPI = agarthaLibAPI.getPluginManagerAPI().getSupportManager();
		final SupportHolographicDisplays supportHolographicDisplays = supportManagerAPI.getSupportHolographicDisplays();
		final CropsBlock cropsBlock = getCropsBlock();
		final CropsProperties cropsProperties = cropsBlock.getCropsProperties();
		final CropsPlant cropsPlant = cropsProperties.getPlant();
		final Block block = cropsBlock.getBlock();
		final Location location = block.getLocation();
		final String cropsName = cropsProperties.getName();
		final String cropsOwner = cropsBlock.getOwnerName();
		final ItemStack icon = cropsProperties.getIcon();
		final Material iconMaterial = EquipmentUtil.getMaterial(icon);
		final short iconData = EquipmentUtil.getData(icon);
		final double height = cropsPlant.getHeight();
		final int timeLeftHarvest = (int) (cropsBlock.getTimeLeftHarvest() / 1000);
		final int timeLeftDecay = (int) (cropsBlock.getTimeLeftDecay() / 1000);
		final String textTimeLeftHarvest = TimeUtil.getTime(timeLeftHarvest);
		final String textTimeLeftDecay = TimeUtil.getTime(timeLeftDecay);
		final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
		
		String iconText = "item: " + iconMaterial + ", data: " + iconData;
		List<String> listText = timeLeftHarvest > 0 ? Language.HOLOGRAM_CROPS_INFORMATION_GROW.getListText(player) : Language.HOLOGRAM_CROPS_INFORMATION_HARVEST.getListText(player);
		
		if (!icon.getEnchantments().isEmpty()) {
			iconText = iconText + ", shiny: true";
		}
		
		mapPlaceholder.put("crops_name", cropsName);
		mapPlaceholder.put("crops_owner", cropsOwner);
		mapPlaceholder.put("crops_time_harvest", textTimeLeftHarvest);
		mapPlaceholder.put("crops_time_decay", textTimeLeftDecay);
		mapPlaceholder.put("crops_icon", iconText);
		
		listText = TextUtil.placeholder(mapPlaceholder, listText);
		listText = TextUtil.colorful(listText);
		
		location.add(0.5, (listText.size() * 0.25) + height, 0.5);
		supportHolographicDisplays.setLineText(hologram, listText);
		hologram.teleport(location);
	}
	
	public final boolean register() {
		final PlayerCropsHologramMemory playerCropsHologramMemory = PlayerCropsHologramMemory.getInstance();
		final CropsHologramMemory cropsHologramMemory = CropsHologramMemory.getInstance();
		
		if (cropsHologramMemory.register(this)) {
			playerCropsHologramMemory.register(this);
			return true;
		} else {
			return false;
		}
	}
	
	public final boolean remove() {
		final PlayerCropsHologramMemory playerCropsHologramMemory = PlayerCropsHologramMemory.getInstance();
		final CropsHologramMemory cropsHologramMemory = CropsHologramMemory.getInstance();
		
		hologram.delete();
		
		if (cropsHologramMemory.unregister(this)) {
			playerCropsHologramMemory.unregister(this);
			return true;
		} else {
			return false;
		}
	}
}
