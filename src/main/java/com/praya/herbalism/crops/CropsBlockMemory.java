package com.praya.herbalism.crops;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;

import org.bukkit.block.Block;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import com.praya.agarthalib.database.DatabaseMarkType;
import com.praya.agarthalib.database.DatabaseType;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.HerbalismConfig;
import com.praya.herbalism.crops.CropsBlock;
import com.praya.herbalism.manager.game.CropsBlockManager;
import com.praya.herbalism.util.SerializeUtil;

public final class CropsBlockMemory extends CropsBlockManager {
	
	protected final HashMap<Block, CropsBlock> mapCropsBlock; 
	protected final List<Entry<Object, DatabaseMarkType>> listDataEntry;
	
	private CropsBlockDatabase cropsBlockDatabase;
	
	private CropsBlockMemory(Herbalism plugin) {
		super(plugin);
		
		this.mapCropsBlock = new HashMap<Block, CropsBlock>();
		this.listDataEntry = new ArrayList<Entry<Object, DatabaseMarkType>>();
		
		setupDatabase();
	}
	
	private static class CropsBlockMemorySingleton {
		private static final CropsBlockMemory instance;
		
		static {
			final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
			
			instance = new CropsBlockMemory(plugin);
		}
	}
	
	public static final CropsBlockMemory getInstance() {
		return CropsBlockMemorySingleton.instance;
	}
	
	protected final CropsBlockDatabase getDatabaseCropsBlock() {
		return cropsBlockDatabase;
	}
	
	public final void setupDatabase() {
		final HerbalismConfig mainConfig = plugin.getMainConfig();
		final DatabaseType databaseType = mainConfig.getDatabaseServerType();
		
		if (this.cropsBlockDatabase != null) {
			this.cropsBlockDatabase.closeConnection();
		}
		
		switch (databaseType) {
		case MySQL:
		case SQLite:
			this.cropsBlockDatabase = new CropsBlockDatabaseSQL(plugin);
			break;
		default: 
			this.cropsBlockDatabase = null;
		}
		
		clearCache();
		loadCacheFromDatabase();
	}
	
	@Override
	public final Collection<Block> getBlocks() {
		return getBlocks(true);
	}
	
	protected final Collection<Block> getBlocks(boolean clone) {
		final Collection<Block> blocks = this.mapCropsBlock.keySet();
		
		return clone ? new ArrayList<Block>(blocks) : blocks;
	}
	
	@Override
	public final Collection<CropsBlock> getAllCropsBlock() {
		return new ArrayList<CropsBlock>(this.mapCropsBlock.values());
	}
	
	protected final Collection<CropsBlock> getAllCropsBlock(boolean clone) {
		final Collection<CropsBlock> allCropsBlock = this.mapCropsBlock.values();
		
		return clone ? new ArrayList<CropsBlock>(allCropsBlock) : allCropsBlock;
	}
	
	@Override
	public final CropsBlock getCropsBlock(Block block) {
		return block != null ? this.mapCropsBlock.get(block) : null;
	}
	
	protected final boolean register(CropsBlock cropsBlock) {
		if (cropsBlock != null) {
			final Block block = cropsBlock.getBlock();
			
			if (!isSet(block)) {
				
				this.mapCropsBlock.put(block, cropsBlock);
				
				addDataEntry(cropsBlock, DatabaseMarkType.SAVE);
				return true;
			}
		}
		
		return false;
	}
	
	protected final boolean unregister(CropsBlock cropsBlock) {
		if (cropsBlock != null && getAllCropsBlock(false).contains(cropsBlock)) {
			final Block block = cropsBlock.getBlock();
			
			this.mapCropsBlock.remove(block);
			
			addDataEntry(block, DatabaseMarkType.REMOVE);
			return true;
		} else {
			return false;
		}
	}
	
	public final List<Entry<Object, DatabaseMarkType>> getListDataEntry() {
		return new ArrayList<Entry<Object, DatabaseMarkType>>(this.listDataEntry);
	}
	
	public final boolean addDataEntry(Object object, DatabaseMarkType type) {
		if (object != null && type != null) {
			final boolean matchSave = type.equals(DatabaseMarkType.SAVE) && object instanceof CropsBlock;
			final boolean matchRemove = type.equals(DatabaseMarkType.REMOVE) && object instanceof Block;
			final boolean matchLoad = type.equals(DatabaseMarkType.LOAD) && object instanceof Block;
			
			if (matchSave || matchRemove || matchLoad) {
				final SimpleEntry<Object, DatabaseMarkType> entry = new SimpleEntry<Object, DatabaseMarkType>(object, type);
				
				this.listDataEntry.add(entry);
				
				return true;
			}
		}
		
		return false;
	}
	
	public final boolean isListDataEntryEmpty() {
		return this.listDataEntry.isEmpty();
	}
	
	protected final void clearListDataEntry() {
		this.listDataEntry.clear();
	}
	
	protected final void clearCache() {
		this.mapCropsBlock.clear();
	}
	
	protected final void loadCacheFromDatabase() {
		new BukkitRunnable() {
			
			@Override
			public void run() {
				try {
					final List<String> allId = cropsBlockDatabase.getAllId();
					
					for (String id : allId) {
						if (SerializeUtil.matchBlockServer(id)) {
							final Block block = SerializeUtil.deserializeBlock(id);
							
							addDataEntry(block, DatabaseMarkType.LOAD);
						}
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}.runTaskAsynchronously(plugin);
	}
}
