package com.praya.herbalism.crops;

import java.sql.SQLException;
import java.util.List;

import org.bukkit.block.Block;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.handler.HandlerDatabase;
import com.praya.herbalism.util.SerializeUtil;

public abstract class CropsBlockDatabase extends HandlerDatabase {

	protected CropsBlockDatabase(Herbalism plugin) {
		super(plugin);
	}
	
	protected abstract List<String> getAllId() throws SQLException;
	protected abstract List<String> getAllData() throws SQLException;
	protected abstract String getData(String id) throws SQLException;
	protected abstract boolean isExists(String id) throws SQLException;
	protected abstract CropsBlock getCropsBlock(String id) throws SQLException;
	protected abstract boolean saveData(CropsBlock cropsBlock);
	protected abstract boolean deleteData(String id);
	
	public final CropsBlock getCropsBlock(Block block) throws SQLException {
		return block != null ? getCropsBlock(SerializeUtil.serializeBlock(block)) : null;
	}
	
	public final boolean deleteData(CropsBlock cropsBlock) {
		return cropsBlock != null ? deleteData(cropsBlock.getBlock()) : false;
	}
	
	public final boolean deleteData(Block block) {
		return block != null ? deleteData(SerializeUtil.serializeBlock(block)) : false;
	}
}
