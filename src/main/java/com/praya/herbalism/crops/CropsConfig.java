package com.praya.herbalism.crops;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

import com.praya.agarthalib.utility.ConfigUtil;
import com.praya.agarthalib.utility.FileUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.handler.HandlerConfig;
import com.praya.herbalism.item.ItemMemory;

public final class CropsConfig extends HandlerConfig {

	private static final String PATH_FILE = "Configuration/crops.yml";
	
	protected final HashMap<String, CropsProperties> mapCropsProperties = new HashMap<String, CropsProperties>();
	
	protected CropsConfig(Herbalism plugin) {
		super(plugin);
		
		setup();
	}
	
	@Override
	public final void setup() {
		reset();
		loadConfig();
	}
	
	private final void reset() {
		this.mapCropsProperties.clear();
	}
	
	private final void loadConfig() {
		final ItemMemory itemMemory = ItemMemory.getInstance();
		final File file = FileUtil.getFile(plugin, PATH_FILE);		
		
		if (!file.exists()) {
			FileUtil.saveResource(plugin, PATH_FILE);
		}
		
		final FileConfiguration config = FileUtil.getFileConfiguration(file);
		
		for (String key : config.getKeys(false)) {
			final ConfigurationSection cropsDataSection = config.getConfigurationSection(key);
			final CropsRequirement requirement = new CropsRequirement();
			final CropsDrop drops = new CropsDrop();
			
			String name = key;
			String category = "Default";
			CropsPlant plant = CropsPlant.CROPS;
			ItemStack icon = null;
			boolean sustainable = false;
			int duration = 3600;
			
			for (String cropsData : cropsDataSection.getKeys(false)) {
				if (cropsData.equalsIgnoreCase("Name")) {
					name = cropsDataSection.getString(cropsData);
				} else if (cropsData.equalsIgnoreCase("Category")) {
					category = cropsDataSection.getString(cropsData);
				} else if (cropsData.equalsIgnoreCase("Duration")) {
					duration = cropsDataSection.getInt(cropsData);
				} else if (cropsData.equalsIgnoreCase("Sustainable")) {
					sustainable = cropsDataSection.getBoolean(cropsData);
				} else if (cropsData.equalsIgnoreCase("Plant")) {
					final String textPlant = cropsDataSection.getString(cropsData);
					
					plant = CropsPlant.getCropsPlant(textPlant);
				} else if (cropsData.equalsIgnoreCase("Icon")) {
					final ConfigurationSection iconDataSection = cropsDataSection.getConfigurationSection(cropsData);
					
					icon = ConfigUtil.getItemStack(iconDataSection);
				} else if (cropsData.equalsIgnoreCase("Requirement")) {
					final ConfigurationSection requirementDataSection = cropsDataSection.getConfigurationSection(cropsData);
					
					for (String requirementData : requirementDataSection.getKeys(false)) {
						if (requirementData.equalsIgnoreCase("Permission")) {
							final String permission = requirementDataSection.getString(requirementData);
							
							requirement.setPermission(permission);
						} else if (requirementData.equalsIgnoreCase("Level")) {
							final int level = requirementDataSection.getInt(requirementData);
							
							requirement.setLevel(level);;
						}
					}
				} else if (cropsData.equalsIgnoreCase("Drop") || cropsData.equalsIgnoreCase("Drops")) {
					final ConfigurationSection dropDataSection = cropsDataSection.getConfigurationSection(cropsData);
					
					double exp = 0;
					
					for (String dropData : dropDataSection.getKeys(false)) {
						if (dropData.equalsIgnoreCase("Exp") || cropsData.equalsIgnoreCase("Experience")) {
							exp = dropDataSection.getDouble(dropData);
						} else if (dropData.equalsIgnoreCase("Item")) {
							final ConfigurationSection dropItemSection = dropDataSection.getConfigurationSection(dropData);
							final List<CropsDropItem> listDropItem = new ArrayList<CropsDropItem>();
							
							for (String dropItem : dropItemSection.getKeys(false)) {
								if (itemMemory.isExists(dropItem)) {
									final ConfigurationSection dropItemDataSection = dropItemSection.getConfigurationSection(dropItem);
									
									double chance = 100;
									int min = 1;
									int max = 1;
									
									for (String dropItemData : dropItemDataSection.getKeys(false)) {
										if (dropItemData.equalsIgnoreCase("Chance")) {
											chance = MathUtil.limitDouble(dropItemDataSection.getDouble(dropItemData), 0, 100);
										} else if (dropItemData.equalsIgnoreCase("Min")) {
											min = dropItemDataSection.getInt(dropItemData);
										} else if (dropItemData.equalsIgnoreCase("Max")) {
											max = dropItemDataSection.getInt(dropItemData);
										}
									}
									
									min = Math.max(0, min);
									max = Math.max(min, max);
									
									final CropsDropItem cropsDrop = new CropsDropItem(dropItem, chance, min, max);
									
									listDropItem.add(cropsDrop);
								}
							}
							
							drops.setItems(listDropItem);
						}
					}
					
					drops.setExp(exp);
				}
			}
			
			if (name != null && category != null) {
				final CropsProperties cropsProperties = new CropsProperties(key, name, category, sustainable, requirement, drops, plant, icon, duration);
				
				this.mapCropsProperties.put(key, cropsProperties);
			}
		}
	}
}
