package com.praya.herbalism.crops;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.inventory.ItemStack;

import com.praya.agarthalib.utility.MathUtil;
import com.praya.herbalism.item.ItemProperties;

public class CropsDrop {

	private double exp;
	private List<CropsDropItem> items;
	
	public CropsDrop() {
		this.exp = 0;
		this.items = new ArrayList<>();
	}
	
	public final double getExp() {
		return this.exp;
	}
	
	public final List<CropsDropItem> getItems() {
		return this.items;
	}
	
	public final void setExp(double exp) {
		this.exp = Math.max(0, exp);
	}
	
	public final void setItems(List<CropsDropItem> drops) {
		if (drops != null) {
			this.items = drops;
		} else {
			this.items.clear();
		}
	}
	
	public final List<ItemStack> generateItems() {
		final List<ItemStack> items = new ArrayList<ItemStack>();
		
		for (CropsDropItem cropsDropItem : getItems()) {
			final double chance = cropsDropItem.getChance();
			
			if (MathUtil.chanceOf(chance)) {
				final int min = cropsDropItem.getMin();
				final int max = cropsDropItem.getMax();
				final int amount = min + MathUtil.random(max - min);
				
				if (amount > 0) {
					final ItemProperties itemProperties = cropsDropItem.getItemProperties();
					
					if (itemProperties != null) {
						final ItemStack item = itemProperties.getItem().clone();
						
						item.setAmount(amount);
						items.add(item);
					}
				}
			}
		}
		
		return items;
	}
}
