package com.praya.herbalism.crops;

import java.sql.SQLException;
import java.util.List;
import java.util.Map.Entry;

import org.bukkit.block.Block;

import com.praya.agarthalib.database.DatabaseMarkType;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.HerbalismConfig;
import com.praya.herbalism.handler.HandlerTask;
import com.praya.herbalism.util.SerializeUtil;

public final class CropsBlockDatabaseTask extends HandlerTask implements Runnable {

	long tick = 0;
	
	protected CropsBlockDatabaseTask(Herbalism plugin) {
		super(plugin);
	}

	@Override
	public void run() {
		final CropsBlockMemory cropsBlockMemory = CropsBlockMemory.getInstance();
		final CropsBlockDatabase cropsBlockDatabase = cropsBlockMemory.getDatabaseCropsBlock();
		final HerbalismConfig mainConfig = plugin.getMainConfig();
		final long period = mainConfig.getDatabasePeriodSave() * 20;
		
		if (tick % period == 0) {
			for (CropsBlock cropsBlock : cropsBlockMemory.getAllCropsBlock()) {
				cropsBlockMemory.addDataEntry(cropsBlock, DatabaseMarkType.SAVE);
			}
		}
		
		if (!cropsBlockMemory.isListDataEntryEmpty()) {
			final List<Entry<Object, DatabaseMarkType>> listDataEntry = cropsBlockMemory.getListDataEntry();
			
			cropsBlockMemory.clearListDataEntry();
			
			for (Entry<Object, DatabaseMarkType> entry : listDataEntry) {
				final Object object = entry.getKey();
				final DatabaseMarkType mark = entry.getValue();
				
				if (object instanceof Block) {
					final Block block = (Block) object;
					final String id = SerializeUtil.serializeBlock(block);
					
					if (mark.equals(DatabaseMarkType.LOAD)) {
						try {
							final String data = cropsBlockDatabase.getData(id);
							final CropsBlock cropsBlock = CropsBlock.deserializeSilent(data);
							
							if (cropsBlock != null) {
								cropsBlockMemory.mapCropsBlock.put(block, cropsBlock);
							}
						} catch (SQLException e) {
							e.printStackTrace();
						}
					} else if (mark.equals(DatabaseMarkType.REMOVE)) {
						cropsBlockDatabase.deleteData(id);
						cropsBlockMemory.mapCropsBlock.remove(block);
					}
				} else if (object instanceof CropsBlock) {
					final CropsBlock cropsBlock = (CropsBlock) object;
					
					if (mark.equals(DatabaseMarkType.SAVE)) {
						cropsBlockDatabase.saveData(cropsBlock);
					}
				}
			}
		}
		
		tick++;
	}
}
