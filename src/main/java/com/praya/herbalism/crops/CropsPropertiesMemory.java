package com.praya.herbalism.crops;

import java.util.ArrayList;
import java.util.Collection;

import org.bukkit.plugin.java.JavaPlugin;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.crops.CropsConfig;
import com.praya.herbalism.crops.CropsProperties;
import com.praya.herbalism.manager.game.CropsPropertiesManager;

public final class CropsPropertiesMemory extends CropsPropertiesManager {
	
	private final CropsConfig cropsConfig;

	private CropsPropertiesMemory(Herbalism plugin) {
		super(plugin);
		
		this.cropsConfig = new CropsConfig(plugin);
	}
	
	private static class CropsPropertiesMemorySingleton {
		private static final CropsPropertiesMemory instance;
		
		static {
			final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
			
			instance = new CropsPropertiesMemory(plugin);
		}
	}
	
	public static final CropsPropertiesMemory getInstance() {
		return CropsPropertiesMemorySingleton.instance;
	}
	
	protected final CropsConfig getCropsConfig() {
		return this.cropsConfig;
	}
	
	@Override
	public final Collection<String> getCropsIds() {
		return getCropsIds(true);
	}
	
	protected final Collection<String> getCropsIds(boolean clone) {
		final Collection<String> cropsIds = getCropsConfig().mapCropsProperties.keySet();
		
		return clone ? new ArrayList<String>(cropsIds) : cropsIds;
	}
	
	@Override
	public final Collection<CropsProperties> getAllCropsProperties() {
		return new ArrayList<CropsProperties>(getCropsConfig().mapCropsProperties.values());
	}
	
	protected final Collection<CropsProperties> getAllCropsProperties(boolean clone) {
		final Collection<CropsProperties> allCropsProperties = getCropsConfig().mapCropsProperties.values();
		
		return clone ? new ArrayList<CropsProperties>(allCropsProperties) : allCropsProperties;
	}
	
	@Override
	public final CropsProperties getCropsProperties(String crops) {
		if (crops != null) {
			for (String key : getCropsIds(false)) {
				if (key.equalsIgnoreCase(crops)) {
					return getCropsConfig().mapCropsProperties.get(key);
				}
			}
		}
		
		return null;
	}
}
