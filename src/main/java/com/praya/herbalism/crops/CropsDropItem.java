package com.praya.herbalism.crops;

import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.MathUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.item.ItemProperties;
import com.praya.herbalism.manager.game.GameManager;
import com.praya.herbalism.manager.game.ItemManager;

public class CropsDropItem {

	private final String id;
	private final double chance;
	private final int min;
	private final int max;
	
	public CropsDropItem(String id, double chance, int min, int max) {
		if (id == null) {
			throw new IllegalArgumentException();
		} else {
			this.id = id;
			this.chance = chance;
			this.min = MathUtil.limitInteger(min, 0, max);
			this.max = max;
		}
	}
	
	public final String getId() {
		return this.id;
	}
	
	public final double getChance() {
		return this.chance;
	}
	
	public final int getMin() {
		return this.min;
	}
	
	public final int getMax() {
		return this.max;
	}
	
	public final ItemProperties getItemProperties() {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final GameManager gameManager = plugin.getGameManager();
		final ItemManager itemManager = gameManager.getItemManager();
		final ItemProperties itemProperties = itemManager.getItemProperties(getId());
		
		return itemProperties;
	}
}
