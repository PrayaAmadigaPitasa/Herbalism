package com.praya.herbalism.crops;

import org.bukkit.Material;

import core.praya.agarthalib.enums.branch.MaterialEnum;

public enum CropsPlant {

	CROPS(MaterialEnum.CROPS, MaterialEnum.CROPS, 1.5),
	COCOA(MaterialEnum.COCOA, MaterialEnum.COCOA, 1.5),
	CARROT(MaterialEnum.CARROTS, MaterialEnum.CARROTS, 1.5),
	POTATO(MaterialEnum.POTATOES, MaterialEnum.POTATOES, 1.5),
	BEETROOT(MaterialEnum.BEETROOTS, MaterialEnum.BEETROOTS, 1.5),
	MELON(MaterialEnum.MELON_STEM, MaterialEnum.MELON, 1.75),
	PUMPKIN(MaterialEnum.PUMPKIN_STEM, MaterialEnum.PUMPKIN, 1.75),
	NETHER_WART(MaterialEnum.NETHER_WARTS, MaterialEnum.NETHER_WARTS, 1.5);
	
	private final MaterialEnum materialEnumPlant;
	private final MaterialEnum materialEnumHarvest;
	private final double height;
	
	private CropsPlant(MaterialEnum materialEnumPlant, MaterialEnum materialEnumHarvest, double height) {
		final MaterialEnum materialEnumDefault = MaterialEnum.CROPS;
		final Material materialPlant = materialEnumPlant.getMaterial();
		final Material materialHarvest = materialEnumHarvest.getMaterial();
		
		this.materialEnumPlant = materialPlant != null ? materialEnumPlant : materialEnumDefault;
		this.materialEnumHarvest = materialHarvest != null ? materialEnumHarvest : materialEnumDefault;
		this.height = height;
	}
	
	public final MaterialEnum getMaterialEnumPlant() {
		return this.materialEnumPlant;
	}
	
	public final MaterialEnum getMaterialEnumHarvest() {
		return this.materialEnumHarvest;
	}
	
	public final Material getMaterialPlant() {
		return getMaterialEnumPlant().getMaterial();
	}
	
	public final Material getMaterialHarvest() {
		return getMaterialEnumHarvest().getMaterial();
	}
	
	public final double getHeight() {
		return this.height;
	}
	
	public final int getMaxData() {
		switch (getMaterialEnumPlant()) {
		case CROPS : return 7;
		case COCOA : return 2;
		case CARROTS : return 7;
		case POTATOES : return 7;
		case BEETROOTS : return 3;
		case MELON_STEM : return 7;
		case PUMPKIN_STEM : return 7;
		case NETHER_WARTS : return 3;
		default : return 0;
		}
	}
	
	public final boolean isSoilPlant() {
		switch (getMaterialEnumPlant()) {
		case CROPS : return true;
		case CARROTS : return true;
		case POTATOES : return true;
		case BEETROOTS : return true;
		case MELON_STEM : return true;
		case PUMPKIN_STEM : return true;
		default : return false;
		}
	}
	
	public final boolean isStemPlant() {
		switch (getMaterialEnumPlant()) {
		case MELON_STEM : return true;
		case PUMPKIN_STEM : return true;
		default : return false;
		}
	}
	
	public static final CropsPlant getCropsPlant(String plant) {
		if (plant != null) {
			for (CropsPlant key : CropsPlant.values()) {
				if (key.toString().equalsIgnoreCase(plant)) {
					return key;
				}
			}
		}
		
		return CropsPlant.CROPS;
	}
}