package com.praya.herbalism.crops;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import org.bukkit.block.Block;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.crops.CropsBlock;
import com.praya.herbalism.crops.CropsHologram;
import com.praya.herbalism.manager.game.CropsHologramManager;

public final class CropsHologramMemory extends CropsHologramManager {

	private final HashMap<Block, CropsHologram> mapCropsHologram = new HashMap<Block, CropsHologram>();
	
	private CropsHologramMemory(Herbalism plugin) {
		super(plugin);
	}
	
	private static class CropsHologramMemorySingleton {
		private static final CropsHologramMemory instance;
		
		static {
			final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
			
			instance = new CropsHologramMemory(plugin);
		}
	}
	
	public static final CropsHologramMemory getInstance() {
		return CropsHologramMemorySingleton.instance;
	}
	
	@Override
	public final Collection<Block> getAllBlocks() {
		return getAllBlocks(true);
	}
	
	protected final Collection<Block> getAllBlocks(boolean clone) {
		final Collection<Block> allBlocks = this.mapCropsHologram.keySet();
		
		return clone ? new ArrayList<Block>(allBlocks) : allBlocks;
	}
	
	@Override
	public final Collection<CropsHologram> getAllCropsHologram() {
		return getAllCropsHologram(true);
	}
	
	protected final Collection<CropsHologram> getAllCropsHologram(boolean clone) {
		final Collection<CropsHologram> allCropsHologram = this.mapCropsHologram.values();
		
		return clone ? new ArrayList<CropsHologram>(allCropsHologram) : allCropsHologram;
	}

	@Override
	public final CropsHologram getCropsHologram(Block block) {
		return this.mapCropsHologram.get(block);
	}
	
	protected final boolean register(CropsHologram cropsHologram) {
		if (cropsHologram != null && !getAllCropsHologram(false).contains(cropsHologram)) {
			final CropsBlock cropsBlock = cropsHologram.getCropsBlock();
			final Block block = cropsBlock.getBlock();
			
			this.mapCropsHologram.put(block, cropsHologram);
			
			return true;
		} else {
			return false;
		}
	}
	
	protected final boolean unregister(CropsHologram cropsHologram) {
		if (cropsHologram != null && getAllCropsHologram(false).contains(cropsHologram)) {
			final CropsBlock cropsBlock = cropsHologram.getCropsBlock();
			final Block block = cropsBlock.getBlock();
			
			this.mapCropsHologram.remove(block);
			
			return true;
		} else {
			return false;
		}
	}
}
