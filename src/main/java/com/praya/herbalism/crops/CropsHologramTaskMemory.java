package com.praya.herbalism.crops;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scheduler.BukkitTask;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.manager.task.TaskCropsHologramManager;

public final class CropsHologramTaskMemory extends TaskCropsHologramManager {

	private BukkitTask taskCropsHologram;
	
	private CropsHologramTaskMemory(Herbalism plugin) {
		super(plugin);
		
		reloadTaskCropsHologram();
	}
		
	private static class CropsHologramTaskMemorySingleton {
		private static final CropsHologramTaskMemory instance;
		
		static {
			final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
			
			instance = new CropsHologramTaskMemory(plugin);
		}
	}
	
	public static final CropsHologramTaskMemory getInstance() {
		return CropsHologramTaskMemorySingleton.instance;
	}
	
	@Override
	public final void reloadTaskCropsHologram() {
		if (this.taskCropsHologram != null) {
			this.taskCropsHologram.cancel();
		}
		
		this.taskCropsHologram = createTaskCropsHologram();
	}
	
	private final BukkitTask createTaskCropsHologram() {
		final BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
		final Runnable runnable = new CropsHologramTask(plugin);
		final int delay = 0;
		final int period = 1;
		final BukkitTask task = scheduler.runTaskTimer(plugin, runnable, delay, period);
		
		return task;
	}
}
