package com.praya.herbalism;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.ability.AbilityMemory;
import com.praya.herbalism.ability.AbilityPropertiesMemory;
import com.praya.herbalism.command.CommandTreeMemory;
import com.praya.herbalism.crops.CropsBlockMemory;
import com.praya.herbalism.crops.CropsHologramMemory;
import com.praya.herbalism.crops.CropsPropertiesMemory;
import com.praya.herbalism.fertilizer.FertilizerMemory;
import com.praya.herbalism.item.ItemMemory;
import com.praya.herbalism.manager.game.AbilityManager;
import com.praya.herbalism.manager.game.AbilityPropertiesManager;
import com.praya.herbalism.manager.game.CommandTreeManager;
import com.praya.herbalism.manager.game.CropsBlockManager;
import com.praya.herbalism.manager.game.CropsHologramManager;
import com.praya.herbalism.manager.game.CropsPropertiesManager;
import com.praya.herbalism.manager.game.FertilizerManager;
import com.praya.herbalism.manager.game.GameManager;
import com.praya.herbalism.manager.game.ItemManager;
import com.praya.herbalism.manager.game.MenuManager;
import com.praya.herbalism.manager.game.TabCompleterTreeManager;
import com.praya.herbalism.menu.MenuMemory;
import com.praya.herbalism.tabcompleter.TabCompleterTreeMemory;

public final class HerbalismGameMemory extends GameManager {
	
	private final AbilityManager abilityManager;
	private final AbilityPropertiesManager abilityPropertiesManager;
	private final ItemManager itemManager;
	private final FertilizerManager fertilizerManager;
	private final CropsPropertiesManager cropsPropertiesManager;
	private final CropsBlockManager cropsBlockManager;
	private final CropsHologramManager cropsHologramManager;
	private final MenuManager menuManager;
	private final CommandTreeManager commandTreeManager;
	private final TabCompleterTreeManager tabCompleterTreeManager;

	protected HerbalismGameMemory(Herbalism plugin) {
		super(plugin);
		
		this.abilityManager = AbilityMemory.getInstance();
		this.abilityPropertiesManager = AbilityPropertiesMemory.getInstance();
		this.itemManager = ItemMemory.getInstance();
		this.fertilizerManager = FertilizerMemory.getInstance();
		this.cropsPropertiesManager = CropsPropertiesMemory.getInstance();
		this.cropsBlockManager = CropsBlockMemory.getInstance();
		this.cropsHologramManager = CropsHologramMemory.getInstance();
		this.menuManager = MenuMemory.getInstance();
		this.commandTreeManager = CommandTreeMemory.getInstance();
		this.tabCompleterTreeManager = TabCompleterTreeMemory.getInstance();
	}
	
	public final AbilityManager getAbilityManager() {
		return this.abilityManager;
	}
	
	public final AbilityPropertiesManager getAbilityPropertiesManager() {
		return this.abilityPropertiesManager;
	}
	
	public final ItemManager getItemManager() {
		return this.itemManager;
	}
	
	public final FertilizerManager getFertilizerManager() {
		return this.fertilizerManager;
	}
	
	public final CropsPropertiesManager getCropsPropertiesManager() {
		return this.cropsPropertiesManager;
	}
	
	public final CropsBlockManager getCropsBlockManager() {
		return this.cropsBlockManager;
	}
	
	public final CropsHologramManager getCropsHologramManager() {
		return this.cropsHologramManager;
	}
	
	public final MenuManager getMenuManager() {
		return this.menuManager;
	}
	
	public final CommandTreeManager getCommandTreeManager() {
		return this.commandTreeManager;
	}
	
	public final TabCompleterTreeManager getTabCompleterTreeManager() {
		return this.tabCompleterTreeManager;
	}
}
