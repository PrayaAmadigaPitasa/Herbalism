package com.praya.herbalism.item;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.MapUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.crops.CropsProperties;
import com.praya.herbalism.crops.CropsRequirement;
import com.praya.herbalism.manager.game.CropsPropertiesManager;
import com.praya.herbalism.manager.game.GameManager;

public class ItemProperties {

	private final String id;
	private final String category;
	private final ItemStack item;
	private final boolean buyable;
	private final boolean sellable;
	private final double price;
	private final HashMap<String, ItemCrops> mapCrops;
	
	public ItemProperties(String id, String category, ItemStack item, boolean buyable, boolean sellable, double price, HashMap<String, ItemCrops> mapCrops) {
		if (id == null) {
			throw new IllegalArgumentException();
		} else {
			this.id = id;
			this.category = category;
			this.item = item;
			this.buyable = buyable;
			this.sellable = sellable;
			this.price = price;
			this.mapCrops = mapCrops;
		}
	}
	
	public final String getId() {
		return this.id;
	}
	
	public final String getCategory() {
		return this.category;
	}
	
	public final ItemStack getItem() {
		return this.item;
	}
	
	public final boolean isBuyable() {
		return this.buyable;
	}
	
	public final boolean isSellable() {
		return this.sellable;
	}
	
	public final double getPrice() {
		return this.price;
	}
	
	public final Collection<String> getCropsIDs() {
		return this.mapCrops.keySet();
	}
	
	public final Collection<ItemCrops> getAllItemCrops() {
		return this.mapCrops.values();
	}
	
	public final ItemCrops getItemCrops(String crops) {
		if (crops != null) {
			for (String key : getCropsIDs()) {
				if (key.equalsIgnoreCase(crops)) {
					return this.mapCrops.get(key);
				}
			}
		}
		
		return null;
	}
	
	public final boolean isItemCropsExists(String crops) {
		return getItemCrops(crops) != null;
	}
	
	public final String generateCrops(Player player) {
		if (player != null) {
			final HashMap<String, Integer> mapPossibility = new HashMap<String, Integer>();
			for (String crops : getAllowedCrops(player)) {
				final ItemCrops itemCrops = getItemCrops(crops);
				
				if (itemCrops != null) {
					final int possibility = itemCrops.getPossibility();
					
					mapPossibility.put(crops, possibility);
				}
			}
			
			return MapUtil.getRandomIdByInteger(mapPossibility);
		} else {
			return null;
		}
	}
	
	public final List<String> getAllowedCrops(Player player) {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final GameManager gameManager = plugin.getGameManager();
		final CropsPropertiesManager cropsManager = gameManager.getCropsPropertiesManager();
		final List<String> allowedCrops = new ArrayList<String>();
		
		if (player != null) {
			for (String crops : getCropsIDs()) {
				final CropsProperties cropsProperties = cropsManager.getCropsProperties(crops);
				
				if (cropsProperties != null) {
					final CropsRequirement cropsRequirement = cropsProperties.getCropsRequirement();
					
					if (cropsRequirement.isAllowed(player)) {
						allowedCrops.add(crops);
					}
				}
			}
		}
		
		return allowedCrops;
	}
	
	public final boolean isAllowed(Player player) {
		return !getAllowedCrops(player).isEmpty();
	}
}
