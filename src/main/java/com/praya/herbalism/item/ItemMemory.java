package com.praya.herbalism.item;

import java.util.ArrayList;
import java.util.Collection;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.item.ItemConfig;
import com.praya.herbalism.item.ItemProperties;
import com.praya.herbalism.manager.game.ItemManager;

public final class ItemMemory extends ItemManager {

	private final ItemConfig itemConfig;
	
	private ItemMemory(Herbalism plugin) {
		super(plugin);
		
		this.itemConfig = new ItemConfig(plugin);
	}
	
	private static class ItemMemorySingleton {
		private static final ItemMemory instance;
		
		static {
			final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
			
			instance = new ItemMemory(plugin);
		}
	}
	
	public static final ItemMemory getInstance() {
		return ItemMemorySingleton.instance;
	}
	
	public final ItemConfig getItemConfig() {
		return this.itemConfig;
	}
	
	@Override
	public final Collection<String> getItemIds() {
		return getItemIds(true);
	}
	
	protected final Collection<String> getItemIds(boolean clone) {
		final Collection<String> itemIds = getItemConfig().mapItemProperties.keySet();
		
		return clone ? new ArrayList<String>(itemIds) : itemIds;
	}
	
	@Override
	public final Collection<ItemProperties> getAllItemProperties() {
		return new ArrayList<ItemProperties>(getItemConfig().mapItemProperties.values());
	}
	
	protected final Collection<ItemProperties> getAllItemProperties(boolean clone) {
		final Collection<ItemProperties> allItemProperties = getItemConfig().mapItemProperties.values();
		
		return clone ? new ArrayList<ItemProperties>(allItemProperties) : allItemProperties;
	}
	
	@Override
	public final ItemProperties getItemProperties(String id) {
		if (id != null) {
			for (String key : getItemIds(false)) {
				if (key.equalsIgnoreCase(id)) {
					return getItemConfig().mapItemProperties.get(key);
				}
			}
		}
		
		return null;
	}
}
