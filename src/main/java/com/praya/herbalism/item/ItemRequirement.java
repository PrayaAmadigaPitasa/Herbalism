package com.praya.herbalism.item;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.SenderUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.manager.player.PlayerHerbalismManager;
import com.praya.herbalism.manager.player.PlayerManager;
import com.praya.herbalism.player.PlayerHerbalism;

public class ItemRequirement {

	private String permission;
	private int level;
	
	public ItemRequirement() {
		this(null, 1);
	}
	
	public ItemRequirement(String permission, int level) {
		setPermission(permission);
		setLevel(level);
	}
	
	public final String getPermission() {
		return this.permission;
	}
	
	public final int getLevel() {
		return this.level;
	}
	
	public final void setPermission(String permission) {
		this.permission = permission;
	}
	
	public final void setLevel(int level) {
		this.level = Math.max(0, level);
	}
	
	public final boolean isAllowed(Player player) {
		final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerHerbalismManager playerHerbalismManager = playerManager.getPlayerHerbalismManager();
		
		if (player != null) {
			final PlayerHerbalism playerHerbalism = playerHerbalismManager.getPlayerHerbalism(player);
			final int playerLevel = playerHerbalism.getLevel();
			final boolean allowedLevel = getLevel() <= playerLevel;
			final boolean allowedPermission = SenderUtil.hasPermission(player, getPermission());
			
			return allowedLevel && allowedPermission;
		} else {
			return false;
		}
	}
}
