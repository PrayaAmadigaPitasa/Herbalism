package com.praya.herbalism.item;

public class ItemCrops {

	private final String crops;
	private final int possibility;
	
	public ItemCrops(String crops, int possibility) {
		this.crops = crops;
		this.possibility = possibility;
	}
	
	public final String getCrops() {
		return this.crops;
	}
	
	public final int getPossibility() {
		return this.possibility;
	}
}
