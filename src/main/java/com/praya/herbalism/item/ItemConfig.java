package com.praya.herbalism.item;

import java.io.File;
import java.util.HashMap;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

import com.praya.agarthalib.utility.ConfigUtil;
import com.praya.agarthalib.utility.FileUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.handler.HandlerConfig;

public final class ItemConfig extends HandlerConfig {

	private static final String PATH_FILE = "Configuration/item.yml";
	
	protected final HashMap<String, ItemProperties> mapItemProperties = new HashMap<String, ItemProperties>();
	
	protected ItemConfig(Herbalism plugin) {
		super(plugin);
		
		setup();
	}
	
	@Override
	public final void setup() {
		reset();
		loadConfig();
	}
	
	private final void reset() {
		this.mapItemProperties.clear();
	}
	
	private final void loadConfig() {
		final File file = FileUtil.getFile(plugin, PATH_FILE);		
		
		if (!file.exists()) {
			FileUtil.saveResource(plugin, PATH_FILE);
		}
		
		final FileConfiguration config = FileUtil.getFileConfiguration(file);
		
		for (String key : config.getKeys(false)) {
			final ConfigurationSection itemDataSection = config.getConfigurationSection(key);
			final ItemStack item = ConfigUtil.getItemStack(itemDataSection);
			final HashMap<String, ItemCrops> mapCrops = new HashMap<String, ItemCrops>();
			
			String category = "Default";
			boolean buyable = false;
			boolean sellable = false;
			double price = 100;
			
			for (String itemData : itemDataSection.getKeys(false)) {
				if (itemData.equalsIgnoreCase("Category")) {
					category = itemDataSection.getString(itemData);
				} else if (itemData.equalsIgnoreCase("Buyable")) {
					buyable = itemDataSection.getBoolean(itemData);
				} else if (itemData.equalsIgnoreCase("Sellable")) {
					sellable = itemDataSection.getBoolean(itemData);
				} else if (itemData.equalsIgnoreCase("Price")) {
					price = itemDataSection.getDouble(itemData);
				} else if (itemData.equalsIgnoreCase("Crops")) {
					final ConfigurationSection cropsSection = itemDataSection.getConfigurationSection(itemData);
					
					for (String crops : cropsSection.getKeys(false)) {
						final ConfigurationSection cropsDataSection = cropsSection.getConfigurationSection(crops);
						
						int possibility = 0;
						
						for (String cropsData : cropsSection.getKeys(false)) {
							if (cropsData.equalsIgnoreCase("Possibility") || cropsData.equalsIgnoreCase("Weight")) {
								possibility = cropsDataSection.getInt(cropsData);
							}
						}
						
						final ItemCrops itemCrops = new ItemCrops(crops, possibility);
						
						mapCrops.put(crops, itemCrops);
					}
				}
			}
			
			if (item != null) {
				final ItemProperties itemProperties = new ItemProperties(key, category, item, buyable, sellable, price, mapCrops);
				
				this.mapItemProperties.put(key, itemProperties);
			}
			
		}
	}
}
