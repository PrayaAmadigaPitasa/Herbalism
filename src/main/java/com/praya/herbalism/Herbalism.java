package com.praya.herbalism;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.builder.face.Agartha;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.ServerEventUtil;
import com.praya.herbalism.listener.custom.ListenerCropsCultivate;
import com.praya.herbalism.listener.custom.ListenerCropsFertilize;
import com.praya.herbalism.listener.custom.ListenerCropsHarvest;
import com.praya.herbalism.listener.custom.ListenerMenuOpen;
import com.praya.herbalism.listener.custom.ListenerPlayerHerbalismExpChange;
import com.praya.herbalism.listener.custom.ListenerPlayerHerbalismLevelChange;
import com.praya.herbalism.listener.main.ListenerBlockBreak;
import com.praya.herbalism.listener.main.ListenerBlockGrow;
import com.praya.herbalism.listener.main.ListenerBlockPlace;
import com.praya.herbalism.listener.main.ListenerPlayerInteract;
import com.praya.herbalism.listener.main.ListenerPlayerJoin;
import com.praya.herbalism.listener.main.ListenerPlayerQuit;
import com.praya.herbalism.manager.game.GameManager;
import com.praya.herbalism.manager.player.PlayerManager;
import com.praya.herbalism.manager.plugin.PluginManager;
import com.praya.herbalism.manager.task.TaskManager;

public class Herbalism extends JavaPlugin implements Agartha {
	
	private final String type = "Premium";
	private final String placeholder = "herbalism";
	
	private HerbalismConfig mainConfig;
	
	private PluginManager pluginManager;
	private PlayerManager playerManager;
	private GameManager gameManager;
	private TaskManager taskManager;
	
	@Override
	public String getPluginName() {
		return this.getName();
	}

	@Override
	public String getPluginType() {
		return this.type;
	}
	
	@Override
	public String getPluginVersion() {
		return getDescription().getVersion();
	}
	
	@Override
	public String getPluginPlaceholder() {
		return this.placeholder;
	}

	@Override
	public String getPluginWebsite() {
		return getPluginManager().getPluginPropertiesManager().getPluginWebsite();
	}

	@Override
	public String getPluginLatest() {
		return getPluginManager().getPluginPropertiesManager().getPluginVersion();
	}
	
	@Override
	public List<String> getPluginDevelopers() {
		return getPluginManager().getPluginPropertiesManager().getPluginDevelopers();
	}
	
	public final HerbalismConfig getMainConfig() {
		return this.mainConfig;
	}
	
	public final PluginManager getPluginManager() {
		return this.pluginManager;
	}
	
	public final PlayerManager getPlayerManager() {
		return this.playerManager;
	}
	
	public final GameManager getGameManager() {
		return this.gameManager;
	}
	
	public final TaskManager getTaskManager() {
		return this.taskManager;
	}
	
	@Override
	public void onEnable() {
		registerConfig();
		registerManager();
		registerListener();
		registerPlaceholder();
	}
	
	private final void registerConfig() {
		this.mainConfig = new HerbalismConfig(this);
	}
	
	private final void registerManager() {
		this.pluginManager = new HerbalismPluginMemory(this);
		this.playerManager = new HerbalismPlayerMemory(this);
		this.gameManager = new HerbalismGameMemory(this);
		this.taskManager = new HerbalismTaskMemory(this);
	}
	
	private final void registerPlaceholder() {
		getPluginManager().getPlaceholderManager().registerAll();
	}
	
	private final void registerListener() {
		final Listener listenerBlockBreak = new ListenerBlockBreak(this);
		final Listener listenerBlockGrow = new ListenerBlockGrow(this);
		final Listener listenerBlockPlace = new ListenerBlockPlace(this);
		final Listener listenerPlayerInteract = new ListenerPlayerInteract(this);
		final Listener listenerPlayerJoin = new ListenerPlayerJoin(this);
		final Listener listenerPlayerQuit = new ListenerPlayerQuit(this);
		final Listener listenerCropsCultivate = new ListenerCropsCultivate(this);
		final Listener listenerCropsFertilize = new ListenerCropsFertilize(this);
		final Listener listenerCropsHarvest = new ListenerCropsHarvest(this);
		final Listener listenerMenuOpen = new ListenerMenuOpen(this);
		final Listener listenerPlayerHerbalismLevelChange = new ListenerPlayerHerbalismLevelChange(this);
		final Listener listenerPlayerHerbalismExpChange = new ListenerPlayerHerbalismExpChange(this);
		
		ServerEventUtil.registerEvent(this, listenerBlockBreak);
		ServerEventUtil.registerEvent(this, listenerBlockGrow);
		ServerEventUtil.registerEvent(this, listenerBlockPlace);
		ServerEventUtil.registerEvent(this, listenerPlayerInteract);
		ServerEventUtil.registerEvent(this, listenerPlayerJoin);
		ServerEventUtil.registerEvent(this, listenerPlayerQuit);
		ServerEventUtil.registerEvent(this, listenerCropsCultivate);
		ServerEventUtil.registerEvent(this, listenerCropsFertilize);
		ServerEventUtil.registerEvent(this, listenerCropsHarvest);
		ServerEventUtil.registerEvent(this, listenerMenuOpen);
		ServerEventUtil.registerEvent(this, listenerPlayerHerbalismLevelChange);
		ServerEventUtil.registerEvent(this, listenerPlayerHerbalismExpChange);
	}
	
	@Override
	public final void onDisable() {
		final String informationID = "Herbalism Information";
		final BukkitScheduler scheduler = Bukkit.getScheduler();
		
		scheduler.cancelTasks(this);
		
		for (Player player : PlayerUtil.getOnlinePlayers()) {
			Bridge.getBridgeMessage().removeBossBar(player, informationID);
		}
	}
}
