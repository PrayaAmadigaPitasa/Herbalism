package com.praya.herbalism.menu.gui;

import java.util.HashMap;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import api.praya.agarthalib.builder.support.SupportVault;
import api.praya.agarthalib.main.AgarthaLibAPI;
import api.praya.agarthalib.manager.plugin.SupportManagerAPI;
import core.praya.agarthalib.builder.menu.Menu;
import core.praya.agarthalib.builder.menu.MenuExecutor;
import core.praya.agarthalib.builder.menu.MenuSlotAction.ActionType;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import com.praya.agarthalib.utility.InventoryUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.HerbalismConfig;
import com.praya.herbalism.fertilizer.FertilizerProperties;
import com.praya.herbalism.handler.HandlerMenuExecutor;
import com.praya.herbalism.item.ItemProperties;
import com.praya.herbalism.language.Language;
import com.praya.herbalism.manager.game.FertilizerManager;
import com.praya.herbalism.manager.game.GameManager;
import com.praya.herbalism.manager.game.ItemManager;
import com.praya.herbalism.manager.game.MenuManager;

public final class MenuHerbalismExecutor extends HandlerMenuExecutor implements MenuExecutor {

	protected MenuHerbalismExecutor(Herbalism plugin) {
		super(plugin);
	}

	@Override
	public void onClick(Player player, Menu menu, ActionType actionType, String... args) {
		final AgarthaLibAPI agarthaLibAPI = AgarthaLibAPI.getInstance();
		final SupportManagerAPI supportManagerAPI = agarthaLibAPI.getPluginManagerAPI().getSupportManager();
		final GameManager gameManager = plugin.getGameManager();
		final MenuManager menuManager = gameManager.getMenuManager();
		final ItemManager itemManager = gameManager.getItemManager();
		final FertilizerManager fertilizerManager = gameManager.getFertilizerManager();
		final HerbalismConfig mainConfig = plugin.getMainConfig();
		final SupportVault supportVault = supportManagerAPI.getSupportVault();
		
		if (args.length > 0) {
			final String label = args[0];
			
			if (label.equalsIgnoreCase("Herbalism")) {
				if (args.length > 1) {
					final String key = args[1];
					
					if (key.equalsIgnoreCase("Menu")) {
						if (args.length > 2) {
							final String menuID = args[2];
							final MenuHerbalism menuHerbalism = menuManager.getMenuHerbalism();
							final int page;
							
							if (args.length > 3) {
								final String textPage = args[3];
								
								if (MathUtil.isNumber(textPage)) {
									page = MathUtil.parseInteger(textPage);
								} else {
									page = 1;
								}
							} else {
								page = 1;
							}
							
							if (menuID.equalsIgnoreCase("Home")) {
								menuHerbalism.openMenuHome(player);
								return;
							} else if (menuID.equalsIgnoreCase("Player")) {
								menuHerbalism.openMenuPlayer(player);
								return;
							} else if (menuID.equalsIgnoreCase("Progress")) {
								menuHerbalism.openMenuProgress(player);
								return;
							} else if (menuID.equalsIgnoreCase("Progress_Ability")) {
								menuHerbalism.openMenuProgressAbility(player, page);
								return;
							} else if (menuID.equalsIgnoreCase("Progress_Crops")) {
								menuHerbalism.openMenuProgressCrops(player, page);
								return;
							} else if (menuID.equalsIgnoreCase("Progress_Fertilizer")) {
								menuHerbalism.openMenuProgressFertilizer(player, page);
								return;
							} else if (menuID.equalsIgnoreCase("Shop")) {
								menuHerbalism.openMenuShop(player);
								return;
							} else if (menuID.equalsIgnoreCase("Shop_Harvest_Buy")) {
								menuHerbalism.openMenuBuyHarvest(player, page);
								return;
							} else if (menuID.equalsIgnoreCase("Shop_Harvest_Sell")) {
								menuHerbalism.openMenuSellHarvest(player, page);
								return;
							} else if (menuID.equalsIgnoreCase("Shop_Fertilizer_Buy")) {
								menuHerbalism.openMenuBuyFertilizer(player, page);
								return;
							}
						} 
					} else if (key.equalsIgnoreCase("Action")) {
						if (args.length > 2) {
							final String action = args[2];
							
							if (action.equalsIgnoreCase("Harvest")) {
								if (args.length > 3) {
									final String subAction = args[3];
									
									if (subAction.equalsIgnoreCase("Buy")) {
										if (supportVault == null) {
											final MessageBuild messageBuild = Language.PLUGIN_NOT_INSTALLED.getMessage(player);

											messageBuild.sendMessage(player, "plugin", "Vault");
											SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
											return;
										} else if (args.length < 5) {
											final MessageBuild messageBuild = Language.ARGUMENT_NOT_COMPLETE.getMessage(player);
											
											messageBuild.sendMessage(player);
											SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
											return;
										} else {
											final String itemID = args[4];
											final ItemProperties itemProperties = itemManager.getItemProperties(itemID);
											
											if (itemProperties == null) {
												final MessageBuild messageBuild = Language.ITEM_NOT_EXIST.getMessage(player);
												
												messageBuild.sendMessage(player, "nameid", itemID);
												SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
												return;
											} else {
												final PlayerInventory inventory = player.getInventory();
												final ItemStack item = itemProperties.getItem().clone();
												final double money = supportVault.getBalance(player);
												final double price = itemProperties.getPrice();
												
												int amount = 1;
												
												if (args.length > 5) {
													final String textAmount = args[5];
													
													if (MathUtil.isNumber(textAmount)) {
														amount = MathUtil.parseInteger(textAmount);
														amount = MathUtil.limitInteger(amount, 1, 64);
													} else {
														final MessageBuild messageBuild = Language.ARGUMENT_INVALID_VALUE.getMessage(player);
														
														messageBuild.sendMessage(player);
														SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
														return;
													}
												}
												
												final double totalPrice = amount*price;
												
												if (money < totalPrice) {
													final MessageBuild messageBuild = Language.ARGUMENT_LACK_MONEY.getMessage(player);
													
													messageBuild.sendMessage(player);
													SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
													return;
												} else {
													final String symbolCurrency = mainConfig.getUtilityCurrency();
													final HashMap<String, String> map = new HashMap<String, String>();
													final MessageBuild messageBuild = Language.COMMAND_HERBALISM_HARVEST_BUY_SUCCESS.getMessage(player);
													
													map.put("amount", String.valueOf(amount));
													map.put("item", itemID);
													map.put("money", String.valueOf(MathUtil.roundNumber(totalPrice, 2)));
													map.put("symbol_currency", symbolCurrency);
													
													messageBuild.sendMessage(player, map);
													supportVault.remBalance(player, totalPrice);
													InventoryUtil.addItem(inventory, item, amount);
													SenderUtil.playSound(player, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
													return;
												}
											}
										}
									} else if (subAction.equalsIgnoreCase("Sell")) {
										if (supportVault == null) {
											final MessageBuild messageBuild = Language.PLUGIN_NOT_INSTALLED.getMessage(player);

											messageBuild.sendMessage(player, "plugin", "Vault");
											SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
											return;
										} else if (args.length < 5) {
											final MessageBuild messageBuild = Language.ARGUMENT_NOT_COMPLETE.getMessage(player);
											
											messageBuild.sendMessage(player);
											SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
											return;
										} else {
											final String itemID = args[4];
											final ItemProperties itemProperties = itemManager.getItemProperties(itemID);
											
											if (itemProperties == null) {
												final MessageBuild messageBuild = Language.ITEM_NOT_EXIST.getMessage(player);
												
												messageBuild.sendMessage(player, "nameid", itemID);
												SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
												return;
											} else {
												final PlayerInventory inventory = player.getInventory();
												final ItemStack item = itemProperties.getItem().clone();
												final double price = itemProperties.getPrice();
												
												int amount = 1;
												
												if (args.length > 5) {
													final String textAmount = args[5];
													
													if (MathUtil.isNumber(textAmount)) {
														amount = MathUtil.parseInteger(textAmount);
														amount = MathUtil.limitInteger(amount, 1, 64);
													} else {
														final MessageBuild messageBuild = Language.ARGUMENT_INVALID_VALUE.getMessage(player);
														
														messageBuild.sendMessage(player);
														SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
														return;
													}
												}
												
												final int totalItem = InventoryUtil.getCountItem(inventory, item);
												
												amount = MathUtil.limitInteger(amount, 0, totalItem);
												
												if (totalItem == 0) {
													final MessageBuild messageBuild = Language.ARGUMENT_LACK_ITEM.getMessage(player);
													
													messageBuild.sendMessage(player);
													SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
													return;
												} else {
													final String symbolCurrency = mainConfig.getUtilityCurrency();
													final double totalPrice = amount*price;
													final MessageBuild messageBuild = Language.COMMAND_HERBALISM_HARVEST_SELL_SUCCESS.getMessage(player);
													final HashMap<String, String> map = new HashMap<String, String>();
													
													map.put("amount", String.valueOf(amount));
													map.put("item", itemID);
													map.put("money", String.valueOf(MathUtil.roundNumber(totalPrice, 2)));
													map.put("symbol_currency", symbolCurrency);
													
													messageBuild.sendMessage(player, map);
													supportVault.addBalance(player, totalPrice);
													InventoryUtil.removeItem(inventory, item, amount);
													SenderUtil.playSound(player, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
													return;
												}
											}
										}
									}
								} 
							} else if (action.equalsIgnoreCase("Fertilizer")) {
								if (args.length > 3) {
									final String subAction = args[3];
									
									if (subAction.equalsIgnoreCase("Buy")) {
										if (supportVault == null) {
											final MessageBuild messageBuild = Language.PLUGIN_NOT_INSTALLED.getMessage(player);

											messageBuild.sendMessage(player, "plugin", "Vault");
											SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
											return;
										} else if (args.length < 5) {
											final MessageBuild messageBuild = Language.ARGUMENT_NOT_COMPLETE.getMessage(player);
											
											messageBuild.sendMessage(player);
											SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
											return;
										} else {
											final String fertilizerID = args[4];
											final FertilizerProperties fertilizerProperties = fertilizerManager.getFertilizer(fertilizerID);
											
											if (fertilizerProperties == null) {
												final MessageBuild messageBuild = Language.ITEM_NOT_EXIST.getMessage(player);
												
												messageBuild.sendMessage(player, "nameid", fertilizerID);
												SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
												return;
											} else {
												final PlayerInventory inventory = player.getInventory();
												final ItemStack item = fertilizerProperties.getItem().clone();
												final double money = supportVault.getBalance(player);
												final double price = fertilizerProperties.getPrice();
												
												int amount = 1;
												
												if (args.length > 5) {
													final String textAmount = args[5];
													
													if (MathUtil.isNumber(textAmount)) {
														amount = MathUtil.parseInteger(textAmount);
														amount = MathUtil.limitInteger(amount, 1, 64);
													} else {
														final MessageBuild messageBuild = Language.ARGUMENT_INVALID_VALUE.getMessage(player);
														
														messageBuild.sendMessage(player);
														SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
														return;
													}
												}
												
												final double totalPrice = amount*price;
												
												if (money < totalPrice) {
													final MessageBuild messageBuild = Language.ARGUMENT_LACK_MONEY.getMessage(player);
													
													messageBuild.sendMessage(player);
													SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
													return;
												} else {
													final String symbolCurrency = mainConfig.getUtilityCurrency();
													final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
													final MessageBuild messageBuild = Language.COMMAND_HERBALISM_HARVEST_BUY_SUCCESS.getMessage(player);
													
													mapPlaceholder.put("amount", String.valueOf(amount));
													mapPlaceholder.put("item", fertilizerID);
													mapPlaceholder.put("money", String.valueOf(MathUtil.roundNumber(totalPrice, 2)));
													mapPlaceholder.put("symbol_currency", symbolCurrency);
													
													messageBuild.sendMessage(player, mapPlaceholder);
													supportVault.remBalance(player, totalPrice);
													InventoryUtil.addItem(inventory, item, amount);
													SenderUtil.playSound(player, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
													return;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
