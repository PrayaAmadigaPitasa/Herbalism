package com.praya.herbalism.menu.gui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import core.praya.agarthalib.builder.menu.Menu;
import core.praya.agarthalib.builder.menu.MenuExecutor;
import core.praya.agarthalib.builder.menu.MenuGUI;
import core.praya.agarthalib.builder.menu.MenuSlot;
import core.praya.agarthalib.builder.menu.MenuSlotAction.ActionCategory;
import core.praya.agarthalib.builder.menu.MenuSlotAction.ActionType;
import core.praya.agarthalib.builder.text.Text;
import core.praya.agarthalib.builder.text.TextLine;
import core.praya.agarthalib.enums.branch.FlagEnum;
import core.praya.agarthalib.enums.branch.MaterialEnum;
import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.SortUtil;
import com.praya.agarthalib.utility.TextUtil;
import com.praya.herbalism.Herbalism;
import com.praya.herbalism.HerbalismConfig;
import com.praya.herbalism.ability.Ability;
import com.praya.herbalism.crops.CropsDrop;
import com.praya.herbalism.crops.CropsDropItem;
import com.praya.herbalism.crops.CropsProperties;
import com.praya.herbalism.crops.CropsRequirement;
import com.praya.herbalism.fertilizer.FertilizerEffect;
import com.praya.herbalism.fertilizer.FertilizerProperties;
import com.praya.herbalism.handler.HandlerMenu;
import com.praya.herbalism.item.ItemProperties;
import com.praya.herbalism.item.ItemRequirement;
import com.praya.herbalism.language.Language;
import com.praya.herbalism.manager.game.AbilityManager;
import com.praya.herbalism.manager.game.CropsPropertiesManager;
import com.praya.herbalism.manager.game.FertilizerManager;
import com.praya.herbalism.manager.game.GameManager;
import com.praya.herbalism.manager.game.ItemManager;
import com.praya.herbalism.manager.player.PlayerHerbalismManager;
import com.praya.herbalism.manager.player.PlayerManager;
import com.praya.herbalism.manager.plugin.PlaceholderManager;
import com.praya.herbalism.manager.plugin.PluginManager;
import com.praya.herbalism.player.PlayerHerbalism;

public final class MenuHerbalism extends HandlerMenu {
	
	private MenuHerbalism(Herbalism plugin, MenuExecutor menuExecutor) {
		super(plugin, menuExecutor);
	}
	
	private static class MenuHerbalismSingleton {
		private static final MenuHerbalism INSTANCE;
		
		static {
			final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
			final MenuExecutor menuExecutor = new MenuHerbalismExecutor(plugin);
			
			INSTANCE = new MenuHerbalism(plugin, menuExecutor);
		}
	}
	
	public static final MenuHerbalism getInstance() {
		return MenuHerbalismSingleton.INSTANCE;
	}
	
	public final void openMenuHome(Player player) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final PlaceholderManager placeholderManager = pluginManager.getPlaceholderManager();
		final MenuExecutor executor = getMenuExecutor();
		final String playerName = player.getName();
		
		final String id = "Herbalism Home";
		final String textTitle = Language.MENU_PAGE_TITLE_HOME.getText(player);
		final int row = 3;
		final int size = row * 9;
		final Text title = new TextLine(textTitle);
		final String prefix = placeholderManager.getPlaceholder("prefix");
		final String headerPlayer = Language.MENU_ITEM_HEADER_HOME_PLAYER.getText(player);
		final String headerShop = Language.MENU_ITEM_HEADER_HOME_SHOP.getText(player);
		final int[] arraySlotPaneWhite = new int[] {0, 8, 9, 17, 18, 26};
		final MenuSlot menuSlotPlayer = new MenuSlot(11);
		final MenuSlot menuSlotShop = new MenuSlot(15);
		final Map<Integer, MenuSlot> mapSlot = new ConcurrentHashMap<Integer, MenuSlot>();
		
		List<String> lorePlayer = Language.MENU_ITEM_LORES_HOME_PLAYER.getListText(player);
		List<String> loreShop = Language.MENU_ITEM_LORES_HOME_SHOP.getListText(player);
		
		final ItemStack itemPaneBlack = EquipmentUtil.createItem(MaterialEnum.BLACK_STAINED_GLASS_PANE, prefix, 1);
		final ItemStack itemPaneWhite = EquipmentUtil.createItem(MaterialEnum.WHITE_STAINED_GLASS_PANE, prefix, 1);
		final ItemStack itemPlayer = EquipmentUtil.createPlayerHead(playerName, headerPlayer, lorePlayer);
		final ItemStack itemShop = EquipmentUtil.createItem(MaterialEnum.WHEAT, headerShop, 1, loreShop);
		
		menuSlotPlayer.setItem(itemPlayer);
		menuSlotShop.setItem(itemShop);
		
		menuSlotPlayer.setActionArguments(ActionCategory.ALL_CLICK, "Herbalism Menu Player");
		menuSlotShop.setActionArguments(ActionCategory.ALL_CLICK, "Herbalism Menu Shop");

		mapSlot.put(menuSlotPlayer.getSlot(), menuSlotPlayer);
		mapSlot.put(menuSlotShop.getSlot(), menuSlotShop);
		
		for (int slot : arraySlotPaneWhite) {
			final MenuSlot menuSlot = new MenuSlot(slot);
			
			menuSlot.setItem(itemPaneWhite);
			mapSlot.put(slot, menuSlot);
		}
		
		for (int slot = 0; slot < size; slot++) {
			if (!mapSlot.containsKey(slot)) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				menuSlot.setItem(itemPaneBlack);
				mapSlot.put(slot, menuSlot);
			}
		}
		
		final MenuGUI menuGUI = new MenuGUI(null, id, row, title, executor, false, mapSlot);
		
		Menu.openMenu(player, menuGUI);
		return;
	}
	
	public final void openMenuPlayer(Player player) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final GameManager gameManager = plugin.getGameManager();
		final PlayerManager playerManager = plugin.getPlayerManager();
		final AbilityManager abilityManager = gameManager.getAbilityManager();
		final CropsPropertiesManager cropsManager = gameManager.getCropsPropertiesManager();
		final FertilizerManager fertilizerManager = gameManager.getFertilizerManager();
		final PlayerHerbalismManager playerHerbalismManager = playerManager.getPlayerHerbalismManager();
		final PlaceholderManager placeholderManager = pluginManager.getPlaceholderManager();
		final HerbalismConfig mainConfig = plugin.getMainConfig();
		final MenuExecutor executor = getMenuExecutor();
		final PlayerHerbalism playerHerbalism = playerHerbalismManager.getPlayerHerbalism(player);
		
		if (playerHerbalism != null) {
			final String playerName = player.getName();
			final int playerLevel = playerHerbalism.getLevel();
			final int playerCrops = playerHerbalism.getTotalCrops();
			final int playerMaxCrops = playerHerbalism.getMaxCrops();
			final int unlockedAbility = playerHerbalism.getUnlockedAbilities().size();
			final int unlockedCrops = playerHerbalism.getUnlockedCrops().size();
			final int unlockedFertilizer = playerHerbalism.getUnlockedFertilizer().size();
			final int maxAbility = abilityManager.getAbilityIds().size();
			final int maxCrops = cropsManager.getCropsIds().size();
			final int maxFertilizer = fertilizerManager.getFertilizerIds().size();
			final float playerExp = playerHerbalism.getExp();
			final float playerExpUp = playerHerbalism.getExpToUp();
			final boolean enableMenuPageHome = mainConfig.isEnableMenuPageHome();
			
			final String id = "Herbalism Player";
			final String textTitle = Language.MENU_PAGE_TITLE_PLAYER.getText(player);
			final int row = 3;
			final int size = row * 9;
			final Text title = new TextLine(textTitle);
			final String prefix = placeholderManager.getPlaceholder("prefix");
			final String headerPageHome = Language.MENU_ITEM_HEADER_PAGE_HOME.getText(player);
			final String headerStats = Language.MENU_ITEM_HEADER_PLAYER_STATS.getText(player);
			final String headerProgress = Language.MENU_ITEM_HEADER_PLAYER_PROGRESS.getText(player);
			final int[] arraySlotPaneWhite = new int[] {0, 8, 9, 17, 18, 26};
			final MenuSlot menuSlotPageHome = new MenuSlot(13);
			final MenuSlot menuSlotStats = new MenuSlot(11);
			final MenuSlot menuSlotProgress = new MenuSlot(15);
			final Map<Integer, MenuSlot> mapSlot = new ConcurrentHashMap<Integer, MenuSlot>();
			final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
			
			List<String> lorePageHome = Language.MENU_ITEM_LORES_PAGE_HOME.getListText(player);
			List<String> loreStats = Language.MENU_ITEM_LORES_PLAYER_STATS.getListText(player);
			List<String> loreProgress = Language.MENU_ITEM_LORES_PLAYER_PROGRESS.getListText(player);
			
			mapPlaceholder.put("player_name", playerName);
			mapPlaceholder.put("player_level", String.valueOf(playerLevel));
			mapPlaceholder.put("player_crops", String.valueOf(playerCrops));
			mapPlaceholder.put("player_max_crops", String.valueOf(playerMaxCrops));
			mapPlaceholder.put("player_exp", String.valueOf(MathUtil.roundNumber(playerExp, 1)));
			mapPlaceholder.put("player_exp_up", String.valueOf(MathUtil.roundNumber(playerExpUp, 1)));
			mapPlaceholder.put("player_unlocked_ability", String.valueOf(unlockedAbility));
			mapPlaceholder.put("player_unlocked_crops", String.valueOf(unlockedCrops));
			mapPlaceholder.put("player_unlocked_fertilizer", String.valueOf(unlockedFertilizer));
			mapPlaceholder.put("max_ability", String.valueOf(maxAbility));
			mapPlaceholder.put("max_crops", String.valueOf(maxCrops));
			mapPlaceholder.put("max_fertilizer", String.valueOf(maxFertilizer));
			
			loreStats = TextUtil.placeholder(mapPlaceholder, loreStats);
			loreProgress = TextUtil.placeholder(mapPlaceholder, loreProgress);
			
			final ItemStack itemPaneBlack = EquipmentUtil.createItem(MaterialEnum.BLACK_STAINED_GLASS_PANE, prefix, 1);
			final ItemStack itemPaneWhite = EquipmentUtil.createItem(MaterialEnum.WHITE_STAINED_GLASS_PANE, prefix, 1);
			final ItemStack itemPageHome = EquipmentUtil.createItem(MaterialEnum.LILY_PAD, headerPageHome, 1, lorePageHome);
			final ItemStack itemStats = EquipmentUtil.createPlayerHead(playerName, headerStats, loreStats);
			final ItemStack itemProgress = EquipmentUtil.createItem(MaterialEnum.BOOK, headerProgress, 1, loreProgress);
			
			menuSlotStats.setItem(itemStats);
			menuSlotProgress.setItem(itemProgress);
			
			menuSlotProgress.setActionArguments(ActionCategory.ALL_CLICK, "Herbalism Menu Progress");
	
			mapSlot.put(menuSlotStats.getSlot(), menuSlotStats);
			mapSlot.put(menuSlotProgress.getSlot(), menuSlotProgress);
			
			if (enableMenuPageHome) {
				menuSlotPageHome.setItem(itemPageHome);
				menuSlotPageHome.setActionArguments(ActionCategory.ALL_CLICK, "Herbalism Menu Home");
				mapSlot.put(menuSlotPageHome.getSlot(), menuSlotPageHome);
			}
			
			for (int slot : arraySlotPaneWhite) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				menuSlot.setItem(itemPaneWhite);
				mapSlot.put(slot, menuSlot);
			}
			
			for (int slot = 0; slot < size; slot++) {
				if (!mapSlot.containsKey(slot)) {
					final MenuSlot menuSlot = new MenuSlot(slot);
					
					menuSlot.setItem(itemPaneBlack);
					mapSlot.put(slot, menuSlot);
				}
			}
			
			final MenuGUI menuGUI = new MenuGUI(null, id, row, title, executor, false, mapSlot);
			
			Menu.openMenu(player, menuGUI);
			return;
		}
	}
	
	public final void openMenuProgress(Player player) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final GameManager gameManager = plugin.getGameManager();
		final PlayerManager playerManager = plugin.getPlayerManager();
		final AbilityManager abilityManager = gameManager.getAbilityManager();
		final CropsPropertiesManager cropsManager = gameManager.getCropsPropertiesManager();
		final FertilizerManager fertilizerManager = gameManager.getFertilizerManager();
		final PlayerHerbalismManager playerHerbalismManager = playerManager.getPlayerHerbalismManager();
		final PlaceholderManager placeholderManager = pluginManager.getPlaceholderManager();
		final HerbalismConfig mainConfig = plugin.getMainConfig();
		final MenuExecutor executor = getMenuExecutor();
		final PlayerHerbalism playerHerbalism = playerHerbalismManager.getPlayerHerbalism(player);
		
		if (playerHerbalism != null) {
			final String playerName = player.getName();
			final int playerLevel = playerHerbalism.getLevel();
			final int playerCrops = playerHerbalism.getTotalCrops();
			final int playerMaxCrops = playerHerbalism.getMaxCrops();
			final int unlockedAbility = playerHerbalism.getUnlockedAbilities().size();
			final int unlockedCrops = playerHerbalism.getUnlockedCrops().size();
			final int unlockedFertilizer = playerHerbalism.getUnlockedFertilizer().size();
			final int maxAbility = abilityManager.getAbilityIds().size();
			final int maxCrops = cropsManager.getCropsIds().size();
			final int maxFertilizer = fertilizerManager.getFertilizerIds().size();
			final float playerExp = playerHerbalism.getExp();
			final float playerExpUp = playerHerbalism.getExpToUp();
			final boolean enableMenuPageBack = mainConfig.isEnableMenuPageBack();
			
			final String id = "Herbalism Progress";
			final String textTitle = Language.MENU_PAGE_TITLE_PROGRESS.getText(player);
			final int row = 5;
			final int size = row * 9;
			final Text title = new TextLine(textTitle);
			final String prefix = placeholderManager.getPlaceholder("prefix");
			final String headerPageBack = Language.MENU_ITEM_HEADER_PAGE_BACK.getText(player);
			final String headerProgressStats = Language.MENU_ITEM_HEADER_PROGRESS_STATS.getText(player);
			final String headerProgressAbility = Language.MENU_ITEM_HEADER_PROGRESS_ABILITY.getText(player);
			final String headerProgressCrops = Language.MENU_ITEM_HEADER_PROGRESS_CROPS.getText(player);
			final String headerProgressFertilizer = Language.MENU_ITEM_HEADER_PROGRESS_FERTILIZER.getText(player);
			final int[] arraySlotPaneWhite = new int[] {0, 8, 9, 17, 18, 26, 27, 35, 36, 44};
			final int[] arraySlotPaneRed = new int[] {1, 2, 6, 7, 37, 38, 42, 43};
			final int[] arraySlotPageBack = new int[] {39, 40, 41};
			final int[] arraySlotProgressStats = new int[] {3, 4, 5};
			final MenuSlot menuSlotProgressAbility = new MenuSlot(20);
			final MenuSlot menuSlotProgressCrops = new MenuSlot(22);
			final MenuSlot menuSlotProgressFertilizer = new MenuSlot(24);
			final Map<Integer, MenuSlot> mapSlot = new ConcurrentHashMap<Integer, MenuSlot>();
			final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
			
			List<String> lorePageBack = Language.MENU_ITEM_LORES_PAGE_BACK.getListText(player);
			List<String> loreProgressStats = Language.MENU_ITEM_LORES_PROGRESS_STATS.getListText(player);
			List<String> loreProgressAbility = Language.MENU_ITEM_LORES_PROGRESS_ABILITY.getListText(player);
			List<String> loreProgressCrops = Language.MENU_ITEM_LORES_PROGRESS_CROPS.getListText(player);
			List<String> loreProgressFertilizer = Language.MENU_ITEM_LORES_PROGRESS_FERTILIZER.getListText(player);
			
			mapPlaceholder.put("player_name", playerName);
			mapPlaceholder.put("player_level", String.valueOf(playerLevel));
			mapPlaceholder.put("player_crops", String.valueOf(playerCrops));
			mapPlaceholder.put("player_max_crops", String.valueOf(playerMaxCrops));
			mapPlaceholder.put("player_exp", String.valueOf(MathUtil.roundNumber(playerExp, 1)));
			mapPlaceholder.put("player_exp_up", String.valueOf(MathUtil.roundNumber(playerExpUp, 1)));
			mapPlaceholder.put("player_unlocked_ability", String.valueOf(unlockedAbility));
			mapPlaceholder.put("player_unlocked_crops", String.valueOf(unlockedCrops));
			mapPlaceholder.put("player_unlocked_fertilizer", String.valueOf(unlockedFertilizer));
			mapPlaceholder.put("max_ability", String.valueOf(maxAbility));
			mapPlaceholder.put("max_crops", String.valueOf(maxCrops));
			mapPlaceholder.put("max_fertilizer", String.valueOf(maxFertilizer));
			
			loreProgressStats = TextUtil.placeholder(mapPlaceholder, loreProgressStats);
			loreProgressAbility = TextUtil.placeholder(mapPlaceholder, loreProgressAbility);
			loreProgressCrops = TextUtil.placeholder(mapPlaceholder, loreProgressCrops);
			loreProgressFertilizer = TextUtil.placeholder(mapPlaceholder, loreProgressFertilizer);
	
			final ItemStack itemPaneBlack = EquipmentUtil.createItem(MaterialEnum.BLACK_STAINED_GLASS_PANE, prefix, 1);
			final ItemStack itemPaneWhite = EquipmentUtil.createItem(MaterialEnum.WHITE_STAINED_GLASS_PANE, prefix, 1);
			final ItemStack itemPaneRed = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, prefix, 1);
			final ItemStack itemPageBack = EquipmentUtil.createItem(MaterialEnum.IRON_BARS, headerPageBack, 1, lorePageBack);
			final ItemStack itemProgressStats = EquipmentUtil.createItem(MaterialEnum.SIGN, headerProgressStats, 1, loreProgressStats);
			final ItemStack itemProgressAbility = EquipmentUtil.createItem(MaterialEnum.IRON_HOE, headerProgressAbility, 1, loreProgressAbility);
			final ItemStack itemProgressCrops = EquipmentUtil.createItem(MaterialEnum.WHEAT, headerProgressCrops, 1, loreProgressCrops);
			final ItemStack itemProgressFertilizer = EquipmentUtil.createItem(MaterialEnum.GRAY_DYE, headerProgressFertilizer, 1, loreProgressFertilizer);
			
			EquipmentUtil.addFlag(itemProgressAbility, FlagEnum.HIDE_ATTRIBUTES);
			
			menuSlotProgressAbility.setItem(itemProgressAbility);
			menuSlotProgressCrops.setItem(itemProgressCrops);
			menuSlotProgressFertilizer.setItem(itemProgressFertilizer);
			
			menuSlotProgressAbility.setActionArguments(ActionCategory.ALL_CLICK, "Herbalism Menu Progress_Ability");
			menuSlotProgressCrops.setActionArguments(ActionCategory.ALL_CLICK, "Herbalism Menu Progress_Crops");
			menuSlotProgressFertilizer.setActionArguments(ActionCategory.ALL_CLICK, "Herbalism Menu Progress_Fertilizer");
			
			mapSlot.put(menuSlotProgressAbility.getSlot(), menuSlotProgressAbility);
			mapSlot.put(menuSlotProgressCrops.getSlot(), menuSlotProgressCrops);
			mapSlot.put(menuSlotProgressFertilizer.getSlot(), menuSlotProgressFertilizer);
			
			if (enableMenuPageBack) {
				for (int slot : arraySlotPageBack) {
					final MenuSlot menuSlot = new MenuSlot(slot);
					
					menuSlot.setItem(itemPageBack);
					menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "Herbalism Menu Player");
					mapSlot.put(slot, menuSlot);
				}
			}
			
			for (int slot : arraySlotProgressStats) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				menuSlot.setItem(itemProgressStats);
				mapSlot.put(slot, menuSlot);
			} 
			
			for (int slot : arraySlotPaneWhite) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				menuSlot.setItem(itemPaneWhite);
				mapSlot.put(slot, menuSlot);
			}
			
			for (int slot : arraySlotPaneRed) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				menuSlot.setItem(itemPaneRed);
				mapSlot.put(slot, menuSlot);
			}
			
			for (int slot = 0; slot < size; slot++) {
				if (!mapSlot.containsKey(slot)) {
					final MenuSlot menuSlot = new MenuSlot(slot);
					
					menuSlot.setItem(itemPaneBlack);
					mapSlot.put(slot, menuSlot);
				}
			}
			
			final MenuGUI menuGUI = new MenuGUI(null, id, row, title, executor, false, mapSlot);
			
			Menu.openMenu(player, menuGUI);
			return;
		}
	}
	
	public final void openMenuProgressAbility(Player player) {
		openMenuProgressAbility(player, 1);
	}
	
	public final void openMenuProgressAbility(Player player, int page) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final GameManager gameManager = plugin.getGameManager();
		final AbilityManager abilityManager = gameManager.getAbilityManager();
		final PlaceholderManager placeholderManager = pluginManager.getPlaceholderManager();
		final MenuExecutor executor = getMenuExecutor();
		final String id = "Herbalism Progress_Ability";
		final Collection<Ability> allAbility = abilityManager.getAllAbility();
		
		final int row = 6;
		final int size = allAbility.size();
		final int slots = 28;
		final int maxPage = Math.max(1, size % slots > 0 ? (size/slots) + 1 : size/slots);
		final String prefix = placeholderManager.getPlaceholder("prefix");
		final String headerPageBack = Language.MENU_ITEM_HEADER_PAGE_BACK.getText(player);
		final String headerPageNext = Language.MENU_ITEM_HEADER_PAGE_NEXT.getText(player);
		final String headerPagePrevious = Language.MENU_ITEM_HEADER_PAGE_PREVIOUS.getText(player);
		final int[] arraySlotPane = new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 17, 18, 26, 27, 35, 36, 44, 45, 46, 47, 51, 52, 53};
		final int[] arraySlotBack = new int[] {48, 49, 50};
		final int[] arraySlotNext = new int[] {26, 35};
		final int[] arraySlotPrevious = new int[] {18, 27};
		final int[] abilitySlotComponent = new int[] {10, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 23, 24, 25, 28, 29, 30, 31, 32, 33, 34, 37, 38, 39, 40, 41, 42, 43};
		
		final Map<String, Integer> mapAbility = new HashMap<String, Integer>();
		final Map<Integer, MenuSlot> mapSlot = new ConcurrentHashMap<Integer, MenuSlot>();
		final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
		final List<Ability> listAbilityUnlocked = new ArrayList<Ability>();
		final List<Ability> listAbilityLocked = new ArrayList<Ability>();
		
		for (Ability ability : allAbility) {
			mapAbility.put(ability.getId(), ability.getRequirementLevel());
		}
		
		for (String abilityId : SortUtil.sortByValue(mapAbility).keySet()) {
			final Ability ability = abilityManager.getAbility(abilityId);
			
			if (ability.isAllowed(player)) {
				listAbilityUnlocked.add(ability);
			} else {
				listAbilityLocked.add(ability);
			}
		}
		
		page = MathUtil.limitInteger(page, 1, maxPage);
		
		String textTitle = Language.MENU_PAGE_TITLE_PROGRESS_ABILITY.getText(player);
		
		List<String> lorePageBack = Language.MENU_ITEM_LORES_PAGE_BACK.getListText(player);
		List<String> lorePageNext = Language.MENU_ITEM_LORES_PAGE_NEXT.getListText(player);
		List<String> lorePagePrevious = Language.MENU_ITEM_LORES_PAGE_PREVIOUS.getListText(player);
		
		mapPlaceholder.put("page", String.valueOf(page));
		mapPlaceholder.put("max_page", String.valueOf(maxPage));
		
		textTitle = TextUtil.placeholder(mapPlaceholder, textTitle);
		
		final ItemStack itemPaneBlack = EquipmentUtil.createItem(MaterialEnum.BLACK_STAINED_GLASS_PANE, prefix, 1);
		final ItemStack itemPageBack = EquipmentUtil.createItem(MaterialEnum.IRON_BARS, headerPageBack, 1, lorePageBack);
		final ItemStack itemPageNext = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerPageNext, 1, lorePageNext);
		final ItemStack itemPagePrevious = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerPagePrevious, 1, lorePagePrevious);
		
		for (int slot : arraySlotPane) {
			final MenuSlot menuSlot = new MenuSlot(slot);
			
			menuSlot.setItem(itemPaneBlack);
			mapSlot.put(slot, menuSlot);
		}
		
		for (int slot : arraySlotBack) {
			final MenuSlot menuSlot = new MenuSlot(slot);
			
			menuSlot.setItem(itemPageBack);
			menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "Herbalism Menu Progress");
			mapSlot.put(slot, menuSlot);
		} 
		
		if (page < maxPage) {
			for (int slot : arraySlotNext) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				menuSlot.setItem(itemPageNext);
				menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "Herbalism Menu Progress_Ability " + (page+1));
				mapSlot.put(slot, menuSlot);
			} 
		}
		
		if (page > 1) {
			for (int slot : arraySlotPrevious) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				menuSlot.setItem(itemPagePrevious);
				menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "Herbalism Menu Progress_Ability " + (page-1));
				mapSlot.put(slot, menuSlot);
			} 
		}
		
		for (int index = 0; index < abilitySlotComponent.length; index++) {
			final int slot = abilitySlotComponent[index];
			final int indexRegister = index + (slots*(page-1));
			
			if (indexRegister < size) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				if (indexRegister < listAbilityUnlocked.size()) {
					final Ability ability = listAbilityUnlocked.get(indexRegister);
					final String abilityId = ability.getId();
					final String abilityName = ability.getName(player);
					final String divider = "\n";
					final List<String> abilityDescriptions = new ArrayList<String>();
					
					for (String abilityDescription : ability.getDescription(player)) {
						final String format = Language.MENU_ITEM_FORMAT_PROGRESS_ABILITY_DESCRIPTION.getText(player);
						final String text = TextUtil.placeholder(format, "ability_description", abilityDescription);
						
						abilityDescriptions.add(text);
					}
					
					final String abilityMergeDescriptions = TextUtil.convertListToString(abilityDescriptions, divider);
					
					String headerAbility = Language.MENU_ITEM_HEADER_PROGRESS_ABILITY_UNLOCKED.getText(player);
					List<String> loreAbility = Language.MENU_ITEM_LORES_PROGRESS_ABILITY_UNLOCKED.getListText(player);
					
					mapPlaceholder.clear();
					mapPlaceholder.put("ability_id", abilityId);
					mapPlaceholder.put("ability_name", abilityName);
					mapPlaceholder.put("ability_descriptions", abilityMergeDescriptions);
					
					headerAbility = TextUtil.placeholder(mapPlaceholder, headerAbility);
					loreAbility = TextUtil.placeholder(mapPlaceholder, loreAbility);
					
					final ItemStack itemAbility = EquipmentUtil.createItem(MaterialEnum.BOOK, headerAbility, 1, loreAbility);
					
					menuSlot.setItem(itemAbility);
					mapSlot.put(slot, menuSlot);
				} else {
					final Ability ability = listAbilityLocked.get(indexRegister - listAbilityUnlocked.size());
					final String abilityId = ability.getId();
					final String abilityName = ability.getName(player);
					final String requirementPermission = ability.getRequirementPermission();
					final int requirementLevel = ability.getRequirementLevel();
					
					String headerAbility = Language.MENU_ITEM_HEADER_PROGRESS_ABILITY_LOCKED.getText(player);
					List<String> loreAbility = Language.MENU_ITEM_LORES_PROGRESS_ABILITY_LOCKED.getListText(player);
					
					mapPlaceholder.clear();
					mapPlaceholder.put("ability_id", abilityId);
					mapPlaceholder.put("ability_name", abilityName);
					mapPlaceholder.put("requirement_permission", requirementPermission != null ? requirementPermission : "null");
					mapPlaceholder.put("requirement_level", String.valueOf(requirementLevel));
					
					headerAbility = TextUtil.placeholder(mapPlaceholder, headerAbility);
					loreAbility = TextUtil.placeholder(mapPlaceholder, loreAbility);
					
					final ItemStack itemAbility = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerAbility, 1, loreAbility);
					
					menuSlot.setItem(itemAbility);
					mapSlot.put(slot, menuSlot);
				}
			} else {
				break;
			}
		}
		
		final Text title = new TextLine(textTitle);
		final MenuGUI menuGUI = new MenuGUI(player, id, row, title, executor, false, mapSlot);
		
		Menu.openMenu(player, menuGUI);
		return;
	}
	
	public final void openMenuProgressCrops(Player player) {
		openMenuProgressCrops(player, 1);
	}
	
	public final void openMenuProgressCrops(Player player, int page) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final GameManager gameManager = plugin.getGameManager();
		final CropsPropertiesManager cropsManager = gameManager.getCropsPropertiesManager();
		final PlaceholderManager placeholderManager = pluginManager.getPlaceholderManager();
		final MenuExecutor executor = getMenuExecutor();
		final String id = "Herbalism Progress_Crops";
		final Collection<CropsProperties> allCropsProperties = cropsManager.getAllCropsProperties();
		final Collection<String> allCategory = cropsManager.getAllCategory();
		
		final int row = 6;
		final int size = allCropsProperties.size();
		final int slots = 28;
		final int maxPage = Math.max(1, size % slots > 0 ? (size/slots) + 1 : size/slots);
		final String prefix = placeholderManager.getPlaceholder("prefix");
		final String headerPageBack = Language.MENU_ITEM_HEADER_PAGE_BACK.getText(player);
		final String headerPageNext = Language.MENU_ITEM_HEADER_PAGE_NEXT.getText(player);
		final String headerPagePrevious = Language.MENU_ITEM_HEADER_PAGE_PREVIOUS.getText(player);
		final int[] arraySlotPane = new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 17, 18, 26, 27, 35, 36, 44, 45, 46, 47, 51, 52, 53};
		final int[] arraySlotBack = new int[] {48, 49, 50};
		final int[] arraySlotNext = new int[] {26, 35};
		final int[] arraySlotPrevious = new int[] {18, 27};
		final int[] abilitySlotComponent = new int[] {10, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 23, 24, 25, 28, 29, 30, 31, 32, 33, 34, 37, 38, 39, 40, 41, 42, 43};
		
		final Map<String, Integer> mapCropsProperties = new HashMap<String, Integer>();
		final Map<Integer, MenuSlot> mapSlot = new ConcurrentHashMap<Integer, MenuSlot>();
		final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
		final List<CropsProperties> listCropsUnlocked = new ArrayList<CropsProperties>();
		final List<CropsProperties> listCropsLocked = new ArrayList<CropsProperties>();
		
		for (CropsProperties cropsProperties : allCropsProperties) {
			final CropsRequirement requirement = cropsProperties.getCropsRequirement();
			
			mapCropsProperties.put(cropsProperties.getId(), requirement.getLevel());
		}
		
		for (String category : allCategory) {
			for (CropsProperties cropsProperties : cropsManager.getAllCropsProperties(category)) {
				final CropsRequirement requirement = cropsProperties.getCropsRequirement();
				
				if (requirement.isAllowed(player)) {
					listCropsUnlocked.add(cropsProperties);
				}
			}
		}
		
		for (String crops : SortUtil.sortByValue(mapCropsProperties).keySet()) {
			final CropsProperties cropsProperties = cropsManager.getCropsProperties(crops);
			final CropsRequirement requirement = cropsProperties.getCropsRequirement();
			
			if (!requirement.isAllowed(player)) {
				listCropsLocked.add(cropsProperties);
			}
		}
		
		page = MathUtil.limitInteger(page, 1, maxPage);
		
		String textTitle = Language.MENU_PAGE_TITLE_PROGRESS_CROPS.getText(player);
		
		List<String> lorePageBack = Language.MENU_ITEM_LORES_PAGE_BACK.getListText(player);
		List<String> lorePageNext = Language.MENU_ITEM_LORES_PAGE_NEXT.getListText(player);
		List<String> lorePagePrevious = Language.MENU_ITEM_LORES_PAGE_PREVIOUS.getListText(player);
		
		mapPlaceholder.put("page", String.valueOf(page));
		mapPlaceholder.put("max_page", String.valueOf(maxPage));
		
		textTitle = TextUtil.placeholder(mapPlaceholder, textTitle);
		
		final ItemStack itemPaneBlack = EquipmentUtil.createItem(MaterialEnum.BLACK_STAINED_GLASS_PANE, prefix, 1);
		final ItemStack itemPageBack = EquipmentUtil.createItem(MaterialEnum.IRON_BARS, headerPageBack, 1, lorePageBack);
		final ItemStack itemPageNext = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerPageNext, 1, lorePageNext);
		final ItemStack itemPagePrevious = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerPagePrevious, 1, lorePagePrevious);
		
		for (int slot : arraySlotPane) {
			final MenuSlot menuSlot = new MenuSlot(slot);
			
			menuSlot.setItem(itemPaneBlack);
			mapSlot.put(slot, menuSlot);
		}
		
		for (int slot : arraySlotBack) {
			final MenuSlot menuSlot = new MenuSlot(slot);
			
			menuSlot.setItem(itemPageBack);
			menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "Herbalism Menu Progress");
			mapSlot.put(slot, menuSlot);
		} 
		
		if (page < maxPage) {
			for (int slot : arraySlotNext) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				menuSlot.setItem(itemPageNext);
				menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "Herbalism Menu Progress_Crops " + (page+1));
				mapSlot.put(slot, menuSlot);
			} 
		}
		
		if (page > 1) {
			for (int slot : arraySlotPrevious) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				menuSlot.setItem(itemPagePrevious);
				menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "Herbalism Menu Progress_Crops " + (page-1));
				mapSlot.put(slot, menuSlot);
			} 
		}
		
		for (int index = 0; index < abilitySlotComponent.length; index++) {
			final int slot = abilitySlotComponent[index];
			final int indexRegister = index + (slots*(page-1));
			
			if (indexRegister < size) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				if (indexRegister < listCropsUnlocked.size()) {
					final CropsProperties cropsProperties = listCropsUnlocked.get(indexRegister);
					final CropsDrop cropsDrop = cropsProperties.getCropsDrop();
					final String crops = cropsProperties.getId();
					final String category = cropsProperties.getCategory();
					final String divider = "\n";
					final ItemStack item = cropsProperties.getIcon();
					final MaterialEnum materialEnum = MaterialEnum.getMaterialEnum(item);
					final int duration = cropsProperties.getDuration();
					final double dropsExp = cropsDrop.getExp();
					final Collection<CropsDropItem> allCropsDropItem = cropsDrop.getItems();
					final List<String> dropsItem = new ArrayList<String>();
					
					for (CropsDropItem cropsDropItem : allCropsDropItem) {
						final String dropItemID = cropsDropItem.getId();
						final double dropItemChance = cropsDropItem.getChance();
						
						String format = Language.MENU_ITEM_FORMAT_PROGRESS_CROPS_DROPS_ITEM.getText(player);
						
						mapPlaceholder.clear();
						mapPlaceholder.put("crops_drops_item", String.valueOf(dropItemID));
						mapPlaceholder.put("crops_drop_item_chance", String.valueOf(MathUtil.roundNumber(dropItemChance)));
						
						format = TextUtil.placeholder(mapPlaceholder, format);
						
						dropsItem.add(format);
					}
					
					final String cropsMergeDropItem = TextUtil.convertListToString(dropsItem, divider);
					
					String headerCrops = Language.MENU_ITEM_HEADER_PROGRESS_CROPS_UNLOCKED.getText(player);
					List<String> loreCrops = Language.MENU_ITEM_LORES_PROGRESS_CROPS_UNLOCKED.getListText(player);
					
					mapPlaceholder.clear();
					mapPlaceholder.put("crops_id", crops);
					mapPlaceholder.put("crops_category", category);
					mapPlaceholder.put("crops_duration", String.valueOf(duration));
					mapPlaceholder.put("crops_drops_exp", String.valueOf(MathUtil.roundNumber(dropsExp)));
					mapPlaceholder.put("crops_drops_item", cropsMergeDropItem);
					
					headerCrops = TextUtil.placeholder(mapPlaceholder, headerCrops);
					loreCrops = TextUtil.placeholder(mapPlaceholder, loreCrops);
					
					final ItemStack itemCrops = EquipmentUtil.createItem(materialEnum, headerCrops, 1, loreCrops);
					
					menuSlot.setItem(itemCrops);
					mapSlot.put(slot, menuSlot);
				} else {
					final CropsProperties cropsProperties = listCropsLocked.get(indexRegister - listCropsUnlocked.size());
					final CropsRequirement requirement = cropsProperties.getCropsRequirement();
					final String crops = cropsProperties.getId();
					final String category = cropsProperties.getCategory();
					final String requirementPermission = requirement.getPermission();
					final int requirementLevel = requirement.getLevel();
					final int duration = cropsProperties.getDuration();
					
					String headerCrops = Language.MENU_ITEM_HEADER_PROGRESS_CROPS_LOCKED.getText(player);
					List<String> loreCrops = Language.MENU_ITEM_LORES_PROGRESS_CROPS_LOCKED.getListText(player);
					
					mapPlaceholder.clear();
					mapPlaceholder.put("crops_id", crops);
					mapPlaceholder.put("crops_category", category);
					mapPlaceholder.put("crops_duration", String.valueOf(duration));
					mapPlaceholder.put("requirement_permission", requirementPermission != null ? requirementPermission : "null");
					mapPlaceholder.put("requirement_level", String.valueOf(requirementLevel));
					
					headerCrops = TextUtil.placeholder(mapPlaceholder, headerCrops);
					loreCrops = TextUtil.placeholder(mapPlaceholder, loreCrops);
					
					final ItemStack itemCrops = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerCrops, 1, loreCrops);
					
					menuSlot.setItem(itemCrops);
					mapSlot.put(slot, menuSlot);
				}
			} else {
				break;
			}
		}
		
		final Text title = new TextLine(textTitle);
		final MenuGUI menuGUI = new MenuGUI(player, id, row, title, executor, false, mapSlot);
		
		Menu.openMenu(player, menuGUI);
		return;
	}
	
	public final void openMenuProgressFertilizer(Player player) {
		openMenuProgressFertilizer(player, 1);
	}
	
	public final void openMenuProgressFertilizer(Player player, int page) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final GameManager gameManager = plugin.getGameManager();
		final FertilizerManager fertilizerManager = gameManager.getFertilizerManager();
		final PlaceholderManager placeholderManager = pluginManager.getPlaceholderManager();
		final MenuExecutor executor = getMenuExecutor();
		final String id = "Herbalism Progress_Fertilizer";
		final Collection<FertilizerProperties> allFertilizerProperties = fertilizerManager.getAllFertilizer();
		
		final int row = 6;
		final int size = allFertilizerProperties.size();
		final int slots = 28;
		final int maxPage = Math.max(1, size % slots > 0 ? (size/slots) + 1 : size/slots);
		final String prefix = placeholderManager.getPlaceholder("prefix");
		final String headerPageBack = Language.MENU_ITEM_HEADER_PAGE_BACK.getText(player);
		final String headerPageNext = Language.MENU_ITEM_HEADER_PAGE_NEXT.getText(player);
		final String headerPagePrevious = Language.MENU_ITEM_HEADER_PAGE_PREVIOUS.getText(player);
		final int[] arraySlotPane = new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 17, 18, 26, 27, 35, 36, 44, 45, 46, 47, 51, 52, 53};
		final int[] arraySlotBack = new int[] {48, 49, 50};
		final int[] arraySlotNext = new int[] {26, 35};
		final int[] arraySlotPrevious = new int[] {18, 27};
		final int[] abilitySlotComponent = new int[] {10, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 23, 24, 25, 28, 29, 30, 31, 32, 33, 34, 37, 38, 39, 40, 41, 42, 43};
		
		final Map<String, Integer> mapFertilizer = new HashMap<String, Integer>();
		final Map<Integer, MenuSlot> mapSlot = new ConcurrentHashMap<Integer, MenuSlot>();
		final HashMap<String, List<FertilizerProperties>> mapListFertilizer = new HashMap<String, List<FertilizerProperties>>();
		final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
		final List<FertilizerProperties> listFertilizerUnlocked = new ArrayList<FertilizerProperties>();
		final List<FertilizerProperties> listFertilizerLocked = new ArrayList<FertilizerProperties>();
		
		for (FertilizerProperties fertilizerProperties : allFertilizerProperties) {
			final ItemRequirement requirement = fertilizerProperties.getRequirement();
			final ItemStack item = fertilizerProperties.getItem();
			final String serialize = "Type: " + item.getType() + ", Data: "+ EquipmentUtil.getData(item);
			final String fertilizer = fertilizerProperties.getId();
			final int level = requirement.getLevel();
			
			mapFertilizer.put(fertilizer, level);
			
			if (mapListFertilizer.containsKey(serialize)) {
				mapListFertilizer.get(serialize).add(fertilizerProperties);
			} else {
				final List<FertilizerProperties> listFertilizer = new ArrayList<FertilizerProperties>();
				
				listFertilizer.add(fertilizerProperties);
				mapListFertilizer.put(serialize, listFertilizer);
			}
		}
		
		for (List<FertilizerProperties> listFertilizer : mapListFertilizer.values()) {
			for (FertilizerProperties fertilizerProperties : listFertilizer) {
				final ItemRequirement requirement = fertilizerProperties.getRequirement();
				
				if (requirement.isAllowed(player)) {
					listFertilizerUnlocked.add(fertilizerProperties);
				}
			}
		}
		
		for (String fertilizer : SortUtil.sortByValue(mapFertilizer).keySet()) {
			final FertilizerProperties fertilizerProperties = fertilizerManager.getFertilizer(fertilizer);
			final ItemRequirement requirement = fertilizerProperties.getRequirement();
			
			if (!requirement.isAllowed(player)) {
				listFertilizerLocked.add(fertilizerProperties);
			}
		}
		
		page = MathUtil.limitInteger(page, 1, maxPage);
		
		String textTitle = Language.MENU_PAGE_TITLE_PROGRESS_FERTILIZER.getText(player);
		
		List<String> lorePageBack = Language.MENU_ITEM_LORES_PAGE_BACK.getListText(player);
		List<String> lorePageNext = Language.MENU_ITEM_LORES_PAGE_NEXT.getListText(player);
		List<String> lorePagePrevious = Language.MENU_ITEM_LORES_PAGE_PREVIOUS.getListText(player);
		
		mapPlaceholder.put("page", String.valueOf(page));
		mapPlaceholder.put("max_page", String.valueOf(maxPage));
		
		textTitle = TextUtil.placeholder(mapPlaceholder, textTitle);
		
		final ItemStack itemPaneBlack = EquipmentUtil.createItem(MaterialEnum.BLACK_STAINED_GLASS_PANE, prefix, 1);
		final ItemStack itemPageBack = EquipmentUtil.createItem(MaterialEnum.IRON_BARS, headerPageBack, 1, lorePageBack);
		final ItemStack itemPageNext = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerPageNext, 1, lorePageNext);
		final ItemStack itemPagePrevious = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerPagePrevious, 1, lorePagePrevious);
		
		for (int slot : arraySlotPane) {
			final MenuSlot menuSlot = new MenuSlot(slot);
			
			menuSlot.setItem(itemPaneBlack);
			mapSlot.put(slot, menuSlot);
		}
		
		for (int slot : arraySlotBack) {
			final MenuSlot menuSlot = new MenuSlot(slot);
			
			menuSlot.setItem(itemPageBack);
			menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "Herbalism Menu Progress");
			mapSlot.put(slot, menuSlot);
		} 
		
		if (page < maxPage) {
			for (int slot : arraySlotNext) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				menuSlot.setItem(itemPageNext);
				menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "Herbalism Menu Progress_Fertilizer " + (page+1));
				mapSlot.put(slot, menuSlot);
			} 
		}
		
		if (page > 1) {
			for (int slot : arraySlotPrevious) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				menuSlot.setItem(itemPagePrevious);
				menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "Herbalism Menu Progress_Fertilizer " + (page-1));
				mapSlot.put(slot, menuSlot);
			} 
		}
		
		for (int index = 0; index < abilitySlotComponent.length; index++) {
			final int slot = abilitySlotComponent[index];
			final int indexRegister = index + (slots*(page-1));
			
			if (indexRegister < size) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				if (indexRegister < listFertilizerUnlocked.size()) {
					final FertilizerProperties fertilizerProperties = listFertilizerUnlocked.get(indexRegister);
					final FertilizerEffect fertilizerEffect = fertilizerProperties.getEffect();
					final String fertilizer = fertilizerProperties.getId();
					final ItemStack item = fertilizerProperties.getItem();
					final MaterialEnum materialEnum = MaterialEnum.getMaterialEnum(item);
					final double effectGrowthAdditional = fertilizerEffect.getGrowthAdditional();
					final double effectGrowthPercent = fertilizerEffect.getGrowthPercent();
					
					String headerFertilizer = Language.MENU_ITEM_HEADER_PROGRESS_FERTILIZER_UNLOCKED.getText(player);
					List<String> loreFertilizer = Language.MENU_ITEM_LORES_PROGRESS_FERTILIZER_UNLOCKED.getListText(player);
					
					mapPlaceholder.clear();
					mapPlaceholder.put("fertilizer_id", fertilizer);
					mapPlaceholder.put("effect_growth_additional", String.valueOf(MathUtil.roundNumber(effectGrowthAdditional)));
					mapPlaceholder.put("effect_growth_percent", String.valueOf(MathUtil.roundNumber(effectGrowthPercent)));
					
					headerFertilizer = TextUtil.placeholder(mapPlaceholder, headerFertilizer);
					loreFertilizer = TextUtil.placeholder(mapPlaceholder, loreFertilizer);
					
					final ItemStack itemFertilizer = EquipmentUtil.createItem(materialEnum, headerFertilizer, 1, loreFertilizer);
					
					menuSlot.setItem(itemFertilizer);
					mapSlot.put(slot, menuSlot);
				} else {
					final FertilizerProperties fertilizerProperties = listFertilizerLocked.get(indexRegister - listFertilizerUnlocked.size());
					final ItemRequirement requirement = fertilizerProperties.getRequirement();
					final String fertilizer = fertilizerProperties.getId();
					final String requirementPermission = requirement.getPermission();
					final int requirementLevel = requirement.getLevel();;
					
					String headerFertilizer = Language.MENU_ITEM_HEADER_PROGRESS_FERTILIZER_LOCKED.getText(player);
					List<String> loreFertilizer = Language.MENU_ITEM_LORES_PROGRESS_FERTILIZER_LOCKED.getListText(player);
					
					mapPlaceholder.clear();
					mapPlaceholder.put("fertilizer_id", fertilizer);
					mapPlaceholder.put("requirement_permission", requirementPermission != null ? requirementPermission : "null");
					mapPlaceholder.put("requirement_level", String.valueOf(requirementLevel));
					
					headerFertilizer = TextUtil.placeholder(mapPlaceholder, headerFertilizer);
					loreFertilizer = TextUtil.placeholder(mapPlaceholder, loreFertilizer);
					
					final ItemStack itemFertilizer = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerFertilizer, 1, loreFertilizer);
					
					menuSlot.setItem(itemFertilizer);
					mapSlot.put(slot, menuSlot);
				}
			} else {
				break;
			}
		}
		
		final Text title = new TextLine(textTitle);
		final MenuGUI menuGUI = new MenuGUI(player, id, row, title, executor, false, mapSlot);
		
		Menu.openMenu(player, menuGUI);
		return;
	}
	
	public final void openMenuShop(Player player) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final PlaceholderManager placeholderManager = pluginManager.getPlaceholderManager();
		final HerbalismConfig mainConfig = plugin.getMainConfig();
		final MenuExecutor executor = getMenuExecutor();
		final boolean enableMenuPageBack = mainConfig.isEnableMenuPageBack();
		
		final String id = "Herbalism Shop";
		final String textTitle = Language.MENU_PAGE_TITLE_SHOP.getText(player);
		final int row = 5;
		final int size = row * 9;
		final Text title = new TextLine(textTitle);
		final String prefix = placeholderManager.getPlaceholder("prefix");
		final String headerPageBack = Language.MENU_ITEM_HEADER_PAGE_BACK.getText(player);
		final String headerInformation = Language.MENU_ITEM_HEADER_SHOP_INFORMATION.getText(player);
		final String headerHarvestBuy = Language.MENU_ITEM_HEADER_SHOP_HARVEST_BUY.getText(player);
		final String headerHarvestSell = Language.MENU_ITEM_HEADER_SHOP_HARVEST_SELL.getText(player);
		final String headerFertilizerBuy = Language.MENU_ITEM_HEADER_SHOP_FERTILIZER_BUY.getText(player);
		final int[] arraySlotPaneWhite = new int[] {0, 8, 9, 17, 18, 26, 27, 35, 36, 44};
		final int[] arraySlotPaneRed = new int[] {1, 2, 6, 7, 37, 38, 42, 43};
		final int[] arraySlotPageBack = new int[] {39, 40, 41};
		final int[] arraySlotInformation = new int[] {3, 4, 5};
		final MenuSlot menuSlotHarvestBuy = new MenuSlot(20);
		final MenuSlot menuSlotHarvestSell = new MenuSlot(22);
		final MenuSlot menuSlotFertilizerBuy = new MenuSlot(24);
		final Map<Integer, MenuSlot> mapSlot = new ConcurrentHashMap<Integer, MenuSlot>();
		
		List<String> lorePageBack = Language.MENU_ITEM_LORES_PAGE_BACK.getListText(player);
		List<String> loreInformation = Language.MENU_ITEM_LORES_SHOP_INFORMATION.getListText(player);
		List<String> loreHarvestBuy = Language.MENU_ITEM_LORES_SHOP_HARVEST_BUY.getListText(player);
		List<String> loreHarvestSell = Language.MENU_ITEM_LORES_SHOP_HARVEST_SELL.getListText(player);
		List<String> loreFertilizerBuy = Language.MENU_ITEM_LORES_SHOP_FERTILIZER_BUY.getListText(player);

		final ItemStack itemPaneBlack = EquipmentUtil.createItem(MaterialEnum.BLACK_STAINED_GLASS_PANE, prefix, 1);
		final ItemStack itemPaneWhite = EquipmentUtil.createItem(MaterialEnum.WHITE_STAINED_GLASS_PANE, prefix, 1);
		final ItemStack itemPaneRed = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, prefix, 1);
		final ItemStack itemPageBack = EquipmentUtil.createItem(MaterialEnum.IRON_BARS, headerPageBack, 1, lorePageBack);
		final ItemStack itemInformation = EquipmentUtil.createItem(MaterialEnum.SIGN, headerInformation, 1, loreInformation);
		final ItemStack itemHarvestBuy = EquipmentUtil.createItem(MaterialEnum.WHEAT_SEEDS, headerHarvestBuy, 1, loreHarvestBuy);
		final ItemStack itemHarvestSell = EquipmentUtil.createItem(MaterialEnum.WHEAT, headerHarvestSell, 1, loreHarvestSell);
		final ItemStack itemFertilizerBuy = EquipmentUtil.createItem(MaterialEnum.GRAY_DYE, headerFertilizerBuy, 1, loreFertilizerBuy);
		
		menuSlotHarvestBuy.setItem(itemHarvestBuy);
		menuSlotHarvestSell.setItem(itemHarvestSell);
		menuSlotFertilizerBuy.setItem(itemFertilizerBuy);
		
		menuSlotHarvestBuy.setActionArguments(ActionCategory.ALL_CLICK, "Herbalism Menu Shop_Harvest_Buy");
		menuSlotHarvestSell.setActionArguments(ActionCategory.ALL_CLICK, "Herbalism Menu Shop_Harvest_Sell");
		menuSlotFertilizerBuy.setActionArguments(ActionCategory.ALL_CLICK, "Herbalism Menu Shop_Fertilizer_Buy");
		
		mapSlot.put(menuSlotHarvestBuy.getSlot(), menuSlotHarvestBuy);
		mapSlot.put(menuSlotHarvestSell.getSlot(), menuSlotHarvestSell);
		mapSlot.put(menuSlotFertilizerBuy.getSlot(), menuSlotFertilizerBuy);
		
		if (enableMenuPageBack) {
			for (int slot : arraySlotPageBack) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				menuSlot.setItem(itemPageBack);
				menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "Herbalism Menu Home");
				mapSlot.put(slot, menuSlot);
			}
		}
		
		for (int slot : arraySlotInformation) {
			final MenuSlot menuSlot = new MenuSlot(slot);
			
			menuSlot.setItem(itemInformation);
			mapSlot.put(slot, menuSlot);
		} 
		
		for (int slot : arraySlotPaneWhite) {
			final MenuSlot menuSlot = new MenuSlot(slot);
			
			menuSlot.setItem(itemPaneWhite);
			mapSlot.put(slot, menuSlot);
		}
		
		for (int slot : arraySlotPaneRed) {
			final MenuSlot menuSlot = new MenuSlot(slot);
			
			menuSlot.setItem(itemPaneRed);
			mapSlot.put(slot, menuSlot);
		}
		
		for (int slot = 0; slot < size; slot++) {
			if (!mapSlot.containsKey(slot)) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				menuSlot.setItem(itemPaneBlack);
				mapSlot.put(slot, menuSlot);
			}
		}
		
		final MenuGUI menuGUI = new MenuGUI(null, id, row, title, executor, false, mapSlot);
		
		Menu.openMenu(player, menuGUI);
		return;
	}
	
	public final void openMenuBuyHarvest(Player player) {
		openMenuBuyHarvest(player, 1);
	}
	
	public final void openMenuBuyHarvest(Player player, int page) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final GameManager gameManager = plugin.getGameManager();
		final ItemManager itemManager = gameManager.getItemManager();
		final PlaceholderManager placeholderManager = pluginManager.getPlaceholderManager();
		final HerbalismConfig mainConfig = plugin.getMainConfig();
		final MenuExecutor executor = getMenuExecutor();
		final String id = "Herbalism Shop_Harvest_Buy";
		final String currency = mainConfig.getUtilityCurrency();
		final Collection<ItemProperties> allItemProperties = itemManager.getAllItemProperties();
		final List<ItemProperties> listUnlocked = new ArrayList<ItemProperties>();
		final List<ItemProperties> listLocked = new ArrayList<ItemProperties>();
		final Map<String, Integer> mapItemProperties = new HashMap<String, Integer>();
		
		for (ItemProperties itemProperties : allItemProperties) {
			if (itemProperties.isBuyable()) {
				final String itemId = itemProperties.getId();
				final ItemStack item = itemProperties.getItem();
				final Material material = item.getType();
				
				mapItemProperties.put(itemId, material.ordinal());
			}
		}
		
		for (String itemID : SortUtil.sortByValue(mapItemProperties).keySet()) {
			final ItemProperties itemProperties = itemManager.getItemProperties(itemID);
			
			if (itemProperties.isAllowed(player)) {
				listUnlocked.add(itemProperties);
			} else {
				listLocked.add(itemProperties);
			}
		}
		
		final int row = 6;
		final int size = mapItemProperties.size();
		final int slots = 28;
		final int maxPage = Math.max(1, size % slots > 0 ? (size/slots) + 1 : size/slots);
		final String prefix = placeholderManager.getPlaceholder("prefix");
		final String headerPageBack = Language.MENU_ITEM_HEADER_PAGE_BACK.getText(player);
		final String headerPageNext = Language.MENU_ITEM_HEADER_PAGE_NEXT.getText(player);
		final String headerPagePrevious = Language.MENU_ITEM_HEADER_PAGE_PREVIOUS.getText(player);
		final int[] arraySlotPane = new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 17, 18, 26, 27, 35, 36, 44, 45, 46, 47, 51, 52, 53};
		final int[] arraySlotBack = new int[] {48, 49, 50};
		final int[] arraySlotNext = new int[] {26, 35};
		final int[] arraySlotPrevious = new int[] {18, 27};
		final int[] abilitySlotComponent = new int[] {10, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 23, 24, 25, 28, 29, 30, 31, 32, 33, 34, 37, 38, 39, 40, 41, 42, 43};
		
		final Map<Integer, MenuSlot> mapSlot = new ConcurrentHashMap<Integer, MenuSlot>();
		final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
		
		page = MathUtil.limitInteger(page, 1, maxPage);
		
		String textTitle = Language.MENU_PAGE_TITLE_SHOP_HARVEST_BUY.getText(player);
		
		List<String> lorePageBack = Language.MENU_ITEM_LORES_PAGE_BACK.getListText(player);
		List<String> lorePageNext = Language.MENU_ITEM_LORES_PAGE_NEXT.getListText(player);
		List<String> lorePagePrevious = Language.MENU_ITEM_LORES_PAGE_PREVIOUS.getListText(player);
		
		mapPlaceholder.put("page", String.valueOf(page));
		mapPlaceholder.put("max_page", String.valueOf(maxPage));
		
		textTitle = TextUtil.placeholder(mapPlaceholder, textTitle);
		
		final ItemStack itemPaneBlack = EquipmentUtil.createItem(MaterialEnum.BLACK_STAINED_GLASS_PANE, prefix, 1);
		final ItemStack itemPageBack = EquipmentUtil.createItem(MaterialEnum.IRON_BARS, headerPageBack, 1, lorePageBack);
		final ItemStack itemPageNext = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerPageNext, 1, lorePageNext);
		final ItemStack itemPagePrevious = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerPagePrevious, 1, lorePagePrevious);
		
		for (int slot : arraySlotPane) {
			final MenuSlot menuSlot = new MenuSlot(slot);
			
			menuSlot.setItem(itemPaneBlack);
			mapSlot.put(slot, menuSlot);
		}
		
		for (int slot : arraySlotBack) {
			final MenuSlot menuSlot = new MenuSlot(slot);
			
			menuSlot.setItem(itemPageBack);
			menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "Herbalism Menu Shop");
			mapSlot.put(slot, menuSlot);
		} 
		
		if (page < maxPage) {
			for (int slot : arraySlotNext) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				menuSlot.setItem(itemPageNext);
				menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "Herbalism Menu Shop_Harvest_Buy " + (page+1));
				mapSlot.put(slot, menuSlot);
			} 
		}
		
		if (page > 1) {
			for (int slot : arraySlotPrevious) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				menuSlot.setItem(itemPagePrevious);
				menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "Herbalism Menu Shop_Harvest_Buy " + (page-1));
				mapSlot.put(slot, menuSlot);
			} 
		}
		
		for (int index = 0; index < abilitySlotComponent.length; index++) {
			final int slot = abilitySlotComponent[index];
			final int indexRegister = index + (slots*(page-1));
			
			if (indexRegister < size) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				if (indexRegister < listUnlocked.size()) {
					final ItemProperties itemProperties = listUnlocked.get(indexRegister);
					final String itemId = itemProperties.getId();
					final ItemStack item = itemProperties.getItem().clone();
					final double price = itemProperties.getPrice();
					final List<String> lores = EquipmentUtil.getLores(item);
					
					List<String> loreComponent = Language.MENU_ITEM_FORMAT_SHOP_HARVEST_BUY_COMPONENT.getListText(player);
					
					mapPlaceholder.clear();
					mapPlaceholder.put("item", itemId);
					mapPlaceholder.put("price", String.valueOf(price));
					mapPlaceholder.put("symbol_currency", currency);
					
					loreComponent = TextUtil.placeholder(mapPlaceholder, loreComponent);
					
					lores.addAll(loreComponent);
					EquipmentUtil.setLores(item, lores);
					
					menuSlot.setItem(item);
					menuSlot.setActionArguments(ActionType.LEFT_CLICK, "Herbalism Action Harvest Buy " + itemId + " 1");
					menuSlot.setActionArguments(ActionType.RIGHT_CLICK, "Herbalism Action Harvest Buy " + itemId + " 64");
					mapSlot.put(slot, menuSlot);
				} else {
					final ItemProperties itemProperties = listLocked.get(indexRegister - listUnlocked.size());
					final ItemStack item = itemProperties.getItem().clone();
					final String itemName = EquipmentUtil.getDisplayName(item);
					
					String headerLocked = Language.MENU_ITEM_HEADER_SHOP_LOCKED.getText(player);
					List<String> loreLocked = Language.MENU_ITEM_LORES_SHOP_LOCKED.getListText(player);
					
					mapPlaceholder.clear();
					mapPlaceholder.put("item_name", itemName);
					
					headerLocked = TextUtil.placeholder(mapPlaceholder, headerLocked);
					loreLocked = TextUtil.placeholder(mapPlaceholder, loreLocked);
					EquipmentUtil.setMaterial(item, MaterialEnum.RED_STAINED_GLASS_PANE);
					
					EquipmentUtil.setDisplayName(item, headerLocked);
					EquipmentUtil.setLores(item, loreLocked);
					
					menuSlot.setItem(item);
					mapSlot.put(slot, menuSlot);
				}
			} else {
				break;
			}
		}
		
		final Text title = new TextLine(textTitle);
		final MenuGUI menuGUI = new MenuGUI(player, id, row, title, executor, false, mapSlot);
		
		Menu.openMenu(player, menuGUI);
		return;
	}
	
	public final void openMenuSellHarvest(Player player) {
		openMenuSellHarvest(player, 1);
	}
	
	public final void openMenuSellHarvest(Player player, int page) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final GameManager gameManager = plugin.getGameManager();
		final ItemManager itemManager = gameManager.getItemManager();
		final PlaceholderManager placeholderManager = pluginManager.getPlaceholderManager();
		final HerbalismConfig mainConfig = plugin.getMainConfig();
		final MenuExecutor executor = getMenuExecutor();
		final String id = "Herbalism Shop_Harvest_Sell";
		final String currency = mainConfig.getUtilityCurrency();
		final Collection<ItemProperties> allItemProperties = itemManager.getAllItemProperties();
		final List<ItemProperties> allowedItem = new ArrayList<ItemProperties>();
		final Map<String, Integer> mapItemProperties = new HashMap<String, Integer>();
		
		for (ItemProperties itemProperties : allItemProperties) {
			if (itemProperties.isSellable()) {
				final String itemId = itemProperties.getId();
				final ItemStack item = itemProperties.getItem();
				final Material material = item.getType();
				
				mapItemProperties.put(itemId, material.ordinal());
			}
		}
		
		for (String itemID : SortUtil.sortByValue(mapItemProperties).keySet()) {
			final ItemProperties itemProperties = itemManager.getItemProperties(itemID);
			
			allowedItem.add(itemProperties);
		}
		
		final int row = 6;
		final int size = allowedItem.size();
		final int slots = 28;
		final int maxPage = Math.max(1, size % slots > 0 ? (size/slots) + 1 : size/slots);
		final String prefix = placeholderManager.getPlaceholder("prefix");
		final String headerPageBack = Language.MENU_ITEM_HEADER_PAGE_BACK.getText(player);
		final String headerPageNext = Language.MENU_ITEM_HEADER_PAGE_NEXT.getText(player);
		final String headerPagePrevious = Language.MENU_ITEM_HEADER_PAGE_PREVIOUS.getText(player);
		final int[] arraySlotPane = new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 17, 18, 26, 27, 35, 36, 44, 45, 46, 47, 51, 52, 53};
		final int[] arraySlotBack = new int[] {48, 49, 50};
		final int[] arraySlotNext = new int[] {26, 35};
		final int[] arraySlotPrevious = new int[] {18, 27};
		final int[] abilitySlotComponent = new int[] {10, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 23, 24, 25, 28, 29, 30, 31, 32, 33, 34, 37, 38, 39, 40, 41, 42, 43};
		
		final Map<Integer, MenuSlot> mapSlot = new ConcurrentHashMap<Integer, MenuSlot>();
		final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
		
		page = MathUtil.limitInteger(page, 1, maxPage);
		
		String textTitle = Language.MENU_PAGE_TITLE_SHOP_HARVEST_SELL.getText(player);
		
		List<String> lorePageBack = Language.MENU_ITEM_LORES_PAGE_BACK.getListText(player);
		List<String> lorePageNext = Language.MENU_ITEM_LORES_PAGE_NEXT.getListText(player);
		List<String> lorePagePrevious = Language.MENU_ITEM_LORES_PAGE_PREVIOUS.getListText(player);
		
		mapPlaceholder.put("page", String.valueOf(page));
		mapPlaceholder.put("max_page", String.valueOf(maxPage));
		
		textTitle = TextUtil.placeholder(mapPlaceholder, textTitle);
		
		final ItemStack itemPaneBlack = EquipmentUtil.createItem(MaterialEnum.BLACK_STAINED_GLASS_PANE, prefix, 1);
		final ItemStack itemPageBack = EquipmentUtil.createItem(MaterialEnum.IRON_BARS, headerPageBack, 1, lorePageBack);
		final ItemStack itemPageNext = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerPageNext, 1, lorePageNext);
		final ItemStack itemPagePrevious = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerPagePrevious, 1, lorePagePrevious);
		
		for (int slot : arraySlotPane) {
			final MenuSlot menuSlot = new MenuSlot(slot);
			
			menuSlot.setItem(itemPaneBlack);
			mapSlot.put(slot, menuSlot);
		}
		
		for (int slot : arraySlotBack) {
			final MenuSlot menuSlot = new MenuSlot(slot);
			
			menuSlot.setItem(itemPageBack);
			menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "Herbalism Menu Shop");
			mapSlot.put(slot, menuSlot);
		} 
		
		if (page < maxPage) {
			for (int slot : arraySlotNext) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				menuSlot.setItem(itemPageNext);
				menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "Herbalism Menu Shop_Harvest_Sell " + (page+1));
				mapSlot.put(slot, menuSlot);
			} 
		}
		
		if (page > 1) {
			for (int slot : arraySlotPrevious) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				menuSlot.setItem(itemPagePrevious);
				menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "Herbalism Menu Shop_Harvest_Sell " + (page-1));
				mapSlot.put(slot, menuSlot);
			} 
		}
		
		for (int index = 0; index < abilitySlotComponent.length; index++) {
			final int slot = abilitySlotComponent[index];
			final int indexRegister = index + (slots*(page-1));
			
			if (indexRegister < size) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				final ItemProperties itemProperties = allowedItem.get(indexRegister);
				final String itemId = itemProperties.getId();
				final ItemStack item = itemProperties.getItem().clone();
				final double price = itemProperties.getPrice();
				final List<String> lores = EquipmentUtil.getLores(item);
				
				List<String> loreComponent = Language.MENU_ITEM_FORMAT_SHOP_HARVEST_SELL_COMPONENT.getListText(player);
				
				mapPlaceholder.clear();
				mapPlaceholder.put("item", itemId);
				mapPlaceholder.put("price", String.valueOf(price));
				mapPlaceholder.put("symbol_currency", currency);
				
				loreComponent = TextUtil.placeholder(mapPlaceholder, loreComponent);
				
				lores.addAll(loreComponent);
				EquipmentUtil.setLores(item, lores);
				
				menuSlot.setItem(item);
				menuSlot.setActionArguments(ActionType.LEFT_CLICK, "Herbalism Action Harvest Sell " + itemId + " 1");
				menuSlot.setActionArguments(ActionType.RIGHT_CLICK, "Herbalism Action Harvest Sell " + itemId + " 64");
				mapSlot.put(slot, menuSlot);
			} else {
				break;
			}
		}
		
		final Text title = new TextLine(textTitle);
		final MenuGUI menuGUI = new MenuGUI(player, id, row, title, executor, false, mapSlot);
		
		Menu.openMenu(player, menuGUI);
		return;
	}
	
	public final void openMenuBuyFertilizer(Player player) {
		openMenuBuyFertilizer(player, 1);
	}
	
	public final void openMenuBuyFertilizer(Player player, int page) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final GameManager gameManager = plugin.getGameManager();
		final FertilizerManager fertilizerManager = gameManager.getFertilizerManager();
		final PlaceholderManager placeholderManager = pluginManager.getPlaceholderManager();
		final HerbalismConfig mainConfig = plugin.getMainConfig();
		final MenuExecutor executor = getMenuExecutor();
		final String id = "Herbalism Shop_Fertilizer_Buy";
		final String currency = mainConfig.getUtilityCurrency();
		final Collection<FertilizerProperties> allFertilizerProperties = fertilizerManager.getAllFertilizer();
		final List<FertilizerProperties> listUnlocked = new ArrayList<FertilizerProperties>();
		final List<FertilizerProperties> listLocked = new ArrayList<FertilizerProperties>();
		final Map<String, Integer> mapFertilizerProperties = new HashMap<String, Integer>();
		
		for (FertilizerProperties fertilizerProperties : allFertilizerProperties) {
			if (fertilizerProperties.isBuyable()) {
				final String fertilizerID = fertilizerProperties.getId();
				final ItemStack item = fertilizerProperties.getItem();
				final Material material = item.getType();
				
				mapFertilizerProperties.put(fertilizerID, material.ordinal());
			}
		}
		
		for (String fertilizerID : SortUtil.sortByValue(mapFertilizerProperties).keySet()) {
			final FertilizerProperties fertilizerProperties = fertilizerManager.getFertilizer(fertilizerID);
			final ItemRequirement requirement = fertilizerProperties.getRequirement();
			
			if (requirement.isAllowed(player)) {
				listUnlocked.add(fertilizerProperties);
			} else {
				listLocked.add(fertilizerProperties);
			}
		}
		
		final int row = 6;
		final int size = mapFertilizerProperties.size();
		final int slots = 28;
		final int maxPage = Math.max(1, size % slots > 0 ? (size/slots) + 1 : size/slots);
		final String prefix = placeholderManager.getPlaceholder("prefix");
		final String headerPageBack = Language.MENU_ITEM_HEADER_PAGE_BACK.getText(player);
		final String headerPageNext = Language.MENU_ITEM_HEADER_PAGE_NEXT.getText(player);
		final String headerPagePrevious = Language.MENU_ITEM_HEADER_PAGE_PREVIOUS.getText(player);
		final int[] arraySlotPane = new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 17, 18, 26, 27, 35, 36, 44, 45, 46, 47, 51, 52, 53};
		final int[] arraySlotBack = new int[] {48, 49, 50};
		final int[] arraySlotNext = new int[] {26, 35};
		final int[] arraySlotPrevious = new int[] {18, 27};
		final int[] abilitySlotComponent = new int[] {10, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 23, 24, 25, 28, 29, 30, 31, 32, 33, 34, 37, 38, 39, 40, 41, 42, 43};
		
		final Map<Integer, MenuSlot> mapSlot = new ConcurrentHashMap<Integer, MenuSlot>();
		final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
		
		page = MathUtil.limitInteger(page, 1, maxPage);
		
		String textTitle = Language.MENU_PAGE_TITLE_SHOP_FERTILIZER_BUY.getText(player);
		
		List<String> lorePageBack = Language.MENU_ITEM_LORES_PAGE_BACK.getListText(player);
		List<String> lorePageNext = Language.MENU_ITEM_LORES_PAGE_NEXT.getListText(player);
		List<String> lorePagePrevious = Language.MENU_ITEM_LORES_PAGE_PREVIOUS.getListText(player);
		
		mapPlaceholder.put("page", String.valueOf(page));
		mapPlaceholder.put("max_page", String.valueOf(maxPage));
		
		textTitle = TextUtil.placeholder(mapPlaceholder, textTitle);
		
		final ItemStack itemPaneBlack = EquipmentUtil.createItem(MaterialEnum.BLACK_STAINED_GLASS_PANE, prefix, 1);
		final ItemStack itemPageBack = EquipmentUtil.createItem(MaterialEnum.IRON_BARS, headerPageBack, 1, lorePageBack);
		final ItemStack itemPageNext = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerPageNext, 1, lorePageNext);
		final ItemStack itemPagePrevious = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerPagePrevious, 1, lorePagePrevious);
		
		for (int slot : arraySlotPane) {
			final MenuSlot menuSlot = new MenuSlot(slot);
			
			menuSlot.setItem(itemPaneBlack);
			mapSlot.put(slot, menuSlot);
		}
		
		for (int slot : arraySlotBack) {
			final MenuSlot menuSlot = new MenuSlot(slot);
			
			menuSlot.setItem(itemPageBack);
			menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "Herbalism Menu Shop");
			mapSlot.put(slot, menuSlot);
		} 
		
		if (page < maxPage) {
			for (int slot : arraySlotNext) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				menuSlot.setItem(itemPageNext);
				menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "Herbalism Menu Shop_Fertilizer_Buy " + (page+1));
				mapSlot.put(slot, menuSlot);
			} 
		}
		
		if (page > 1) {
			for (int slot : arraySlotPrevious) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				menuSlot.setItem(itemPagePrevious);
				menuSlot.setActionArguments(ActionCategory.ALL_CLICK, "Herbalism Menu Shop_Fertilizer_Buy " + (page-1));
				mapSlot.put(slot, menuSlot);
			} 
		}
		
		for (int index = 0; index < abilitySlotComponent.length; index++) {
			final int slot = abilitySlotComponent[index];
			final int indexRegister = index + (slots*(page-1));
			
			if (indexRegister < size) {
				final MenuSlot menuSlot = new MenuSlot(slot);
				
				if (indexRegister < listUnlocked.size()) {
					final FertilizerProperties fertilizerProperties = listUnlocked.get(indexRegister);
					final String fertilizerID = fertilizerProperties.getId();
					final ItemStack item = fertilizerProperties.getItem().clone();
					final double price = fertilizerProperties.getPrice();
					final List<String> lores = EquipmentUtil.getLores(item);
					
					List<String> loreComponent = Language.MENU_ITEM_FORMAT_SHOP_FERTILIZER_BUY_COMPONENT.getListText(player);
					
					mapPlaceholder.clear();
					mapPlaceholder.put("item", fertilizerID);
					mapPlaceholder.put("price", String.valueOf(price));
					mapPlaceholder.put("symbol_currency", currency);
					
					loreComponent = TextUtil.placeholder(mapPlaceholder, loreComponent);
					
					lores.addAll(loreComponent);
					EquipmentUtil.setLores(item, lores);
					
					menuSlot.setItem(item);
					menuSlot.setActionArguments(ActionType.LEFT_CLICK, "Herbalism Action Fertilizer Buy " + fertilizerID + " 1");
					menuSlot.setActionArguments(ActionType.RIGHT_CLICK, "Herbalism Action Fertilizer Buy " + fertilizerID + " 64");
					mapSlot.put(slot, menuSlot);
				} else {
					final FertilizerProperties fertilizerProperties = listLocked.get(indexRegister - listUnlocked.size());
					final ItemStack item = fertilizerProperties.getItem().clone();
					final String itemName = EquipmentUtil.getDisplayName(item);
					
					String headerLocked = Language.MENU_ITEM_HEADER_SHOP_LOCKED.getText(player);
					List<String> loreLocked = Language.MENU_ITEM_LORES_SHOP_LOCKED.getListText(player);
					
					mapPlaceholder.clear();
					mapPlaceholder.put("item_name", itemName);
					
					headerLocked = TextUtil.placeholder(mapPlaceholder, headerLocked);
					loreLocked = TextUtil.placeholder(mapPlaceholder, loreLocked);
					
					EquipmentUtil.setDisplayName(item, headerLocked);
					EquipmentUtil.setLores(item, loreLocked);
					EquipmentUtil.setMaterial(item, MaterialEnum.RED_STAINED_GLASS_PANE);
					
					menuSlot.setItem(item);
					mapSlot.put(slot, menuSlot);
				}
			} else {
				break;
			}
		}
		
		final Text title = new TextLine(textTitle);
		final MenuGUI menuGUI = new MenuGUI(player, id, row, title, executor, false, mapSlot);
		
		Menu.openMenu(player, menuGUI);
		return;
	}
}