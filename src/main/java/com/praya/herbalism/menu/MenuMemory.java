package com.praya.herbalism.menu;

import org.bukkit.plugin.java.JavaPlugin;

import com.praya.herbalism.Herbalism;
import com.praya.herbalism.manager.game.MenuManager;
import com.praya.herbalism.menu.gui.MenuHerbalism;

public final class MenuMemory extends MenuManager {
	
	private final MenuHerbalism menuHerbalism;
	
	private MenuMemory(Herbalism plugin) {
		super(plugin);
		
		this.menuHerbalism = MenuHerbalism.getInstance();
	}
	
	private static class MenuMemorySingleton {
		private static final MenuMemory instance;
		
		static {
			final Herbalism plugin = JavaPlugin.getPlugin(Herbalism.class);
			
			instance = new MenuMemory(plugin);
		}
	}
	
	public static final MenuMemory getInstance() {
		return MenuMemorySingleton.instance;
	}
	
	@Override
	public final MenuHerbalism getMenuHerbalism() {
		return this.menuHerbalism;
	}
}