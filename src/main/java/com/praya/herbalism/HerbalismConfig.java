package com.praya.herbalism;

import java.io.File;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import core.praya.agarthalib.builder.message.BossBar.Color;
import core.praya.agarthalib.builder.message.BossBar.Style;

import com.praya.agarthalib.database.DatabaseType;
import com.praya.agarthalib.utility.FileUtil;
import com.praya.herbalism.handler.HandlerConfig;

public final class HerbalismConfig extends HandlerConfig {
	
	private static final String PATH_FILE = "Configuration/config.yml";
	
	public static final FileConfiguration config = new YamlConfiguration();
	
	protected HerbalismConfig(Herbalism plugin) {
		super(plugin);
		
		setup();
	}
	
	public final String getGeneralVersion() {
		return config.getString("Configuration.General.Version");
	}
	
	public final String getGeneralLocale() {
		return config.getString("Configuration.General.Locale");
	}
	
	public final boolean isMetricsMessage() {
		return config.getBoolean("Configuration.Metrics.Message");
	}
	
	public final boolean isHookMessage() {
		return config.getBoolean("Configuration.Hook.Message");
	}
	
	public final String getUtilityTooltip() {
		return config.getString("Configuration.Utility.Tooltip");
	}
	
	public final String getUtilityCurrency() {
		return config.getString("Configuration.Utility.Currency");
	}
	
	public final double getEffectRange() {
		return config.getDouble("Configuration.Effect.Range");
	}
	
	public final int getListContent() {
		return config.getInt("Configuration.List.Content");
	}
	
	public final int getPriorityActionbar() {
		return config.getInt("Configuration.Priority.Actionbar");
	}
	
	public final int getPriorityBossBar() {
		return config.getInt("Configuration.Priority.BossBar");
	}
	
	public final DatabaseType getDatabaseServerType() {
		return (DatabaseType) config.get("Configuration.Database.Server_Type");
	}
	
	public final String getDatabaseServerIdentifier() {
		return config.getString("Configuration.Database.Server_Identifier");
	}
	
	public final int getDatabaseLoadTimeout() {
		return config.getInt("Configuration.Database.Load_Timeout");
	}
	
	public final int getDatabasePeriodSave() {
		return config.getInt("Configuration.Database.Periode_Save");
	}
	
	public final String getDatabaseClientHost() {
		return config.getString("Configuration.Database.Client_Host");
	}
	
	public final int getDatabaseClientPort() {
		return config.getInt("Configuration.Database.Client_Port");
	}
	
	public final String getDatabaseClientDatabase() {
		return config.getString("Configuration.Database.Client_Database");
	}
	
	public final String getDatabaseClientUsername() {
		return config.getString("Configuration.Database.Client_Username");
	}
	
	public final String getDatabaseClientPassword() {
		return config.getString("Configuration.Database.Client_Password");
	}
	
	public final boolean isEnableMenuPageBack() {
		return config.getBoolean("Configuration.Menu.Enable_Page_Back");
	}
	
	public final boolean isEnableMenuPageHome() {
		return config.getBoolean("Configuration.Menu.Enable_Page_Home");
	}
	
	public final int getHerbalismMaxLevel() {
		return config.getInt("Configuration.Herbalism.Max_Level");
	}
	
	public final int getHerbalismMaxTotalPlant() {
		return config.getInt("Configuration.Herbalism.Max_Total_Plant");
	}
	
	public final int getHerbalismMaxTotalLevel() {
		return config.getInt("Configuration.Herbalism.Max_Total_Level");
	}
	
	public final int getHerbalismBaseTotalPlant() {
		return config.getInt("Configuration.Herbalism.Base_Total_Plant");
	}
	
	public final double getHerbalismFormulaExpA() {
		return config.getDouble("Configuration.Herbalism.Formula_Exp_A");
	}
	
	public final double getHerbalismFormulaExpB() {
		return config.getDouble("Configuration.Herbalism.Formula_Exp_B");
	}
	
	public final double getHerbalismFormulaExpC() {
		return config.getDouble("Configuration.Herbalism.Formula_Exp_C");
	}
	
	public final boolean getCropsEnableTrample() {
		return config.getBoolean("Configuration.Crops.Enable_Trample");
	}
	
	public final boolean getCropsEnableHologram() {
		return config.getBoolean("Configuration.Crops.Enable_Hologram");
	}
	
	public final boolean getCropsEnableBossBar() {
		return config.getBoolean("Configuration.Crops.Enable_BossBar");
	}
	
	public final boolean getCropsEnableActionbar() {
		return config.getBoolean("Configuration.Crops.Enable_Actionbar");
	}
	
	public final int getCropsPeriodUpdate() {
		return config.getInt("Configuration.Crops.Period_Update");
	}
	
	public final int getCropsDurationHologram() {
		return config.getInt("Configuration.Crops.Duration_Hologram");
	}
	
	public final double getCropsTimeDecayRelative() {
		return config.getDouble("Configuration.Crops.Time_Decay_Relative");
	}
	
	public final Color getBossBarColorSmall() {
		return (Color) config.get("Configuration.BossBar.Color_Small");
	}
	
	public final Color getBossBarColorMedium() {
		return (Color) config.get("Configuration.BossBar.Color_Medium");
	}
	
	public final Color getBossBarColorTall() {
		return (Color) config.get("Configuration.BossBar.Color_Tall");
	}
	
	public final Style getBossBarStyleSmall() {
		return (Style) config.get("Configuration.BossBar.Style_Small");
	}
	
	public final Style getBossBarStyleMedium() {
		return (Style) config.get("Configuration.BossBar.Style_Medium");
	}
	
	public final Style getBossBarStyleTall() {
		return (Style) config.get("Configuration.BossBar.Style_Tall");
	}
	
	public final void setup() {
		final File file = FileUtil.getFile(plugin, PATH_FILE);
		
		if (!file.exists()) {
			FileUtil.saveResource(plugin, PATH_FILE);
		}
		
		final FileConfiguration configurationResource = FileUtil.getFileConfigurationResource(plugin, PATH_FILE);
		final FileConfiguration configurationFile = FileUtil.getFileConfiguration(file);
		
		loadConfig(config, configurationResource);
		loadConfig(config, configurationFile);
	}
	
	private final void loadConfig(FileConfiguration config, FileConfiguration source) {
		for (String key : source.getKeys(false)) {
			if (key.equalsIgnoreCase("Configuration") || key.equalsIgnoreCase("Config")) {
				final ConfigurationSection dataSection = source.getConfigurationSection(key);
				
				for (String data : dataSection.getKeys(false)) {
					if (data.equalsIgnoreCase("General")) {
						final ConfigurationSection generalDataSection = dataSection.getConfigurationSection(data);
						
						for (String generalData : generalDataSection.getKeys(false)) {
							if (generalData.equalsIgnoreCase("Version")) {
								final String path = "Configuration.General.Version";
								final String generalVersion = generalDataSection.getString(generalData);
								
								config.set(path, generalVersion);
							} else if (generalData.equalsIgnoreCase("Locale")) {
								final String path = "Configuration.General.Locale";
								final String generalLocale = generalDataSection.getString(generalData);
								
								config.set(path, generalLocale);
							}
						}
					} else if (data.equalsIgnoreCase("Metrics")) {
						final ConfigurationSection metricsDataSection = dataSection.getConfigurationSection(data);
						
						for (String metricsData : metricsDataSection.getKeys(false)) {
							if (metricsData.equalsIgnoreCase("Message")) {
								final String path = "Configuration.Metrics.Message";
								final boolean metricsMessage = metricsDataSection.getBoolean(metricsData);
								
								config.set(path, metricsMessage);
							}
						}
					} else if (data.equalsIgnoreCase("Hook")) {
						final ConfigurationSection hookDataSection = dataSection.getConfigurationSection(data);
						
						for (String hookData : hookDataSection.getKeys(false)) {
							if (hookData.equalsIgnoreCase("Message")) {
								final String path = "Configuration.Hook.Message";
								final boolean hookMessage = hookDataSection.getBoolean(hookData);
								
								config.set(path, hookMessage);
							}
						}
					} else if (data.equalsIgnoreCase("Utility")) {
						final ConfigurationSection utilityDataSection = dataSection.getConfigurationSection(data);
						
						for (String utilityData : utilityDataSection.getKeys(false)) {
							if (utilityData.equalsIgnoreCase("Tooltip")) {
								final String path = "Configuration.Utility.Tooltip";
								final String utilityTooltip = utilityDataSection.getString(utilityData);
								
								config.set(path, utilityTooltip);
							} else if (utilityData.equalsIgnoreCase("Currency")) {
								final String path = "Configuration.Utility.Currency";
								final String utilityCurrency = utilityDataSection.getString(utilityData);
								
								config.set(path, utilityCurrency);
							}
						}
					} else if (data.equalsIgnoreCase("Effect")) {
						final ConfigurationSection effectDataSection = dataSection.getConfigurationSection(data);
						
						for (String effectData : effectDataSection.getKeys(false)) {
							if (effectData.equalsIgnoreCase("Range")) {
								final String path = "Configuration.Effect.Range";
								final double effectRange = effectDataSection.getDouble(effectData);
								
								config.set(path, effectRange);
							}
						}
					} else if (data.equalsIgnoreCase("List")) {
						final ConfigurationSection listDataSection = dataSection.getConfigurationSection(data);
						
						for (String listData : listDataSection.getKeys(false)) {
							if (listData.equalsIgnoreCase("Content")) {
								final String path = "Configuration.List.Content";
								final int listContent = listDataSection.getInt(listData);
								
								config.set(path, listContent);
							}
						}
					} else if (data.equalsIgnoreCase("Priority")) {
						final ConfigurationSection priorityDataSection = dataSection.getConfigurationSection(data);
						
						for (String priorityData : priorityDataSection.getKeys(false)) {
							if (priorityData.equalsIgnoreCase("Actionbar")) {
								final String path = "Configuration.Priority.Actionbar";
								final int priorityActionbar = priorityDataSection.getInt(priorityData);
								
								config.set(path, priorityActionbar);
							} else if (priorityData.equalsIgnoreCase("BossBar")) {
								final String path = "Configuration.Priority.BossBar";
								final int priorityBossBar = priorityDataSection.getInt(priorityData);
								
								config.set(path, priorityBossBar);
							}
						}
					} else if (data.equalsIgnoreCase("Database")) {
						final ConfigurationSection databaseDataSection = dataSection.getConfigurationSection(data);
						
						for (String databaseData : databaseDataSection.getKeys(false)) {
							if (databaseData.equalsIgnoreCase("Server_Type")) {
								final String path = "Configuration.Database.Server_Type";
								final String databaseServerTypeText = databaseDataSection.getString(databaseData);
								final DatabaseType databaseServerType = DatabaseType.getDatabaseType(databaseServerTypeText);
								
								config.set(path, databaseServerType);
							} else if (databaseData.equalsIgnoreCase("Server_Identifier")) {
								final String path = "Configuration.Database.Server_Identifier";
								final String databaseServerIdentifier = databaseDataSection.getString(databaseData);
								
								config.set(path, databaseServerIdentifier);
							} else if (databaseData.equalsIgnoreCase("Load_Timeout")) {
								final String path = "Configuration.Database.Load_Timeout";
								final int databaseLoadTimeout = databaseDataSection.getInt(databaseData);
								
								config.set(path, databaseLoadTimeout);
							} else if (databaseData.equalsIgnoreCase("Periode_Save")) {
								final String path = "Configuration.Database.Periode_Save";
								final int databasePeriodSave = databaseDataSection.getInt(databaseData);
								
								config.set(path, databasePeriodSave);
							} else if (databaseData.equalsIgnoreCase("Client_Host")) {
								final String path = "Configuration.Database.Client_Host";
								final String databaseClientHost = databaseDataSection.getString(databaseData);
								
								config.set(path, databaseClientHost);
							} else if (databaseData.equalsIgnoreCase("Client_Port")) {
								final String path = "Configuration.Database.Client_Port";
								final int databaseClientPort = databaseDataSection.getInt(databaseData);
								
								config.set(path, databaseClientPort);
							} else if (databaseData.equalsIgnoreCase("Client_Database")) {
								final String path = "Configuration.Database.Client_Database";
								final String databaseClientDatabase = databaseDataSection.getString(databaseData);
								
								config.set(path, databaseClientDatabase);
							} else if (databaseData.equalsIgnoreCase("Client_Username")) {
								final String path = "Configuration.Database.Client_Username";
								final String databaseClientUsername = databaseDataSection.getString(databaseData);
								
								config.set(path, databaseClientUsername);
							} else if (databaseData.equalsIgnoreCase("Client_Password")) {
								final String path = "Configuration.Database.Client_Password";
								final String databaseClientPassword = databaseDataSection.getString(databaseData);
								
								config.set(path, databaseClientPassword);
							}
						}
					} else if (data.equalsIgnoreCase("Menu")) {
						final ConfigurationSection menuDataSection = dataSection.getConfigurationSection(data);
						
						for (String menuData : menuDataSection.getKeys(false)) {
							if (menuData.equalsIgnoreCase("Enable_Page_Back")) {
								final String path = "Configuration.Menu.Enable_Page_Back";
								final boolean menuEnablePageBack = menuDataSection.getBoolean(menuData);
								
								config.set(path, menuEnablePageBack);
							} else if (menuData.equalsIgnoreCase("Enable_Page_Home")) {
								final String path = "Configuration.Menu.Enable_Page_Home";
								final boolean menuEnablePageHome = menuDataSection.getBoolean(menuData);
								
								config.set(path, menuEnablePageHome);
							}
						}
					} else if (data.equalsIgnoreCase("Herbalism")) {
						final ConfigurationSection herbalismDataSection = dataSection.getConfigurationSection(data);
						
						for (String herbalismData : herbalismDataSection.getKeys(false)) {
							if (herbalismData.equalsIgnoreCase("Max_Level")) {
								final String path = "Configuration.Herbalism.Max_Level";
								final int herbalismMaxLevel = herbalismDataSection.getInt(herbalismData);
								
								config.set(path, herbalismMaxLevel);
							} else if (herbalismData.equalsIgnoreCase("Max_Total_Plant")) {
								final String path = "Configuration.Herbalism.Max_Total_Plant";
								final int herbalismMaxTotalPlant = herbalismDataSection.getInt(herbalismData);
								
								config.set(path, herbalismMaxTotalPlant);
							} else if (herbalismData.equalsIgnoreCase("Max_Total_Level")) {
								final String path = "Configuration.Herbalism.Max_Total_Level";
								final int herbalismMaxTotalLevel = herbalismDataSection.getInt(herbalismData);
								
								config.set(path, herbalismMaxTotalLevel);
							} else if (herbalismData.equalsIgnoreCase("Base_Total_Plant")) {
								final String path = "Configuration.Herbalism.Base_Total_Plant";
								final int herbalismBaseTotalPlant = herbalismDataSection.getInt(herbalismData);
								
								config.set(path, herbalismBaseTotalPlant);
							} else if (herbalismData.equalsIgnoreCase("Formula_Exp_A")) {
								final String path = "Configuration.Herbalism.Formula_Exp_A";
								final double herbalismFormulaExpA = herbalismDataSection.getDouble(herbalismData);
								
								config.set(path, herbalismFormulaExpA);
							} else if (herbalismData.equalsIgnoreCase("Formula_Exp_B")) {
								final String path = "Configuration.Herbalism.Formula_Exp_B";
								final double herbalismFormulaExpB = herbalismDataSection.getDouble(herbalismData);
								
								config.set(path, herbalismFormulaExpB);
							} else if (herbalismData.equalsIgnoreCase("Formula_Exp_C")) {
								final String path = "Configuration.Herbalism.Formula_Exp_C";
								final double herbalismFormulaExpC = herbalismDataSection.getDouble(herbalismData);
								
								config.set(path, herbalismFormulaExpC);
							}
						}
					} else if (data.equalsIgnoreCase("Crops")) {
						final ConfigurationSection cropsDataSection = dataSection.getConfigurationSection(data);
						
						for (String cropsData : cropsDataSection.getKeys(false)) {
							if (cropsData.equalsIgnoreCase("Enable_Trample")) {
								final String path = "Configuration.Crops.Enable_Trample";
								final boolean cropsEnableTrample = cropsDataSection.getBoolean(cropsData);
								
								config.set(path, cropsEnableTrample);
							} else if (cropsData.equalsIgnoreCase("Enable_Hologram")) {
								final String path = "Configuration.Crops.Enable_Hologram";
								final boolean cropsEnableHologram = cropsDataSection.getBoolean(cropsData);
								
								config.set(path, cropsEnableHologram);
							} else if (cropsData.equalsIgnoreCase("Enable_BossBar")) {
								final String path = "Configuration.Crops.Enable_BossBar";
								final boolean cropsEnableBossBar = cropsDataSection.getBoolean(cropsData);
								
								config.set(path, cropsEnableBossBar);
							} else if (cropsData.equalsIgnoreCase("Enable_Actionbar")) {
								final String path = "Configuration.Crops.Enable_Actionbar";
								final boolean cropsEnableActionbar = cropsDataSection.getBoolean(cropsData);
								
								config.set(path, cropsEnableActionbar);
							} else if (cropsData.equalsIgnoreCase("Period_Update")) {
								final String path = "Configuration.Crops.Period_Update";
								final int cropsPeriodUpdate = cropsDataSection.getInt(cropsData);
								
								config.set(path, cropsPeriodUpdate);
							} else if (cropsData.equalsIgnoreCase("Duration_Hologram")) {
								final String path = "Configuration.Crops.Duration_Hologram";
								final int cropsDurationHologram = cropsDataSection.getInt(cropsData);
								
								config.set(path, cropsDurationHologram);
							} else if (cropsData.equalsIgnoreCase("Time_Decay_Relative")) {
								final String path = "Configuration.Crops.Time_Decay_Relative";
								final double cropsTimeDecayRelative = cropsDataSection.getDouble(cropsData);
								
								config.set(path, cropsTimeDecayRelative);
							}
						}
					} else if (data.equalsIgnoreCase("BossBar")) {
						final ConfigurationSection bossDataSection = dataSection.getConfigurationSection(data);
						
						for (String bossData : bossDataSection.getKeys(false)) {
							if (bossData.equalsIgnoreCase("Color_Small")) {
								final String path = "Configuration.BossBar.Color_Small";
								final String bossBarColorSmallText = bossDataSection.getString(bossData);
								final Color bossBarColorSmall = Color.getColor(bossBarColorSmallText);
								
								config.set(path, bossBarColorSmall != null ? bossBarColorSmall : Color.RED);
							} else if (bossData.equalsIgnoreCase("Color_Medium")) {
								final String path = "Configuration.BossBar.Color_Medium";
								final String bossBarColorMediumText = bossDataSection.getString(bossData);
								final Color bossBarColorMedium = Color.getColor(bossBarColorMediumText);
								
								config.set(path, bossBarColorMedium != null ? bossBarColorMedium : Color.YELLOW);
							} else if (bossData.equalsIgnoreCase("Color_Tall")) {
								final String path = "Configuration.BossBar.Color_Tall";
								final String bossBarColorTallText = bossDataSection.getString(bossData);
								final Color bossBarColorTall = Color.getColor(bossBarColorTallText);
								
								config.set(path, bossBarColorTall != null ? bossBarColorTall : Color.YELLOW);
							} else if (bossData.equalsIgnoreCase("Style_Small")) {
								final String path = "Configuration.BossBar.Style_Small";
								final String bossBarStyleSmallText = bossDataSection.getString(bossData);
								final Style bossBarStyleSmall = Style.getStyle(bossBarStyleSmallText);
								
								config.set(path, bossBarStyleSmall != null ? bossBarStyleSmall : Style.PROGRESS);
							} else if (bossData.equalsIgnoreCase("Style_Medium")) {
								final String path = "Configuration.BossBar.Style_Medium";
								final String bossBarStyleMediumText = bossDataSection.getString(bossData);
								final Style bossBarStyleMedium = Style.getStyle(bossBarStyleMediumText);
								
								config.set(path, bossBarStyleMedium != null ? bossBarStyleMedium : Style.PROGRESS);
							} else if (bossData.equalsIgnoreCase("Style_Tall")) {
								final String path = "Configuration.BossBar.Style_Tall";
								final String bossBarStyleTallText = bossDataSection.getString(bossData);
								final Style bossBarStyleTall = Style.getStyle(bossBarStyleTallText);
								
								config.set(path, bossBarStyleTall != null ? bossBarStyleTall : Style.PROGRESS);
							}
						}
					}
				}
			}
		}
	}
}